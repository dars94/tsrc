<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>About TSRC─CSR</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　<a href="about.php">About TSRC</a>　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" --><div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05_2.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05-2.jpg" width="171" height="22" id="Image14" onmouseover="MM_swapImage('Image14','','images/about/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><img src="../EN/images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><img src="images/about/b05-3_2.jpg" alt="" name="Image8" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/about/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
           <tr>
            <td><img src="../EN/images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="../EN/about05_4.php"><img src="images/about/b05-4.jpg" alt="" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/about/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><img src="../EN/images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="../EN/about05_5.php"><img src="images/about/b05-5.jpg" alt="" name="Image10" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/about/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><img src="../EN/images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="../EN/about05_6.php"><img src="images/about/b05-6.jpg" alt="" width="171" height="38" id="Image11" onmouseover="MM_swapImage('Image11','','images/about/b05-6_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><img src="../EN/images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="../EN/about05_7.php"><img src="images/about/b05-7.jpg" alt="" name="Image12" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/about/b05-7_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><img src="../EN/images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="../EN/about05_8.php"><img src="images/about/b05-8.jpg" alt="" name="Image13" width="171" height="38" id="Image13" onmouseover="MM_swapImage('Image13','','images/about/b05-8_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top">
        <table width="688" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="184"><img src="../TW/images/about/pic01_5_3.jpg" width="184" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="310"><img src="../TW/images/about/pic02_5_3.jpg" width="310" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="184"><img src="../TW/images/about/pic03_5_3.jpg" width="184" height="197" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">Committed to fulfill social responsibilities</td>
          </tr>
          <tr>
            <td class="gray12_3">Since its foundation in 1973, TSRC has been pooling the strength of employees  to fulfill social  responsibilities and completed the enterprise citizen through the following activities under the vision of &ldquo;taking social  responsibilities and pursuing innovative developments.&rdquo;</td>
          </tr>
          <tr>
            <td class="gray12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%" align="left" valign="top">(1)</td>
                <td width="97%"> With the technology developed, TSRC  sponsored the rubber signs and chairs at  Kaohsiung City  Shoushan  Zoo and donated cash to improve the relevant facilities as  well.</td>
              </tr>
              <tr>
                <td align="left" valign="top">(2) </td>
                <td>Through the anniversary celebration of our company, TSRC organized its employees to hold activities, such  as tree planting, mountain cleaning, and hiking in Buddha Mountain.</td>
              </tr>
              <tr>
                <td align="left" valign="top">(3) </td>
                <td>TSRC donated thermometers to students in local schools with  a view to help  to prevent flu.</td>
              </tr>
              <tr>
                <td align="left" valign="top">(4) </td>
                <td>Yearly, TSRC funds to pay  bills for utilities for the community residents, the scholarships, and the nutritious lunch from  its own good-neighborly expense and social feedback fund. TSRC also makes donations to public  interest groups irregularly to help the disadvantaged.</td>
              </tr>
              <tr>
                <td align="left" valign="top">(5) </td>
                <td>By holding the spirit of &quot;sharing  the  woes&quot;, TSRC mobilized all  the enterprise to make donations to the  earthquake disaster area in Sichuan and the southern area of Taiwan stricken by Typhoon  Morakot.</td>
              </tr>
              <tr>
                <td align="left" valign="top">(6) </td>
                <td>TSRC encourages employees to participate in the activities of  organizations for social welfare, religious conviction,  and public interest to care and  support the disadvantaged.</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
