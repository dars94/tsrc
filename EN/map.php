<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/map_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>site map</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<link href="css/map.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg')">
<div id="wrap_map">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="about.php">About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_map">
        <div id="sitemap_bg"></div>
        <div id="sitemap-content">
          <div id="alpha" class="gray12">
            <p class="toppage"><a href="index.php">Home</a></p>
            <ul class="sitemap">
              <li><a href="about.php">About TSRC</a>
                <ul>
                  <li><a href="about01.php">Vision and Mission</a></li>
                  <li><a href="about02.php">Chairman's Message</a></li>
                  <li><a href="about05.php">President & Ceo's Message</a></li>
                  <li><a href="about03.php">Company History</a></li>
                  <li><a href="about04.php">Maijor Business</a></li>
                  <li class="end"><a href="about05_2.php">CSR</a>
                    <ul>
                      <li><a href="about05_2.php">Corporate Vision</a></li>
                      <li><a href="about05_3.php">Social participation</a></li>
                      <li><a href="about05_4.php">Employee care</a></li>
                      <li><a href="about05_5.php">Green products development</a></li>
                      <li><a href="about05_6.php">Environmental protection policies</a></li>
                      <li><a href="about05_7.php">Safety& Sanitation Management</a></li>
                      <li class="end"><a href="about05_8.php"> Performance of environment,safely, and health</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="service.php">Investor Relations</a>
                <ul>
                  <li><a href="service.php">Report To Shareholders</a></li>
                  <li><a href="service02.php">Financials Information</a>
                    <ul>
                      <li><a href="service02_1.php">Monthly Turnover</a></li>
                      <li class="end"><a href="service02_1.php">Financials Report</a></li>
                    </ul>
                  </li>
                  <li><a href="service03.php">Annual Reports</a></li>
                  <li><a href="service04.php">Corporate Governance</a></li>
                  <li class="end"><a href="service05.php">Shareholders Counter</a>
                    <ul>
                      <li><a href="service05.php">Stock Quotes</a></li>
                      <li><a href="service05.php">Shareholders' Meetings </a></li>
                      <li><a href="service05.php">Dividend Distribution</a></li>
                      <li><a href="service05.php">Announcement of Material Information</a></li>
                      <li class="end"><a href="service05.php">Contact</a></li>
                    </ul>
                </ul>
              </li>
              <li><a href="product.php">Products</a>
                <ul>
                  <li><a href="product01.php">Synthetic Rubber Business</a>
                    <ul>
                      <li><a href="product01_1.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">ESBR</span></a></li>
                      <li><a href="product01_2.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">SSBR</span></a></li>
                      <li><a href="product01_3.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">BR</span></a></li>
                      <li class="end"><a href="product01_4.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">NBR</span></a></li>
                    </ul>
                  </li>
                  <li><a href="product02.php">Applied Polymers Business</a>
                    <ul>
                    <li><a href="product02_5.php">Customer Service Center</a></li>
                      <li><a href="product02_1.php"><img src="images/product/TA.png" alt="" width="45" height="14" border="0" /></a><a href="product02_1.php"><span class="blue12">TPE</span></a></li>
                      <li><a href="product02_2.php">SBS SIS SEBS</a></li>
                      <li><a href="product02_3.php">About TPE</a></li>
                      <li class="end"><img src="../TW/images/blend02.png" width="80" height="15" /></li>
                    </ul>
                  </li>
                  <li class="end"><a href="product03.php">Innovate Products Service</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div id="beta" class="gray12">
            <ul class="sitemap">
              <li><a href="research.php">Technical Development</a>
                <ul>
                  <li><a href="research01.php">New Product Development</a></li>
                  <li><a href="research02.php">New Process Development</a></li>
                  <li class="end"><a href="research03.php">New application technology and services</a></li>
                </ul>
              </li>
              <li>News
                <ul>
                  <li><a href="news.php">Latest News</a></li>
                  <li class="end"><a href="news02.php">Exhibition & Seminar</a></li>
                </ul>
              </li>
              <li><a href="member.php">Group Member</a>
                <ul>
                  <li><a href="member.php">Head OfficeTSRC CORPORATION </a></li>
                  <li><a href="member02.php">SHEN HUA CHEMICAL  INDUSTRIAL CO., LTD.</a></li>
                  <li><a href="member03.php">TSRC (Nantong) INDUSTRIAL LTD.</a></li>
                  <li>TSRC-UBE (Nantong) CHEMICAL INDUSTRIAL CO., LTD. Ltd.</li>
                  <li><a href="member05.php">LANXESS-TSRC (Nantone) CHEMICAL INDUSTRIAL CO., LTD.</a></li>
                  <li><a href="member06.php">DEXCO POLYMERS LP</a></li>
                  <li><a href="member07.php">INDIAN SYNTHETIC RUBBER LTD.</a></li>
                  <li><a href="member08.php">THAI SYNTHETIC RUBBER COMPANY LTD.</a></li>
                  <li><a href="member09.php">TSRC(SHANGHAI) INDUSTRIES LTD.</a></li>
                  <li>TSRC(JINAN) INDUSTRIES LTD.</li>
                  <li class="end"><a href="member11.php">NANTONG QIX STORAGE CO., LTD.</a></li>
                </ul>
              </li>
              <li class="end"><a href="#">other</a>
                <ul>
                  <li><a href="../EN/index.php">English</a></li>
                  <li><a href="../CN/index.php">Simplified Chinese</a></li>
                  <li><a href="../TW/index.php"></a>Traditional Chinese</li>
                  <li><a href="search.php">search</a></li>
                  <li><a href="map.php">site map</a></li>
                  <li class="end"><a href="mailto:tsrcsales@tsrc-global.com">contact us</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer_map">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">site map</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
