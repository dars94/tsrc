<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/service_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Investor Relations─Financials Information</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/service/b01_2.jpg','images/service/b02_2.jpg','images/service/b02-1_2.jpg','images/service/b04_2.jpg','images/service/b05-1_2.jpg','images/service/b05-2_2.jpg','images/service/b05-4_2.jpg','images/service/b05-5_2.jpg','images/service/b03_2.jpg','images/service/b05_2.jpg','images/service/b05-3_2.jpg','../TW/images/service/b02-2_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="about.php">About TSRC</a></span><span class="blue12">　l　</span><span class="blue12"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="service.php"><img src="images/service/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/service/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/service/b02_2.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/service/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="service02_1.php"><img src="images/service/b02-1.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/service/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service02_1.php"><img src="images/service/b02-2.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/service/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service03.php"><img src="images/service/b03.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/service/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service04.php"><img src="images/service/b04.jpg" name="Image7" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/service/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05.jpg" name="Image8" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/service/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-1.jpg" name="Image9" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/service/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-2.jpg" name="Image10" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/service/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-3.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/service/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-4.jpg" name="Image12" width="171" height="38" id="Image12" onmouseover="MM_swapImage('Image12','','images/service/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-5.jpg" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/service/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_6">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="115" align="center" valign="top"><img src="../TW/images/service/pic01_1_1.jpg" alt="" width="115" height="148" /></td>
                  <td width="6"><img src="images/spacer.gif" width="6" height="10" /></td>
                  <td width="567" align="center" valign="top"><table width="567" class="table" summary="Submitted table designs">
                      <thead>
                        <tr class="gray12" >
                          <td width="150" align="center" bgcolor="#BBC595" scope="col">Item</td>
                          <td width="111" align="center" bgcolor="#BBC595" scope="col"> Unit</td>
                          <td width="61" align="center" bgcolor="#BBC595" scope="col">2008</td>
                          <td width="61" align="center" bgcolor="#BBC595" scope="col">2009</td>
                          <td width="61" align="center" bgcolor="#BBC595" scope="col">2010</td>
                        </tr>
                      </thead>
                      <tfoot>
                      </tfoot>
                      <tbody>
                        <tr class="gray12" >
                          <td align="left" bgcolor="#FFFFFF" scope="row">Total assets</td>
                          <td align="center" bgcolor="#FFFFFF">Million</td>
                          <td align="center" bgcolor="#FFFFFF">22,657</td>
                          <td align="center" bgcolor="#FFFFFF">22,480</td>
                          <td align="center" bgcolor="#FFFFFF">25,082</td>
                        </tr>
                        <tr class="gray12">
                          <td align="left" scope="row">Liabilities</td>
                          <td align="center"> Million</td>
                          <td align="center">8,808</td>
                          <td align="center">8,333</td>
                          <td align="center">9,914</td>
                        </tr>
                        <tr class="gray12" >
                          <td align="left" bgcolor="#FFFFFF" scope="row">Stockholders' equity</td>
                          <td align="center" bgcolor="#FFFFFF">Million</td>
                          <td align="center" bgcolor="#FFFFFF">13,850</td>
                          <td align="center" bgcolor="#FFFFFF">14,146</td>
                          <td align="center" bgcolor="#FFFFFF">15,168</td>
                        </tr>
                        <tr class="gray12">
                          <td align="left" scope="row">Gross sale</td>
                          <td align="center">Million</td>
                          <td align="center">30,934</td>
                          <td align="center">22,051</td>
                          <td align="center">36,620</td>
                        </tr>
                        <tr class="gray12">
                          <td align="left" bgcolor="#FFFFFF"  scope="row">Gross profit</td>
                          <td align="center" bgcolor="#FFFFFF">Million</td>
                          <td align="center" bgcolor="#FFFFFF">4,687</td>
                          <td align="center" bgcolor="#FFFFFF">4,826</td>
                          <td align="center" bgcolor="#FFFFFF">6,586</td>
                        </tr>
                        <tr class="gray12">
                          <td align="left" id="r95" scope="row">Net income</td>
                          <td align="center"> Million</td>
                          <td align="center">3,145</td>
                          <td align="center">2,848</td>
                          <td align="center">4,089</td>
                        </tr>
                        <tr class="blue12" >
                          <td align="left" bgcolor="#FFFFFF" scope="row">Earnings per share - Diluted</td>
                          <td align="center" bgcolor="#FFFFFF">NT$</td>
                          <td align="center" bgcolor="#FFFFFF">4.25</td>
                          <td align="center" bgcolor="#FFFFFF">3.58</td>
                          <td align="center" bgcolor="#FFFFFF">5.03</td>
                        </tr>
                      </tbody>
                    </table>
                    <table width="567" class="table" summary="Submitted table designs">
                      <thead>
                        <tr class="gray12">
                          <td width="227" align="left" scope="col">Return on shareholders' equity</td>
                          <td width="90" align="center" scope="col">%</td>
                          <td width="74" align="center" scope="col">22.72</td>
                          <td width="74" align="center" scope="col">20.34</td>
                          <td width="78" align="center" scope="col">27.90</td>
                        </tr>
                      </thead>
                      <tfoot>
                      </tfoot>
                      <tbody>
                        <tr class="gray12">
                          <td align="left" bgcolor="#FFFFFF" scope="row">Debt ratio</td>
                          <td align="center" bgcolor="#FFFFFF">%</td>
                          <td align="center" bgcolor="#FFFFFF">38.87</td>
                          <td align="center" bgcolor="#FFFFFF">37.07</td>
                          <td align="center" bgcolor="#FFFFFF">39.53</td>
                        </tr>
                        <tr class="gray12">
                          <td align="left" scope="row">Cash dividend yield</td>
                          <td align="center">%</td>
                          <td align="center">7.20</td>
                          <td align="center">9.03</td>
                          <td align="center">7.40</td>
                        </tr>
                      </tbody>
                  </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><table width="688" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="169" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="../TW/images/service/graph01.jpg" alt="" width="169" height="150" /></td>
                      </tr>
                      <tr>
                        <td align="center" class="gray12">Gross sale<br />
                          (Million)</td>
                      </tr>
                    </table></td>
                  <td><img src="images/spacer.gif" alt="" width="4" height="10" /></td>
                  <td><table width="169" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="../TW/images/service/graph02.jpg" alt="" width="169" height="150" /></td>
                      </tr>
                      <tr>
                        <td align="center" class="gray12">Return on shareholders' <br />
                        equity%</td>
                      </tr>
                    </table></td>
                  <td><img src="images/spacer.gif" alt="" width="4" height="10" /></td>
                  <td><table width="169" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="../TW/images/service/graph03.jpg" alt="" width="169" height="150" /></td>
                      </tr>
                      <tr>
                        <td align="center" class="gray12">Debt ratio %</td>
                      </tr>
                      <tr>
                        <td align="center" class="gray12">&nbsp;</td>
                      </tr>
                    </table></td>
                  <td><img src="images/spacer.gif" alt="" width="4" height="10" /></td>
                  <td><table width="169" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="../TW/images/service/graph04.jpg" alt="" width="169" height="150" /></td>
                      </tr>
                      <tr>
                        <td align="center" class="gray12">Cash dividend yield %</td>
                      </tr>
                      <tr>
                        <td align="center" class="gray12">&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
