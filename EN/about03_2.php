     <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" id='btn_1' coords="3,2,168,24" />

                  <area shape="rect" id='btn_2' coords="174,2,342,25" />

                  <area shape="rect" id='btn_3' coords="348,1,513,24" />

                  <area shape="rect" id='btn_4' coords="519,3,683,25" />

                  </map></td>

              </tr>

            <tr>

              <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="65" align="left" valign="top" class="green12">Nov 1999</td>
                  <td width="527" align="left" class="gray12">The name of Taiwan Synthetic  Rubber Corporation was changed to  TSRC Corporation and the company logo was redesign  to diversify company future development.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 1999</td>
                  <td align="left" class="gray12">TSRC invested in  Taiwan High Speed Railway project.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Aug 1998</td>
                  <td align="left" class="gray12">BR plant  was de-bottlenecked to expand the capacity to  55,000 tons per year.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Feb 1998</td>
                  <td align="left" class="gray12">TSRC received ISO-14001  for  Environment Management System at  Kaohsiung plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jan 1997</td>
                  <td align="left" class="gray12">TSRC received ISO-9001for  the Quality Management System at  Kaohsiung plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 1996</td>
                  <td align="left" class="gray12">TSRC led a US$92 million investment  project in  100,000  tons per year of SBR  plant  in China. </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Feb 1996</td>
                  <td align="left" class="gray12">TSRC received the honorable  award in  &ldquo;Great Achievement of Industrial R&amp;D&rdquo; from the Ministry of Economic Affairs,  R.O.C. (Taiwan)</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Oct 1995</td>
                  <td align="left" class="gray12">TSRC participatedUS$2.4  million in 50,000 tons per year of BR plant in  Thailand.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 1995</td>
                  <td align="left" class="gray12">The hydrogenated type TPE product  was successfully developed and ready  for commercialization.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 1995</td>
                  <td align="left" class="gray12">TSRC invested in  power business.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sept 1994</td>
                  <td align="left" class="gray12">BR plant  was de-bottlenecked to expand production capacity to  51,000 tons per year.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 1994</td>
                  <td align="left" class="gray12">TSRC developed  new type of TPE  and built the second TPE production  line. The total TPE  capacity reached 54,000  tons per year.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jan 1994 </td>
                  <td align="left" class="gray12">TSRC receipted ISO-9002 for the Quality Assurance at  Kaohsiung plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sep 1991</td>
                  <td align="left" class="gray12">A pilot  plant and a co-generation  facility  were built.</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td align="center">&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>