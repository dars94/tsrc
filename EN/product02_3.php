<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/product_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Products─Applied Polymers Business</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/product/b01_2.jpg','images/product/b01-1_2.jpg','images/product/b01-2_2.jpg','images/product/b01-3_2.jpg','images/product/b01-4_2.jpg','images/product/b02_2.jpg','images/product/b02-1_2.jpg','images/product/b02-2_2.jpg','images/product/b02-3_2.jpg','images/product/b02-4_2.jpg','images/product/b03_2.jpg','images/product/btn02-3-1_2.png','images/product/btn02-3-2_2.png','images/product/btn02-3-3_2.png','images/product/btn02-3-4_2.png','images/product/btn02-3-5_2.png')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="about.php">About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="blue12"><a href="product.php">Products</a>　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product01.php"><img src="images/product/b01.jpg" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/product/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_1.php"><img src="images/product/b01-1.jpg" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/product/b01-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_2.php"><img src="images/product/b01-2.jpg" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/product/b01-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_3.php"><img src="images/product/b01-3.jpg" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/product/b01-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_4.php"><img src="images/product/b01-4.jpg" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/product/b01-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product02.php"><img src="images/product/b02_3.jpg" name="Image7" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/product/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_1.php"><img src="images/product/b02-1.jpg" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/product/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_2.php"><img src="images/product/b02-2.jpg" width="171" height="22" border="0" id="Image9" onmouseover="MM_swapImage('Image9','','images/product/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/product/b02-3_2.jpg" name="Image10" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/product/b02-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="product02_4.php"><img src="images/product/b02-4.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/product/b02-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product03.php"><img src="images/product/b03.jpg" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/product/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top_5"><img src="images/product/pic02_2_3.jpg" width="688" height="161" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_9">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td><table width="632" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="210" align="left" valign="top"><table width="210" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td align="left"><a href="#t01"><img src="images/product/btn02-3-1.png" alt="" name="Image13" width="73" height="13" id="Image13" onmouseover="MM_swapImage('Image13','','images/product/btn02-3-1_2.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                      </tr>
                      <tr>
                        <td align="left"><a href="#t02"><img src="images/product/btn02-3-2.png" alt="" name="Image14" width="126" height="11" id="Image14" onmouseover="MM_swapImage('Image14','','images/product/btn02-3-2_2.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                      </tr>
                      <tr>
                        <td align="left"><a href="#t03"><img src="images/product/btn02-3-3.png" alt="" name="Image15" width="184" height="13" id="Image15" onmouseover="MM_swapImage('Image15','','images/product/btn02-3-3_2.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                      </tr>
                      <tr>
                        <td align="left"><a href="#t04"><img src="images/product/btn02-3-4.png" alt="" name="Image16" width="197" height="13" id="Image16" onmouseover="MM_swapImage('Image16','','images/product/btn02-3-4_2.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                      </tr>
                      <tr>
                        <td align="left"><a href="#t05"><img src="images/product/btn02-3-5.png" alt="" name="Image17" width="153" height="14" id="Image17" onmouseover="MM_swapImage('Image17','','images/product/btn02-3-5_2.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                      </tr>
                    </table></td>
                  <td width="406" align="center" valign="top"><table width="422" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="left"><a name="t01" id="t"></a></td>
                      </tr>
                      <tr>
                        <td align="center"><table width="400" border="0" cellpadding="0" cellspacing="0" class="table_7">
                            <tr>
                              <td align="center"><table width="410" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td width="130" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="10">
                                        <tr>
                                          <td><img src="images/product/btn02-3-1_2.png" alt="" width="73" height="13" /><a name="t01" id="t01"></a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><table width="400" border="0" cellspacing="0" cellpadding="2">
                                        <tr>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>TPS (Styrene, SBC): SBS, SIS, SEBS</td>
                                        </tr>
                                        <tr>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Olefinic: TPO (polyolefin), TPV (EPDM/PP, partial sulfur)</td>
                                        </tr>
                                        <tr>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>TPU (polyurethane): ester (easy to hydrolyze), ether</td>
                                        </tr>
                                        <tr>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Engineering TPEs：TPE-E (polyester), TPE-A (amide)</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><img src="images/line02.png" alt="" width="400" height="21" /></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table width="410" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td width="320" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="10">
                                        <tr>
                                          <td><img src="images/product/btn02-3-2_2.png" alt="" width="126" height="11" /><a name="t02" id="t02"></a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><table width="400" border="0" cellspacing="0" cellpadding="2">
                                        <tr>
                                          <td colspan="2" align="left" class="gray12"><span class="blue12_2">●</span>Easiness to process, low energy consumption</td>
                                        </tr>
                                        <tr>
                                          <td width="230" align="left" class="gray12"><span class="blue12_2">● </span>Easiness to be colored</td>
                                          <td width="198" align="left" class="gray12"><span class="blue12_2">● </span>Recyclable waste materials</td>
                                        </tr>
                                        <tr>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Easiness to mix with plastic </td>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Short recovery cycle </td>
                                        </tr>
                                        <tr>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Low temperature resistance</td>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Need not be vulcanized</td>
                                        </tr>
                                        <tr>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Excellent anti-slippery performance</td>
                                          <td align="left" class="gray12"><span class="blue12_2">● </span>Product quality easy to control</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="middle"><span class="blue12_2">TPE Characteristics (Raw Rubber)</span></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><table width="400" cellpadding="0" cellspacing="0" class="table_8">
                                        <tr>
                                          <td width="20%" align="center" valign="middle" bgcolor="#D1E7EA"><img src="images/product/table.png" alt="" width="68" height="48" /></td>
                                          <td width="20%" align="center" bgcolor="#D1E7EA" class="gray10">Cold-Resistance</td>
                                          <td width="20%" align="center" bgcolor="#D1E7EA" class="gray10">Thermo-resistance</td>
                                          <td width="20%" align="center" bgcolor="#D1E7EA" class="gray10">Weather-resistant </td>
                                          <td width="20%" align="center" bgcolor="#D1E7EA" class="gray10">Stickiness<br />
                                            (in HWA)</td>
                                        </tr>
                                        <tr>
                                          <td align="center" bgcolor="#E6F5FF" class="blue12_2">SBS</td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><span class="blue12_2">~<img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></span></td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                        </tr>
                                        <tr>
                                          <td align="center" bgcolor="#D1E7EA" class="blue12_2">SIS</td>
                                          <td align="center" bgcolor="#D1E7EA"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#D1E7EA"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#D1E7EA"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#D1E7EA"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                        </tr>
                                        <tr>
                                          <td align="center" bgcolor="#E6F5FF" class="blue12_2">SEBS</td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                          <td align="center" bgcolor="#E6F5FF"><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="middle"><span class="gray12">Excellent</span><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" />　<span class="gray12">Good</span><img src="images/star.png" alt="" width="8" height="8" /><img src="images/star.png" alt="" width="8" height="8" />　<span class="gray12">Fair</span><img src="images/star.png" alt="" width="8" height="8" /></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><img src="images/line02.png" alt="" width="400" height="21" /></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table width="410" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="10">
                                        <tr>
                                          <td><img src="images/product/btn02-3-3_2.png" alt="" width="184" height="13" /><a name="t03" id="t03"></a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="middle"><span class="blue12_2">● </span><span class="gray12">With excellent processing characteristics, TPE can be injected, injected and blow moulded.</span></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><img src="images/line02.png" alt="" width="390" height="21" /></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table width="410" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="10">
                                        <tr>
                                          <td><img src="images/product/btn02-3-4_2.png" alt="" width="197" height="13" /><a name="t04" id="t04"></a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><table width="400" cellpadding="5" cellspacing="0" class="table_8">
                                        <tr>
                                          <td width="16%" align="right" bgcolor="#E6F5FF">&nbsp;</td>
                                          <td width="42%" align="left" class="blue12_2">PVC</td>
                                          <td width="42%" align="left" bgcolor="#E6F5FF"><span class="blue12_2">TPE</span></td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="middle" bgcolor="#E6F5FF" class="gray12">CompositionS </td>
                                          <td align="left" class="gray12">Chlorine containing hydrocarbon compounds machine</td>
                                          <td align="left" bgcolor="#E6F5FF" class="gray12">hydrocarbon compounds</td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="middle" bgcolor="#E6F5FF" class="gray12">Characteristics</td>
                                          <td align="left" valign="top" class="gray12"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_8_2">
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>Use of injection moulding</td>
                                              </tr>
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td><span class="blue12_2"> </span>DOP The plasticizer contains DOP</td>
                                              </tr>
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>Odor during the manufacturing process</td>
                                              </tr>
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>Likely to pollute the environment</td>
                                              </tr>
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>Strict waste disposal methods</td>
                                              </tr>
                                            </table></td>
                                          <td align="left" bgcolor="#E6F5FF" class="gray12"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_8_3">
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>Use of injection moulding machine</td>
                                              </tr>
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>No odor during the manufacturing process</td>
                                              </tr>
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>Unlikely to cause corrosion of the equipment, therefore reducing the equipment   maintenance cost</td>
                                              </tr>
                                              <tr>
                                                <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                                <td>Harmless to the environment and human bodies</td>
                                              </tr>
                                            </table></td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="middle" bgcolor="#E6F5FF" class="gray12">Limit of low temperature for operation </td>
                                          <td align="left" class="gray12">-10℃</td>
                                          <td align="left" bgcolor="#E6F5FF" class="gray12">Can reach -50℃; have excellent     weather resistance and low   temperature resistance</td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="middle" bgcolor="#E6F5FF" class="gray12">Specific gravity</td>
                                          <td align="left" class="gray12">1.3~1.4</td>
                                          <td align="left" bgcolor="#E6F5FF" class="gray12">0.9~1.1</td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="middle" bgcolor="#E6F5FF" class="gray12">Hardness</td>
                                          <td align="left" class="gray12">40A~98A</td>
                                          <td align="left" bgcolor="#E6F5FF" class="gray12">0A~98A</td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="middle" bgcolor="#E6F5FF" class="gray12">Others</td>
                                          <td align="left" class="gray12">Restricted in European nations</td>
                                          <td align="left" bgcolor="#E6F5FF" class="gray12">Toxicity-free, low pollution, good environmental protection effects</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="middle"><img src="images/line02.png" alt="" width="400" height="21" /></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="center"><table width="410" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="10">
                                        <tr>
                                          <td><img src="images/product/btn02-3-5_2.png" alt="" width="153" height="14" /><a name="t05" id="t05"></a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="2">
                                        <tr>
                                          <td align="center" valign="top"><span class="blue12_2">●</span></td>
                                          <td><span class="gray12">Replace some of traditional PVC, TPV and EPDM products with TPES products</span></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
