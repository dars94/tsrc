<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>About TSRC─Company History</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){
	$('#btn_1').live('click',function(){
		$.ajax({
			url:'about03_1.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
	$('#btn_2').live('click',function(){
		$.ajax({
			url:'about03_2.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
	$('#btn_3').live('click',function(){
		$.ajax({
			url:'about03_3.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
	$('#btn_4').live('click',function(){
		$.ajax({
			url:'about03_4.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　<a href="about.php">About TSRC</a>　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b03_2.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->

    <div id="content_top_6"><img src="../TW/images/about/pic01_3_1.jpg" width="688" height="120" /></div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->

    <div id="content_main">

	<!-- table year -->

      <div id="tabel_year">

      <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0"usemap="#Map" /></a>
                <map name="Map" id="Map">
                  <area shape="rect" coords="3,2,168,24" href="javascript:void(0)" id='btn_1' />
                  <area shape="rect" coords="174,2,342,25" href="javascript:void(0)" id='btn_2' />
                  <area shape="rect" coords="348,1,513,24" href="javascript:void(0)" id='btn_3' />
                  <area shape="rect" coords="519,3,683,25" href="javascript:void(0)" id='btn_4' />
                  </map></td>

              </tr>

            <tr>

              <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="65" align="left" valign="top" class="green12">Nov 2011</td>
                  <td width="527" align="left" class="gray12">TSRC, CPC and Fubon completed joint venture negotiations and signed the  agreement to form Taiwan Advanced Materials Corporation.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2011</td>
                  <td width="527" align="left" class="gray12">TSRC acquired 100% stake in Dexco  Polymers.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 2010</td>
                  <td align="left" class="gray12">TSRC established  a joint venture, LANXESS-TSRC (Nantong) Chemical Industrial Co., Ltd.,  with Lanxess A.G. of German in China and started a 30,000 tons per year of NBR  plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2010</td>
                  <td align="left" class="gray12">TSRC established  a joint venture, Indian  Synthetic Rubber Ltd, with  Indian Oil  Corporation Ltd. and Marubeni Corporation in Panipat,  India and started a 120,000 tons per year of ESBR  plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Dec 2009</td>
                  <td align="left" class="gray12">TSRC obtained the certifications from  Occupational Health and  Safety Advisory Services (&ldquo;OHSAS&rdquo;) 18001 and Taiwan Occupational  Safety and Health Management System (&ldquo;TOSHMS&rdquo;)</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 2009</td>
                  <td align="left" class="gray12">TSRC signed the styrene-butadiene-styrene  (&ldquo;SBS&rdquo;) technology license agreement with Russian RUSTEP LLC/ OJSC SIBUR HOLDING.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 2009 </td>
                  <td align="left" class="gray12">TSRC-UBE  (Nantong) Chemical  Industrial Company  Limited started BR production.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2008</td>
                  <td align="left" class="gray12">TSRC started operation of Singapore's Polybus Corporation Pte. Ltd.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2008 </td>
                  <td align="left" class="gray12">TSRC (Nantong) Industries Ltd. started SEBS production. </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Nov 2007</td>
                  <td align="left" class="gray12">Atlantic  Polymers GmbH, in Germany, was terminated.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sep 2007 </td>
                  <td align="left" class="gray12">TSRC (Jinan) Industries Ltd. started compound production.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Dec 2006</td>
                  <td align="left" class="gray12">TSRC established TSRC-UBE (Nantong) Chemical  Industrial Company  Limited in  Nantong, China with  50,000 tons per year of BR plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sep 2006</td>
                  <td align="left" class="gray12">TSRC established TSRC (Jinan) Industries Ltd. in Jinan, China with  a 5,000 tons per year of compound plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sep 2006</td>
                  <td align="left" class="gray12">TSRC established TSRC (Nantong) Industries  Ltd. in Nantong, China    with 20,000 tons  per year of SEBS plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Dec 2005</td>
                  <td align="left" class="gray12">The building of TPE Application Center in Gangshan, Taiwan was completed.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2005</td>
                  <td align="left" class="gray12">Shen  Hua Chemical Industrial Co., Ltd. completed ESBR expansion to  180,000 tons  per year.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2005</td>
                  <td align="left" class="gray12">The compound production  lines in Gangshan plant was on stream. </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Oct 2004</td>
                  <td align="left" class="gray12">The completion of Taipei  new offic</a>.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Aug 2004</td>
                  <td align="left" class="gray12">TSRC started the construction of compound plant in Gangshan, Taiwan.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2004</td>
                  <td align="left" class="gray12">TSRC started to build TPE Application  Center.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2004</td>
                  <td align="left" class="gray12">Nantong Qix Storage Co., Ltd. commenced  commercial operation.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Oct 2003</td>
                  <td align="left" class="gray12">Shen  Hua Chemical Industrial Co., Ltd.  completed ESBR  de-bottlenecking and expanded the capacity to 120,000 tons  per year.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Feb 2002</td>
                  <td align="left" class="gray12">TSRC completed successfully the trial run of SEBS,  brought  a new  opportunity for the diversified application of rubber products, at TPE plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Aug 2001 </td>
                  <td align="left" class="gray12">TSRC (Shanghai) Industries Ltd. commenced commercial operation to enhance compound market position in Greater China.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Mar 2001</td>
                  <td align="left" class="gray12">Rubber Business Division (SRD) and Applied Polymers Business Division (AMD) were  established as business units.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Aug 2000</td>
                  <td align="left" class="gray12">TSRC established a German joint venture, Atlantic Polymers GmbH, to promote TSRC's products in the European Market.</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td>&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>

      </div>
    </div>

    <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
