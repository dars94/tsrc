<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>About TSRC─Chairman’s Message</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　<a href="about.php">About TSRC</a>　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b02_2.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_3">
        <table width="632" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">High quality products, geographical globalization - create a new era in  the rubber industry</td>
          </tr>
          <tr>
            <td class="gray12_3">The growth of TSRC is closely related with the world's emerging market. Global consumption of synthetic rubber in 2011 reached 16,300,000 tons, of which tire demand was 10,573,000 tons accounting for 64.9%. The fact that vehicle sales in  China Mainland have already surpassed the United States of America is enough to prove the leading vision of the TSRC to actively deploy the Greater China market in recent years. Today, TSRC output and sales in China Mainland have already exceeded Taiwan. Across the neighborhood, TSRC is opening up the next hot area with rapid economic growth–India. TSRC and Indian Oil Corporation Ltd. have jointly set up Indian Synthetic Rubber Ltd. which  has raw material security and will become  operational in the first quarter of 2013. Since the supply of raw materials is guaranteed by India Oil Corp., it would become the only supplier in this growing market.</td>
          </tr>
          <tr>
            <td class="gray12_3">Facing the rapidly changing world, TSRC established a competitive strategy—creation of high value-added products and geographic globalization. Based on the China Mainland market, extending to Thailand and India, having merged a American manufacture to upgrade the technology and bought the access to the United States of America, TSRC has deployed its global business landscape in production technology, product processing, research and innovation fields, and laid a solid foundation in all domains. The spiritual and cultural values of the enterprise played a significant role for the company to achieve such remarkable success in only thirty years.</td>
          </tr>
          <tr>
            <td class="gray12_3">The professional division of labor and diverse atmosphere of free discussion that TSRC is always proud of had taken shape when the founder Mr. Glyn T.H. Ing introduced the concept of independent directors. The members of the Board of Directors cover specialists in all professional fields to provide different perspectives and views so as to develop appropriate business strategies and business objectives for TSRC. TSRC's spirit-Professional-leadership became the driving force of continuous innovation and development of the company. In the 1970s at it inception, TSRC shared the mission to improve the country's industry in the new era. TSRC also realized that, besides profit, the enterprise should assume more corporate social responsibility.  The largest shareholder of TSRC—Hao Ran Foundation under the supervision of its Board of  trustees, is engaged in many social and benevolent activities to ascertain that all dividend from TSRC would be properly used in order to achieve the goal—whatever coming from the society would be used for the society.</td>
          </tr>
          <tr>
            <td class="gray12_3">TSRC's business  territory started in Taiwan in the 1970s, entered China Mainland in the 1980s,  stood firmly in Asia in the 1990s, expanded to India and Russia since 2010, and  now in high-end markets in Europe and the United States of America.</td>
          </tr>
          <tr>
            <td class="gray12_3">TSRC has been making hard efforts  in the industry for many years, and now has finally created a new global  channel. As the leader in Asia region in the rubber industry, TSRC will continue  to move forward and strive to enhance the quality of human life.</td>
          </tr>
                   <tr>
            <td class="gray12"><table width="632" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="left" valign="top"><img src="../TW/images/about/earth.png" width="393" height="201" /></td>
                  <td align="right" valign="middle"><img src="images/about/sign02.jpg" width="141" height="90" /></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="gray12_3">&nbsp;</td>
          </tr>
          <tr>
            <td align="right" class="light_gray10">&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
