<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TSRC—Leader of Synthetic Rubber in Asia-Pacific Region</title>
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function hideFlash(){
	$('#FlashID').css('display','none');
	$('#main_block').css('display','block');
	$('#content').css('visibility','visible');
}
$(function(){
	$('#content').css('visibility','hidden');
	$('#main_block').css('display','none');
});
function MM_preloadImages() { //v3.0
	var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
	var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
	var p,i,x;	if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n); return x;
}	

function MM_swapImage() { //v3.0
	var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	 if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="logo"><img src="images/index_top.jpg" width="998" height="260" />
      <div id="top_langue">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">l<span class="w12">0</span></span><span class="gray12_2"><a href="index.php">Home<span class="w12">0</span></a></span><span class="blue12">l<span class="w12">0</span></span><span class="gray12_2"><a href="about.php">About TSRC<span class="w12">0</span></a></span><span class="blue12">l<span class="w12">0</span></span><span class="gray12_2"><a href="service.php">Investor Relations<span class="w12">0</span></a></span><span class="blue12">l<span class="w12">0</span></span><span class="gray12_2"><a href="product.php">Products<span class="w12">0</span></a></span><span class="blue12">l<span class="w12">0</span></span><span class="gray12_2"><a href="research.php">Technical Development<span class="w12">0</span></a></span><span class="blue12">l<span class="w12">0</span></span><span class="gray12_2"><a href="news.php">News<span class="w12">0</span></a></span><span class="blue12">l<span class="w12">0</span></span><span class="gray12_2"><a href="member.php">Group Member<span class="w12">0</span></a></span><span class="blue12">l</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><a href="../search.php"><img src="../TW/images/go.jpg" alt="" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <div id="top_btn02">
        <table width="600" border="0" cellpadding="0" cellspacing="0" class="blue15-b">
          <tr>
            <td><a href="../EN/product01_1.php">ESBR</a>　<a href="../EN/product01_2.php">SSBR</a>　<a href="product01_3.php">BR</a>　<a href="../EN/product01_4.php">NBR</a>　<a href="product02_1.php">TPE</a>　( <a href="../EN/product02_2.php">SBS</a> <a href="product02_2.php">SIS</a> <a href="../EN/product02_2.php">SEBS</a> )　<a href="product02_4.php">Compounds</a></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="flash">
      <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="866" height="212">
        <param name="movie" value="../TW/images/flash.swf" />
        <param name="quality" value="high" />
        <param name="wmode" value="opaque" />
        <param name="swfversion" value="6.0.65.0" />
        <!-- 此 param 標籤會提示使用 Flash Player 6.0 r65 和更新版本的使用者下載最新版本的 Flash Player。如果您不想讓使用者看到這項提示，請將其刪除。 -->
        <param name="expressinstall" value="../TW/Scripts/expressInstall.swf" />
        <!-- 下一個物件標籤僅供非 IE 瀏覽器使用。因此，請使用 IECC 將其自 IE 隱藏。 --> 
        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="../TW/images/flash.swf" width="866" height="212">
          <!--<![endif]-->
          <param name="quality" value="high" />
          <param name="wmode" value="opaque" />
          <param name="swfversion" value="6.0.65.0" />
          <param name="expressinstall" value="../TW/Scripts/expressInstall.swf" />
          <!-- 瀏覽器會為使用 Flash Player 6.0 和更早版本的使用者顯示下列替代內容。 -->
          <div>
            <h4>這個頁面上的內容需要較新版本的 Adobe Flash Player。</h4>
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="取得 Adobe Flash Player" width="112" height="33" /></a></p>
          </div>
          <!--[if !IE]>-->
        </object>
        <!--<![endif]-->
      </object>
      <table id="main_block" width="866" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="212"><img src="../TW/images/flash_01.jpg" width="212" height="212" /></td>
          <td width="6">&nbsp;</td>
          <td width="212"><img src="../TW/images/flash_02.jpg" alt="" width="212" height="212" /></td>
          <td width="6">&nbsp;</td>
          <td width="212"><img src="../TW/images/flash_03.jpg" alt="" width="212" height="212" /></td>
          <td width="6">&nbsp;</td>
          <td width="212"><img src="../TW/images/flash_04.jpg" alt="" width="212" height="212" /></td>
        </tr>
      </table>
    </div>
    <div id="content">
      <table width="866" height="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="212" align="center" valign="top"	background="../EN/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="images/title01.jpg" width="197" height="35" /></td>
              </tr>
              <tr>
                <td width="202" align="left"><span class="gray12">TSRC is actively developing environment-friendly rubber materials, such as SSBR, a highly durable, reusable, recyclable, and non-toxic material, perfect for applications such as eco-friendly tires. The Company's property sets a new standard, from which all future rubber materials must emulate<br />
                  ......</span><span class="green12"><a href="about05_5.php">&gt;more</a></span></td>
              </tr>
            </table></td>
          <td width="6">&nbsp;</td>
          <td width="212" align="center" valign="top" background="../EN/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="images/title02.jpg" alt="" width="197" height="35" /></td>
              </tr>
              <tr>
                <td align="left"><span class="gray12">TSRC, CPC and Fubon completed joint venture negotiations and signed the agreement on November 08 to invest NTD 8.6 billion to form the company; Taiwan Advanced Materials Corporation to build a C5 separation plant with annual capacity of 150 KTA</span><br />
                <span class="gray12">......</span><span class="green12"><a href="news.php">&gt;more</a></span></td>
              </tr>
            </table></td>
          <td width="6">&nbsp;</td>
          <td width="212" align="center" valign="top" background="../EN/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="images/title03.jpg" alt="" width="197" height="35" /></td>
              </tr>
              <tr>
                <td align="left"><span class="gray12">TSRC embraces the Corporate Social Responsibility.  It care about our staff, partners, and communities, and pay close attention to environmental conservation. Its ultimate goal is to create a harmonious environment through virtuous cycle of sustainable operation......</span><span class="green12"><a href="about05_3.php">&gt;more</a></span></td>
              </tr>
            </table></td>
          <td width="6">&nbsp;</td>
          <td width="212" align="center" valign="top" background="../EN/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="images/title04.jpg" alt="" width="197" height="35" /></td>
              </tr>
              <tr>
                <td align="left"><span class="gray12">TSRC's R&D team develops a variety of green manufacturing processes over the years and actually applies to production. In addition, TSRC also purchases new facilities for wastewater and gas treatment to effectively carry out energy-saving and carbon dioxide reduction plans and to fulfill its Social Responsibilities.......</span><span class="green12"><a href="about05_6.php">&gt;more</a></span></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
    <div id="down">
      <table width="866" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  class="gray12_2" >contact us ：</td>
                <td class="blue12_mail"><a href="mailto:tsrcsales@tsrc-global.com"></a><a href="mailto:tsrcsales@tsrc-global.com"> tsrcsales@tsrc-global.com</a></td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
          <td align="right" class="light_gray10">© Copyright 2011 TSRC Corporation. All Rights Reserved. </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
</html>
