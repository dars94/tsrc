<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Latest News</title>
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Scripts/jquery-1.5.2.min.js"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}
function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}
function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}
$(function(){

	$('.item_block_about').css('display','none');

	$('.down_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideDown();

		$(this).css('display','none');

	});

	$('.up_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideUp();

		$('#'+tmp_id.split('_')[0]+'_d').css('display','');

	});

})

</script>
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/news/b01_2.jpg','images/news/b02_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="../EN/index.php">Home</a></span><span class="blue12">　l　<span class="gray12_2"><a href="../EN/about.php" >About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/research.php">Technical Development</a></span><span class="blue12">　l　<a href="../EN/news.php">News</a></span><span class="gray12_2"><a href="../EN/news.php"></a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content">
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><img src="images/news/b01_2.jpg" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/news/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="news02.php"><img src="images/news/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/news/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <div id="content_main_3"> <img src="../CN/images/spacer.gif" width="1" height="20" />
        <div id="item">
          <table width="622" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80" align="left" class="gray12">2011/04/01</td>
              <td><span class="blue12_mail"><a href="#">TSRC Completes  Acquisition Dexco Polymers LP in the U.S.</a></span></td>
            </tr>
          </table>
        </div>
        <div class="item_block">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="gray12_3">[Taipei] TSRC  Corporation (&ldquo;TSRC&rdquo;, 2103 TT) announced today that it has successfully  completed the acquisition of Dexco Polymers LP (&ldquo;Dexco&rdquo;), a previous joint  venture equally owned by affiliates of ExxonMobil Chemical Company, a division  of ExxonMobil Corporation and The Dow Chemical Company.  Dexco is a global leader in  Styrene-Isoprene-Styrene (&lsquo;SIS&rdquo;) block copolymers and Styrene-Butadiene-Styrene  (&ldquo;SBS&rdquo;) block copolymers, collectively known as Styrene Block Copolymers (&ldquo;SBCs&rdquo;).</td>
            </tr>
            <tr>
              <td class="gray12_3">Headquartered in  Houston, Texas, Dexco was established in 1988 and operates a production  facility in Plaquemine, Louisiana, which has an annual production capacity of  SIS and SBS of 70 million pounds (32k MT) and 67 million pounds (30k MT),  respectively.  Post transaction, Dexco  will continue its operation in both Texas and Plaquemine.</td>
            </tr>
            <tr>
              <td class="gray12_3">The President and CEO of TSRC, Mr. Wei Hua  Tu said, &ldquo;We are very pleased to welcome Dexco to the TSRC family.  Over the last few months, we had numerous  discussions with Dexco to prepare for the integration and Dexco management  demonstrated best-in-class professionalism and execution capabilities.  TSRC will continuously support Dexco in its  long-term growth and the complementary combination of TSRC and Dexco will allow  us to expand more aggressively globally&rdquo;.</td>
            </tr>
            <tr>
              <td class="gray12_3">Together, TSRC and  Dexco plan to further strengthen their leading position in the global polymer  manufacturing industry.  The completion  of the acquisition brings the two success stories together and marks the  commencement of the integration between the two companies.  TSRC will become a fully integrated global  supplier os SBC products and the combined company will continue to innovate in  differentiated and higher value-added products and offer a full product  portfolio to customers worldwide.</td>
            </tr>
          </table>
        <a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn02" /></a></div>
        <div id="item">
          <table width="622" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80" align="left" valign="top" class="gray12">2010/12/24</td>
              <td><span class="blue12_mail"><a href="#">TSRC Announces Agreement to Acquire Dexco Polymers in the U.S.<img src="../TW/images/down.png" alt="" name="btn01" width="17" height="14" border="0" id="btn" /></a></span></td>
            </tr>
          </table>
        </div>
        <div id="item_block"></div>
        <div id="item">
          <table width="622" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80" align="left" valign="top" class="gray12">2010/05/07</td>
              <td><span class="blue12_mail"><a href="#">LANXESS and TSRC to create rubber joint venture in China</a></span><a href="#"><img src="../TW/images/down.png" alt="" name="btn01" width="17" height="14" border="0" id="btn01" /></a></td>
            </tr>
          </table>
        </div>
        <div id="item_block"></div>
        <div id="item">
          <table width="622" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80" align="left" valign="top" class="gray12">2010/04/03 </td>
              <td><span class="blue12_mail"><a href="#">TSRC Corporation, Indian Oil Corporation, Marubeni Joint Venture to Establish Styrene-Butadiene Manufacturing Facility</a></span><a href="#"><img src="../TW/images/down.png" alt="" name="btn01" width="17" height="14" border="0" id="btn01" /></a></td>
            </tr>
          </table>
        </div>
        <div id="item_block"></div>
        <div id="item">
          <table width="622" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80" align="left" class="gray12">2009/05/27</td>
              <td><span class="blue12_mail"><a href="#">TSRC Corporation Signs SBC Technology License Agreement with RUSTEP LLC</a></span><a href="#"><img src="../TW/images/down.png" alt="" name="btn01" width="17" height="14" border="0" id="btn01" /></a></td>
            </tr>
          </table>
        </div>
        <div id="item_block"></div>
      </div>
    </div>
  </div>
</div>
<div id="footer">
  <table width="998" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="833" align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
</html>
