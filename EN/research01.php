<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/research_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>New Product Development</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){
	$('#item_block_1').css('display','none');
	$('#btn01_d').click(function(){
		$('#item_block_1').slideDown();
		$(this).css('display','none');
	});
	$('#btn01_u').click(function(){
		$('#item_block_1').slideUp();
		$('#btn01_d').css('display','');
	});
})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/research/b01_2.jpg','images/research/b02_2.jpg','images/research/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="about.php">About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="blue12"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><img src="images/research/b01_2.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/research/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="research02.php"><img src="images/research/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/research/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="research03.php"><img src="images/research/b03.jpg" name="Image3" width="171" height="38" id="Image3" onmouseover="MM_swapImage('Image3','','images/research/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="top" -->
      <div id="content_top">
        <table width="688" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="184"><img src="../TW/images/research/pic01_1.jpg" alt="" width="184" height="198" /></td>
            <td width="5">&nbsp;</td>
            <td width="310"><img src="../TW/images/research/pic02_1.jpg" width="310" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="184"><img src="../TW/images/research/pic03_1.jpg" width="184" height="198" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable -->
      <div id="content_main"><!-- InstanceBeginEditable name="main" -->
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td class="gray12_3">New product development involves new product concepts, new processes, and new applications. Its success requires integrating resources at all levels, including marketing, production, research and development, and interdepartmental collaboration. TSRC customer-focused R&D team, through continuous interaction with customers, is able to develop products that meets variety of customers' needs. TSRC consistently makes adjustment to its development process in response to the ever changing environment. The following are some of milestones TSRC has achieved over the years:</td>
          </tr>
          <tr>
            <td align="center" class="gray12"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="50%" align="left">• Development of environmentally friendly SBR and BR</td>
                </tr>
                <tr>
                  <td align="left">• Compliance with key global customer requirements in high functionality SIS, SBS, and SEBS</td>
                </tr>
                <tr>
                  <td align="left">• Highly innovative SEBS production and process technology</td>
                </tr>
                <tr>
                  <td align="left">• FDA compliant SIS and SEBS</td>
                </tr>
                <tr>
                  <td align="left">• High oil resistance NBR rubber High-performance star SIS products</td>
                </tr>
                <tr>
                  <td align="left">• High flexibility SIBS film product</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="gray12">Currently,  TSRC is working on several projects that encompass special <span class="gray12-b">performance, high  value-added, and environmentally friendly products.</span></td>
          </tr>
          <tr>
            <td class="green12"><div id="item_2">2010 high-performance green energy R & D progress SSBR rubber<a href="#"><img src="../TW/images/down.png" name="btn01" width="17" height="14" id="btn01_d" /></a></div></td>
          </tr>
          <tr>
            <td><div id="item_block_1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="gray12_3">TSRC is currently developing "solution polystyrene - butadiene rubber SSBR" in response to the industry and customer demand for environmental friendly and high performance product: </td>
                  </tr>
                  <tr>
                    <td class="gray12_3">Recently, environmental friendly and energy efficient tires (green tire) have become the general trend; particularly Europe require all tires be marked with fuel efficiency, wet grip performance, rolling resistance, noise level, etc. by 2012.  The green tire requirement has prompted the use of SSBR/ Silica formulation in all tires manufactured beyond 2012. Failure to adhere to the EU requirement is likely to be met with market exclusion. Currently, none of the domestic manufacturers is capable of producing SSBR.To overcome the dependency of import, TSRC has initiated an alliance to form a complete SSBR value chain to develop its own SSBR/Silica formulated, environmentally friendly, energy-saving green tires. </td>
                  </tr>
                  <tr>
                    <td class="gray12_3">TSRC leads the alliance by providing SSBR materials while the tire manufacturers develop and design a series of energy efficient green tires under the open innovation methodology. With this model, TSRC is hoping to improve the competitiveness of our tire industry.<a href="#"><img src="../TW/images/up.png" name="btn02" width="17" height="14" id="btn01_u" /></a></td>
                  </tr>
                </table>
              </div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        </table>
        <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
