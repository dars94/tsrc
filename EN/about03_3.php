     <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" id='btn_1' coords="3,2,168,24" />

                  <area shape="rect" id='btn_2' coords="174,2,342,25" />

                  <area shape="rect" id='btn_3' coords="348,1,513,24" />

                  <area shape="rect" id='btn_4' coords="519,3,683,25" />

                  </map></td>

              </tr>

            <tr>

              <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="65" align="left" valign="top" class="green12">April 1988</td>
                  <td width="527" align="left" class="gray12">Thermoplastic Elastomer (&ldquo;TPE&rdquo;) plant was constructed and the trial run was completed.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">June 1985</td>
                  <td align="left" class="gray12">The gas treatment equipment for  BR plant was installed to prevent pollution and preserve ecological environment.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 1984</td>
                  <td align="left" class="gray12">TPE  plant construction started for 20,000 tons per year of butadiene-styrene block copolymer (&ldquo;SBC&rdquo;).</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sept 1982</td>
                  <td align="left" class="gray12">TSRC was listed on Taiwan Stock  Exchange.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">June 1982</td>
                  <td align="left" class="gray12">A 40,000 tons  per year of Polybutadiene Rubber (&ldquo;BR&rdquo;)  plant was on stream.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">&nbsp;</td>
                  <td align="left" class="gray12">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">&nbsp;</td>
                  <td align="left" class="gray12">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">&nbsp;</td>
                  <td align="left" class="gray12">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">&nbsp;</td>
                  <td align="left" class="gray12">&nbsp;</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td align="center">&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>