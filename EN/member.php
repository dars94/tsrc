<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/member_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Group Member─Head Office TSRC Corporation</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/member/b01_2.jpg','images/member/b02_2.jpg','images/member/b03_2.jpg','images/member/b04_2.jpg','images/member/b05_2.jpg','images/member/b06_2.jpg','images/member/b07_2.jpg','images/member/b08_2.jpg','images/member/b09_2.jpg','images/member/b10_2.jpg','images/member/b11_2.jpg')">
<div id="wrap_2">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　<a href="about.php">About TSRC</a>　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left_2">
        <table width="224" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><img src="images/member/b01_2.jpg" name="Image1" width="224" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/member/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member02.php"><img src="images/member/b02.jpg" width="224" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/member/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member03.php"><img src="images/member/b03.jpg" width="224" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/member/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member04.php"><img src="images/member/b04.jpg" name="Image4" width="224" height="38" id="Image4" onmouseover="MM_swapImage('Image4','','images/member/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member05.php"><img src="images/member/b05.jpg" name="Image5" width="224" height="38" id="Image5" onmouseover="MM_swapImage('Image5','','images/member/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member06.php"><img src="images/member/b06.jpg" width="224" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/member/b06_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member07.php"><img src="images/member/b07.jpg" width="224" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/member/b07_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr><tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member08.php"><img src="images/member/b08.jpg" name="Image9" width="224" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/member/b08_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member09.php"><img src="images/member/b09.jpg" width="224" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/member/b09_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member10.php"><img src="images/member/b10.jpg" width="224" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/member/b10_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member11.php"><img src="images/member/b11.jpg" width="224" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/member/b11_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="82" /></td>
          </tr>
          <tr>
            <td class="gray12_2" align="center">contact us ：</td>
          </tr>
          <tr>
            <td class="blue12" align="center"><a href="mailto:tsrcsales@tsrc-global.com">tsrcsales@tsrc-global.com</a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable -->
      <div id="content_top_3"><img src="../TW/images/member/pic01.jpg" width="635" height="110" /></div>
      <div id="content_main_2"><!-- InstanceBeginEditable name="main" -->

        <table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">

          <tr>

            <td>&nbsp;</td>

          </tr>

          <tr>

            <td align="left"><table width="584" border="0" cellspacing="0" cellpadding="0">

              <tr>
                
                <td width="200" align="left"><img src="../TW/images/member/company_1.jpg" width="190" height="110" /></td>
                
                <td width="366" align="left" valign="top" class="blue14-b"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td scope="col">Head Office TSRC Corporation</td>
                  </tr>
                  <tr>
                    <td class="gray12">18F, 95, Sec. 2, Dunhua S. Rd., Taipei City 106,  Taiwan R.O.C.</td>
                  </tr>
                  <tr>
                    <td class="gray12">Tel：+886(2) 3701 6000</td>
                  </tr>
                  <tr>
                    <td class="gray12"> Fax：+886(2) 3701 6868</td>
                  </tr>
                </table></td>
                
              </tr>

              <tr>
                
                <td>&nbsp;</td>
                
                <td>&nbsp;</td>
                
              </tr>

              <tr>

                <td width="200" rowspan="3" align="left" valign="top"><img src="../TW/images/member/company_2.jpg" width="190" height="143" /></td>

                <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="gray12">
                  <tr>
                    <td width="35%" align="left" valign="top">Kaohsiung Factory:</td>
                    <td width="85%">No.2 Singgong Rd., Dashe Dist., Kaohsiung City 815,  Taiwan R.O.C.</td>
                  </tr>
                  <tr>
                    <td width="14%">&nbsp;</td>
                    <td>Tel：+886(7) 351 3811</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>Fax：+886 (7) 351 4705</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">Gangshan Factory:</td>
                    <td>No.39, Bengong 1st Rd., Gangshan Dist.,  Kaohsiung City 820, Taiwan, R.O.C.</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>Tel：+886(7) 623 3005</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>Fax：+886 (7) 622 5481</td>
                  </tr>
                </table></td>

              </tr>

              <tr>

                <td>&nbsp;</td>

              </tr>

              <tr>

                <td valign="top">&nbsp;</td>

              </tr>

            </table></td>

          </tr>

          <tr>

            <td>&nbsp;</td>

          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>

          <tr>

            <td>&nbsp;</td>

          </tr>

        </table>

      <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div>
<div id="footer02">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="635" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="25" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
