<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
<link href="css/text.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="385" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ball">
        <tr>
          <td align="center"><table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right"><a href="#"><img class='close_btn' src="images/about/close.jpg" width="25" height="25" /></a></td>
              </tr>
              <tr>
                <td class="green12_3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left">The management of greenhouse gas emission is not  only a global issue but also a challenge for the petrochemical  industry. By upholding the concept of &quot;cherishing the resources of the earth and fulfilling the responsibilities of environmental protection&rdquo;, TSRC has made unremitting efforts for energy saving, resources &amp; raw materials  recycling, and producing efficiency improving since its foundation in 1973. To cooperate with the promotion of  greenhouse gas reduction plans  by competent  authorities, TSRC has taken the following measures:</td>
                  </tr>
                  <tr>
                    <td align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="5%" align="left" valign="top">(1)</td>
                        <td width="95%" align="left" valign="top">Under the authorization of Economic Ministry, Taiwan Green Productivity Foundation has verified  that TSRC has reduced 37,520 tons of CO2 equivalent emission through its adoption of &ldquo;Voluntary Energy Conservation and Greenhouse Gas  Emission Reduction&rdquo; during 2004~2008.</td>
                        </tr>
                      <tr>
                        <td align="left" valign="top">(2)</td>
                        <td align="left" valign="top"> In order to  reach the goal of voluntary greenhouse gas emission reduction, TSRC has signed  the &ldquo;Agreement on Voluntary Greenhouse Gas Emission  Reduction&rdquo; with the Economic Ministry in 2010. It is  expected that TSRC can reduce another 4,022 tons of CO2 equivalent emission during 2011~ 2015.</td>
                        </tr>
                      <tr>
                        <td align="left" valign="top">(3)</td>
                        <td align="left" valign="top">TSRC plans to obtain the ISO 14064-1 certificate  of greenhouse gas for its Kaohsiung and  Gangshan plants in 2011 to serve as a reference  for the follow-up greenhouse gas emission reductions.</td>
                        </tr>
                      </table></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
