     <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" coords="3,2,168,24" id='btn_1' />

                  <area shape="rect" id='btn_2' coords="174,2,342,25" />

                  <area shape="rect" id='btn_3' coords="348,1,513,24" />

                  <area shape="rect" id='btn_4' coords="519,3,683,25" />

                  </map></td>

              </tr>

            <tr>

              <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="65" align="left" valign="top" class="green12">Nov 2011</td>
                  <td width="527" align="left" class="gray12">TSRC, CPC and Fubon completed joint venture negotiations and signed the  agreement to form Taiwan Advanced Materials Corporation.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2011</td>
                  <td width="527" align="left" class="gray12">TSRC acquired 100% stake in Dexco  Polymers.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 2010</td>
                  <td align="left" class="gray12">TSRC established  a joint venture, LANXESS-TSRC (Nantong) Chemical Industrial Co., Ltd.,  with Lanxess A.G. of German in China and started a 30,000 tons per year of NBR  plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2010</td>
                  <td align="left" class="gray12">TSRC established  a joint venture, Indian  Synthetic Rubber Ltd, with  Indian Oil  Corporation Ltd. and Marubeni Corporation in Panipat,  India and started a 120,000 tons per year of ESBR  plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Dec 2009</td>
                  <td align="left" class="gray12">TSRC obtained the certifications from  Occupational Health and  Safety Advisory Services (&ldquo;OHSAS&rdquo;) 18001 and Taiwan Occupational  Safety and Health Management System (&ldquo;TOSHMS&rdquo;)</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 2009</td>
                  <td align="left" class="gray12">TSRC signed the styrene-butadiene-styrene  (&ldquo;SBS&rdquo;) technology license agreement with Russian RUSTEP LLC/ OJSC SIBUR HOLDING.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">May 2009 </td>
                  <td align="left" class="gray12">TSRC-UBE  (Nantong) Chemical  Industrial Company  Limited started BR production.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2008</td>
                  <td align="left" class="gray12">TSRC started operation of Singapore's Polybus Corporation Pte. Ltd.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2008 </td>
                  <td align="left" class="gray12">TSRC (Nantong) Industries Ltd. started SEBS production. </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Nov 2007</td>
                  <td align="left" class="gray12">Atlantic  Polymers GmbH, in Germany, was terminated.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sep 2007 </td>
                  <td align="left" class="gray12">TSRC (Jinan) Industries Ltd. started compound production.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Dec 2006</td>
                  <td align="left" class="gray12">TSRC established TSRC-UBE (Nantong) Chemical  Industrial Company  Limited in  Nantong, China with  50,000 tons per year of BR plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sep 2006</td>
                  <td align="left" class="gray12">TSRC established TSRC (Jinan) Industries Ltd. in Jinan, China with  a 5,000 tons per year of compound plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Sep 2006</td>
                  <td align="left" class="gray12">TSRC established TSRC (Nantong) Industries  Ltd. in Nantong, China    with 20,000 tons  per year of SEBS plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Dec 2005</td>
                  <td align="left" class="gray12">The building of TPE Application Center in Gangshan, Taiwan was completed.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2005</td>
                  <td align="left" class="gray12">Shen  Hua Chemical Industrial Co., Ltd. completed ESBR expansion to  180,000 tons  per year.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2005</td>
                  <td align="left" class="gray12">The compound production  lines in Gangshan plant was on stream. </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Oct 2004</td>
                  <td align="left" class="gray12">The completion of Taipei  new offic</a>.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Aug 2004</td>
                  <td align="left" class="gray12">TSRC started the construction of compound plant in Gangshan, Taiwan.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Jul 2004</td>
                  <td align="left" class="gray12">TSRC started to build TPE Application  Center.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Apr 2004</td>
                  <td align="left" class="gray12">Nantong Qix Storage Co., Ltd. commenced  commercial operation.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Oct 2003</td>
                  <td align="left" class="gray12">Shen  Hua Chemical Industrial Co., Ltd.  completed ESBR  de-bottlenecking and expanded the capacity to 120,000 tons  per year.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Feb 2002</td>
                  <td align="left" class="gray12">TSRC completed successfully the trial run of SEBS,  brought  a new  opportunity for the diversified application of rubber products, at TPE plant.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Aug 2001 </td>
                  <td align="left" class="gray12">TSRC (Shanghai) Industries Ltd. commenced commercial operation to enhance compound market position in Greater China.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Mar 2001</td>
                  <td align="left" class="gray12">Rubber Business Division (SRD) and Applied Polymers Business Division (AMD) were  established as business units.</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">Aug 2000</td>
                  <td align="left" class="gray12">TSRC established a German joint venture, Atlantic Polymers GmbH, to promote TSRC’s products in the European Market.</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td align="center">&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>