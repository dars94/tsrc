<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/about_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>About TSRC─Maijor Business</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){

	$('.item_block_about').css('display','none');

	$('.down_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideDown();

		$(this).css('display','none');

	});

	$('.up_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideUp();

		$('#'+tmp_id.split('_')[0]+'_d').css('display','');

	});

})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　<a href="about.php">About TSRC</a>　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b04_2.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top_5"><img src="../TW/images/about/pic01_4_1.jpg" width="688" height="188" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main">
        <div id="main_about">
          <table width="632" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td width="130" align="left" valign="top"><table width="100" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left" valign="top"><span class="gray12">Synthetic Rubber Business</span></td>
                  </tr>
                </table></td>
              <td width="510" align="left" valign="top"><div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">ESBR</span><span class="gray12"> － Better abrasion resistance and processability than natural rubber <a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn1_d" /></a></span></div>
                  <div class="item_block_about" id="btn1_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498"  class="gray12">TAIPOL <span class="blue12">ESBR</span> offers better resistance to abrasion and processability than natural rubber. Unlike natural rubber, its quality and quantity are not affected by climatic and geographical factors, which make <span class="blue12">ESBR</span> the most widely used synthetic rubber today. Additionally, it is also the main raw material for automotive tires, shoe soles, conveyor belts, rubber belts, military caterpillar and tow trucks tracks, sports equipment, toys, etc. 
                          TSRC has a production capacity of 100,000 tons of <span class="blue12">ESBR</span> from the Kaohsiung plant, Taiwan. The Shen-Hua Chemical Industrial Co., Ltd. in Nantong Economic and Technological Development Zone, Jiangsu Province, China is a joint venture of TSRC, Japanese Marubeni Corporation, and Nantong Petroleum & Chemical Corporation, which has an annual output of 100,000 tons of <span class="blue12">ESBR</span>. More recently, it has been expanded to 180,000 tons to meet the growing demand of the Chinese market. In addition, Indian Synthetic Rubber Ltd. in India – a joint venture of TSRC, Indian Oil Corporation Ltd., and Marubeni Corporation – projects an annual output of 120,000 tons of <span class="blue12">ESBR</span>, and is expected to go into full production in the first quarter of 2013 to meet the growing demand of Indian market.</span><a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn1_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /><br />
                  </div>
                </div>
                <div id="about_block">
                  <div id="item_about">
                    <table width="510" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="105"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">SSBR</span><span class="gray12"> －</span></td>
                              <td><span class="gray12">Low rolling resistance,&nbsp;good wet grip resistance and  abrasion resistance <a href="#"></a></span></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td><a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn2_d" /></a></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                  </div>
                  <div class="item_block_about" id="btn2_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12" >The property of the SSBR differs from the traditional rubber in that its property can be customized according to the processing and application demands. <span class="blue12">SSBR</span> is mainly used in energy-conserving (low rolling resistance), high performance, and all-season tires. These tires reduce fuel consumption and carbon dioxide emission in line with the environment protection concerns, and provide added value to end users. <span class="blue12">SSBR</span> is also widely applied in shoe soles, conveyor belts, rubber belts, sports equipment, toys, etc. In preparation for the increasing demand on <span class="blue12">SSBR</span>, TSRC has established a new <span class="blue12">SSBR</span> plant in Kaohsiung with an expected annual output of 30,000 tons. The plant is scheduled to go into operation in the fourth quarter of 2011.<a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn2_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="86"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">BR</span><span class="gray12"> － </span></td>
                        <td width="420"><span class="gray12"> Exceptionally elastic, abrasion resistance, and low-temperature performance <a href="#"></a></span></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn3_d" /></a></td>
                      </tr>
                    </table>
                  </div>
                  <div class="item_block_about" id="btn3_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12"><span class="blue12">BR</span> is characterized by good elasticity and resistance to abrasion  and low temperature. It is mainly used in the production of plastic modification for HIPS and high-speed radial tires. With these features,  it can make tires safer and more comfortable to ride on and when <span class="blue12">BR</span> is applied in plastic  modification, HIPS offers better processability and resistance to impact. The wide application  of HIPS in PC, home appliances, and electronics contributes to the growing  demand for <span class="blue12">BR</span> in the market, which in turn drives TSRC's development.  Currently, TSRC <span class="blue12">BR</span> plant in Kaohsiung has an  annual output of 60,000 tons. Moreover, the Thai Synthetic Rubber Ltd. in Thailand- a joint venture of  TSRC, UBE Industries, Ltd., Japan, and Marubeni  Corporation - also offers an additional  annual output of 72,000 tons.</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td class="gray12">To meet the growing demands of clients and increase its competitiveness in the Asia-Pacific market, TSRC aggressively builds up its supply network in China. For example, TSRC-UBE (Nantong) Chemical Industrial Co., Ltd. is a joint venture of TSRC, Japanese UBE Industries, Ltd. and Marubeni Corporation in Nantong Economic and Technological Development Zone in Jiangsu Province, China. The current annual capacity is 50,000 tons, which is expected to reach 72,000 tons in 2012.<a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn3_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">NBR</span><span class="gray12"> － Excellent resistance to oil, solvent, heat, and aging<a href="#"> <img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn4_d" /></a></span></div>
                  <div class="item_block_about" id="btn4_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12"><span class="blue12">NBR</span> is characterized by excellent resistance to oil, solvent, heat and aging. It is mainly used in daily commodities and industrial products such as oil hoses, oil seals, rollers, industrial gaskets, belts, shoe soles, rubber plastic foam sheet, conveyor belts etc. In recent years, the considerable increase of automotive production in China has driven the huge demand of synthetic rubber such as <span class="blue12">NBR</span>. LANXESS-TSRC (Nantong) Chemical Industrial Co., Ltd., a joint venture between TSRC and German Lanxess, has an annual capacity of 30,000 tons of <span class="blue12">NBR</span>. The plant located in Nantong Economic Technological Development Zone of Jiangsu Province, China is expected to go into production in 2012.<a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn4_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td width="130" align="left" valign="top"><table width="100" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left" valign="top"><span class="gray12">Applied Polymers Business</span></td>
                  </tr>
                </table></td>
              <td><div id="about_block">
                  <div id="item_about">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="58%"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="gray12"><span class="blue12">TPE thermoplastic rubber SBS, SIS, SEBS － </span></span></td>
                        <td width="42%" class="gray12"> Rubber with high application value </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><span class="gray12">and high quality<a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn5_d" /></a></span></td>
                      </tr>
                    </table>
                  </div>
                  <div class="item_block_about" id="btn5_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td rowspan="3" align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12">TAIPOL®<span class="blue12">TPE</span>(Thermoplastic Elastomer) is composed of styrene  and other monomers to form Styrenic Block Copolymer (SBC). TSRC's product  Taipol® <span class="blue12">TPE</span> includes</td>
                      </tr>
                      <tr>
                        <td class="gray12">1. SBS with butadiene as compounding monomers</td>
                      </tr>
                      <tr>
                        <td class="gray12">2. SIS series with isoprene as compounding monomers</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td class="gray12">SEBS, the second generation of hydrogenated SBS products  with enhanced better durability and  functions.</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td class="gray12">With the mixed properties of plastics and rubber, thermoplastic <span class="blue12">TPE</span> is widely used in high-grade shoe soles, asphalt modification, plastic modification, and adhesive, etc. It can be directly processed by injection, recycled to lower costs, and used to produce anti-skip soles, which is an important substitution for PVC and traditional vulcanized rubber. The <span class="blue12">TPE</span> has the characteristics of low-viscosity, high-modifiability and blendability, which makes it ideal as a modifier of building materials, asphalt, and water-proofing materials. For example: the load resistance property of the asphalt paved road, which is brittle in the winter and soft in the summer, can be improved with <span class="blue12">TPE</span> as an additive.</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td class="gray12">The  impact resistance and low temperature resistance can be significantly improved by introducing a small amount of <span class="blue12">TPE</span> in polypropylene, polyethylene,  polystyrene and other polymers, which can be applied in medical devices, automotive parts, sports  equipment, toys and industrial products.</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td class="gray12"><span class="blue12">TPE</span> also processes excellent compatibility with tackifiers. If blended after melting, it can be used to  produce adhesives with good adhesive  strength and still meets  the environmental requirements. The resulting blends can be used in  pressure-sensitive self-adhesive tapes, labels,   diapers and other hygiene products.</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td class="gray12">The  Taipol® <span class="blue12">TPE</span>, with the inclusion of SBS, SIS and SEBS can meet the needs of  different clients in different fields and thus boasts an unique competitive advantage in the industry that requires high level  of customized formulation.<a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn5_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"> <img src="../TW/images/about/blend.png" alt="" width="88" height="17" /><span class="gray12"> － </span><span class="gray12">An environmentally friendly material for the comfort of the modern living<a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn6_d" /></a></span> <span class="gray12"><a href="#"></a></span></div>
                  <div class="item_block_about" id="btn6_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12">Thermoplastic <span class="blue12">TPR</span> related products has gained wide adoptions largely because of its environmental friendliness. In response to this growing demand, TSRC launched <span class="blue12">T-BLEND®</span> product, which is blended material with <span class="blue12">SBS</span> or <span class="blue12">SEBS</span> as major ingredients It boasts a number of advantages including good tactile texture, customizable hardness, excellent weather resistance, low temperature endurance, and thermal-plasticity. <span class="blue12">T-BLEND®</span> product can provide clients with customized formulation. It is advantageous over other <span class="blue12">TPR</span> is that it can be injection molded or extrude and that its waste by-product can be recycled to lower the overall production cost.</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td class="gray12">The nature of the <span class="blue12">T-BLEND®</span> environmentally friendliness and versatility makes it a perfect material for mass production of molded goods, such as daily applicant parts, super-soft gels, low-voltage, flame retardant halogen-free cable products, SEBS foaming agents, window, doors, and refrigerator seals, flexible films, etc. <span class="blue12">T-BLEND®</span> material is currently being produced in Kaohsiung of Taiwan, Songjiang of Shanghai, and Jinan of Shandong Province at a combined annual output at about 27,000 tons. In addition to its production plants, TSRC also set up R&D centers in Taiwan, Eastern China, Northern China, and Southern China to facilitate the development of new products and new materials as part of its strategy to expand its business territory to Asia Pacific region.</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td class="gray12">TSRC  made a series of investments as a way to provide the best possible  communications and services between the company and its clients. One is the <span class="blue12">TPE</span> Application Center in Gangshan and Kaohsiung and the other is the Customer  Service Center in Shanghai. In addition to its sophisticated research  apparatus, its R&amp;D team also collaborates with oversea academic researchers  and specialty research institutions to further expand SEBS' formulation and  applications. By actively develops new and high value-added materials and  technology, TSRC will be able to provide a whole array of consulting, design,  and R&amp;D services in the applied materials to accommodate all its customers'  needs.<a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn6_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
