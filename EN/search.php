<?php
require_once('../db.php');

/* 刪除舊cache */
$db->search_cache()->where("datetime<'".date('Y-m-d')."'")->delete();
$keyword = htmlentities(trim($_GET['keyword']),null,'utf-8');
$lang = 'en';
$page_size = 10;
if(!empty($keyword)){
  $data = $db->search_cache()
                      ->where('keyword',$keyword)
                      ->where('lang',$lang)
                      ->where('datetime',date('Y-m-d'))
                      ->order('id DESC');
  if(count($data) < 1){
    /* 最新消息 */
    $data = $db->boards()->where("title LIKE '%".$keyword."%' or content LIKE '%".$keyword."%'")->where('lang',$lang);
    if(count($data)){
      $data = $data->fetch();
      $content = trim(str_replace(' ','',preg_replace('/&nbsp;/',"",$data['content'])));
      $db->search_cache()->insert(array(
        'keyword' => $keyword,
        'lang' => $lang,
        'page_name' => 'news.php',
        'content' => $data['title'].mb_substr($conent,0,100,'utf8'),
        'datetime' => date('Y-m-d')
      ));
    }

    /* 展覽 */
    $data = $db->shows()->where("title LIKE '%".$keyword."%'")->where('lang',$lang);
    if(count($data)){
      $data = $data->fetch();
      $content = trim(str_replace(' ','',preg_replace('/&nbsp;/',"",$data['title'])));
      $db->search_cache()->insert(array(
        'keyword' => $keyword,
        'lang' => $lang,
        'page_name' => 'news02.php',
        'content' => $data['title'],
        'datetime' => date('Y-m-d')
      ));
    }

    /* 頁面資料 */
    $data = $db->pages()->where("content LIKE '%".$keyword."%'")->where('lang',$lang);
    foreach($data as $t){
      $tmp = array(
        'keyword' => $keyword,
        'lang' => $lang,
        'page_name' => $t['page_name'],
        'content' => mb_substr($t['content'],0,200,'utf8'),
        'datetime' => date('Y-m-d')
      );
      $db->search_cache()->insert($tmp);
    }
    $data = $db->search_cache()
                      ->where('keyword',$keyword)
                      ->where('lang',$lang)
                      ->where('datetime',date('Y-m-d'))
                      ->order('id DESC');
  }
  $totalResult = count($data);
  $totalPage = ceil(count($data)/$page_size);
  if(empty($_GET['page'])){
    $page = 1;
  }else{
    $page = $_GET['page'];
  }
  if(($page - 1)<1){
    $prev_page = 1;
  }else{
    $prev_page = ($page - 1);
  }
  if(($page + 1) > $totalPage){
    $next_page = $totalPage;
  }else{
    $next_page = ($page + 1);
  }
  $data = $db->search_cache()
                      ->where('keyword',$keyword)
                      ->where('lang',$lang)
                      ->where('datetime',date('Y-m-d'))
                      ->order('id DESC')
                      ->limit($page_size,(($page-1)*$page_size));
}else{
  echo 'no keyword.';
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/search_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Search Results</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.5.2.min.js"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#search_form').submit();
  });
  $('#Image1').click(function(){
    $('#search_form2').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form method="get" action="search.php" id="search_form">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" value='<?php echo $keyword?>' />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="about.php">About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_3"><table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">Search</td>
          </tr>
          <tr>
            <td class="gray12"><table border="0" align="left" cellpadding="2" cellspacing="0">
                <tr>
                  <td align="left" class="light_gray12">Key word search：</td>
                  <td><form method="get" action="search.php" id="search_form2">
                      <label for="textfield2"></label>
                      <input name="keyword" type="text" id="textfield2" value='<?php echo $keyword?>' />
                    </form></td>
                  <td><img src="../TW/images/go.jpg" alt="" name="Image1" width="23" height="16" id="Image1" onmouseover="MM_swapImage('Image1','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="gray12">&nbsp;</td>
          </tr>
          <tr>
            <td><span class="blue14-b">Search Results</span></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="42%" align="left"><span class="gray12">Found <?php echo $totalResult?> results of </span><span class="blue12"><?php echo $keyword?></span> <span class="gray12">query</span></td>
                  <td width="50%" align="right" class="gray12">
                  <a href="search.php?keyword=<?php echo $keyword?>&page=1">《 First</a>&nbsp;
                  <a href="search.php?keyword=<?php echo $keyword?>&page=<?php echo $prev_page?>">〈 Prev</a>&nbsp;
                  <?php
                  $i = 1;
                  while($i <= $totalPage){
                    if($page == $i){
                      echo '<span class="blue12">'.$i.'</span>&nbsp;';
                    }else{
                      echo '<a href="search.php?keyword='.$keyword.'&page='.$i.'">'.$i.'</a>';
                    }
                    $i++;
                  }
                  ?>
                
                  <a href="search.php?keyword=<?php echo $keyword?>&page=<?php echo $next_page?>">Next 〉</a>&nbsp;
                  <a href="search.php?keyword=<?php echo $keyword?>&page=<?php echo $totalPage?>">Last 》</a>&nbsp;
                  </td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="gray12"><img src="images/line03.png" alt="" width="632" height="5" /></td>
          </tr>
          <?php
        if($totalResult < 1){
          echo '<tr><td>no results.</td></tr>';
        }else{
          $i = 1;
          foreach($data as $t){
            echo '<tr>';
            echo '<td class="green12"><a href="'.$t['page_name'].'">'.($i+(($page-1)*$page_size)).'.'.$t['page_name'].'</a></td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td class="gray12">'.mb_substr($t['content'],0,100,'utf8').'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>&nbsp;</td>';
            echo '</tr>';
            $i++;
          }
        }
        ?>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="42%" align="left"><span class="gray12">Found <?php echo $totalResult?> results of </span><span class="blue12"><?php echo $keyword?></span> <span class="gray12">query</span></td>
                <td width="50%" align="right" class="gray12">
                  <a href="search.php?keyword=<?php echo $keyword?>&page=1">《 First</a>&nbsp;
                  <a href="search.php?keyword=<?php echo $keyword?>&page=<?php echo $prev_page?>">〈 Prev</a>&nbsp;
                  <?php
                  $i = 1;
                  while($i <= $totalPage){
                    if($page == $i){
                      echo '<span class="blue12">'.$i.'</span>&nbsp;';
                    }else{
                      echo '<a href="search.php?keyword='.$keyword.'&page='.$i.'">'.$i.'</a>';
                    }
                    $i++;
                  }
                  ?>
                
                  <a href="search.php?keyword=<?php echo $keyword?>&page=<?php echo $next_page?>">Next 〉</a>&nbsp;
                  <a href="search.php?keyword=<?php echo $keyword?>&page=<?php echo $totalPage?>">Last 》</a>&nbsp;
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><img src="images/line03.png" alt="" width="632" height="5" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
      </table></div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
