<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/product_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Products─Synthetic Rubber Business</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/product/b01_2.jpg','images/product/b01-1_2.jpg','images/product/b01-2_2.jpg','images/product/b01-3_2.jpg','images/product/b01-4_2.jpg','images/product/b02_2.jpg','images/product/b02-1_2.jpg','images/product/b02-2_2.jpg','images/product/b02-3_2.jpg','images/product/b02-4_2.jpg','images/product/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="about.php">About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="blue12"><a href="product.php">Products</a>　l　</span><span class="gray12_2"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product01.php"><img src="images/product/b01_3.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/product/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_1.php"><img src="images/product/b01-1.jpg" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/product/b01-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_2.php"><img src="images/product/b01-2.jpg" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/product/b01-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_3.php"><img src="images/product/b01-3.jpg" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/product/b01-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_4.php"><img src="images/product/b01-4_3.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/product/b01-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product02.php"><img src="images/product/b02.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/product/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_1.php"><img src="images/product/b02-1.jpg" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/product/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_2.php"><img src="images/product/b02-2.jpg" width="171" height="22" border="0" id="Image9" onmouseover="MM_swapImage('Image9','','images/product/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_3.php"><img src="images/product/b02-3.jpg" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/product/b02-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_4.php"><img src="images/product/b02-4.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/product/b02-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product03.php"><img src="images/product/b03.jpg" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/product/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_3">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td align="center" ><table width="632" class="table_5" summary="Submitted table designs">
              <thead>
                <tr  >
                  <th width="80" scope="col"><span class="gray12_4">Grade</span></th>
                  <th width="208" scope="col"><span class="gray12_4">Polymerization System</span></th>
                  <th width="140" scope="col"><span class="gray12_4">Extender</span></th>
                  <th width="200" scope="col"><span class="gray12_4">Package</span></th>
                </tr>
              </thead>
              <tfoot>
              </tfoot>
              <tbody>
                <tr class="gray12" >
                  <td rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF" scope="row"><span class="light_blue12">NBR</span><br />
                    <span class="blue12">#8051</span></td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">Cold process,<br />
                    Emulsion Polymerization </td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">Paper bag   35Kg/bale</td>
                </tr>
                <tr>
                  <td colspan="3" align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_5_2">
                    <tr>
                      <td class="green12_3">Characteristics:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">High bound-acrylonitrile NBR. Good oil-resistant properties.<br /></td>
                    </tr>
                    <tr>
                      <td class="green12_3">Application:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">High oil resistance requirement products, various packings, oil hose, O-ring, PVC blend.</td>
                    </tr>
                  </table></td>
                </tr>
                <tr class="gray12" >
                  <td rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF" scope="row"><span class="light_blue12">NBR</span><br />
                    <span class="blue12">#8051H</span></td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">Cold process,<br />
                    Emulsion Polymerization </td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">Paper bag   35Kg/bale</td>
                </tr>
                <tr >
                  <td colspan="3" align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_5_2">
                    <tr>
                      <td class="green12_3">Characteristics:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">High bound-acrylonitrile and high Mooney NBR, good oil resistance and good solvent resistance</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">properties.</td>
                    </tr>
                    <tr>
                      <td class="green12_3">Application:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">High oil resistance and high performance requirement products, various packings, oil hose, </td>
                    </tr>
                    <tr>
                      <td class="gray12_4">O-ring, PVC blend.</td>
                    </tr>
                  </table></td>
                </tr>
                <tr class="gray12">
                  <td rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF"  scope="row"><span class="light_blue12">NBR</span><br />
                    <span class="blue12">#8052</span></td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">Cold process,<br />
Emulsion Polymerization </td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF"> ─　</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF">Paper bag   35Kg/bale</td>
                </tr>
                <tr >
                  <td colspan="3" align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_5_2">
                    <tr>
                      <td class="green12_3">Characteristics:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">Medium Mooney, easy to processing.<br /></td>
                    </tr>
                    <tr>
                      <td class="green12_3">Application:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">Oil hose, various packings, PVC blend, shoes sole, foaming product and adhesives.</td>
                    </tr>
                  </table></td>
                </tr>
                <tr class="blue12" >
                  <td rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF" scope="row"><span class="light_blue12">NBR</span><br />
                    #8052L</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF"><span class="gray12_4">Cold process,<br />
                    Emulsion Polymerization </span></td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">Paper bag   35Kg/bale</td>
                </tr>
                <tr >
                  <td colspan="3" align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_5_2">
                    <tr>
                      <td class="green12_3">Characteristics:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">Medium Mooney, easy to processing.<br /></td>
                    </tr>
                    <tr>
                      <td class="green12_3">Application:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">Oil hose, various packings, PVC blend, shoes sole, foaming product and adhesives.</td>
                    </tr>
                  </table></td>
                </tr>
                <tr class="blue12" >
                  <td rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF" scope="row"><span class="light_blue12">NBR</span><br />
                    #8053</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF"><span class="gray12_4">Cold process,<br />
                    Emulsion Polymerization </span></td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">Paper bag   35Kg/bale</td>
                </tr>
                <tr  >
                  <td colspan="3" align="left" valign="top" ><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_5_2">
                    <tr>
                      <td class="green12_3">Characteristics:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">Good low temperature resistance properties, good rebound properties.</td>
                    </tr>
                    <tr>
                      <td class="green12_3">Application:</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">Low temperature resistance、high elasticity and oil resistance requirement products</td>
                    </tr>
                    <tr>
                      <td class="gray12_4">(Aviation products).</td>
                    </tr>
                  </table></td>
                </tr>
              </tbody>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right"><a href="product01_4.php"><img src="images/up_page.png" alt="" width="73" height="15" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
   <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
