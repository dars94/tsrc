<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/research_e.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Technical Development</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/research/b01_2.jpg','images/research/b02_2.jpg','images/research/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="about.php">About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="product.php">Products</a></span><span class="blue12">　l　</span><span class="blue12"><a href="research.php">Technical Development</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="news.php">News</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="research01.php"><img src="images/research/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/research/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="research02.php"><img src="images/research/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/research/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="research03.php"><img src="images/research/b03.jpg" name="Image3" width="171" height="38" id="Image3" onmouseover="MM_swapImage('Image3','','images/research/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="top" -->
      <div id="content_top">
        <table width="688" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="184"><img src="../TW/images/research/pic01.jpg" alt="" width="184" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="310"><img src="../TW/images/research/pic02.jpg" width="310" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="184"><img src="../TW/images/research/pic03.jpg" width="184" height="198" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable -->
      <div id="content_main"><!-- InstanceBeginEditable name="main" -->
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td><span class="blue14-b">Market Expansion through Continuous Innovation</span><span class="gray12"><br />
              </span></td>
          </tr>
          <tr>
            <td class="gray12_3">Since its founding in 1973, TSRC has experienced the evolution of synthetic rubber industry from early applications in tires and shoe soles to the expanded applications driven by the dramatic growth of global automotive, high-speed railway, aviation, and 3C industries. Application of synthetic rubber is increasingly diversified, and the performance requirements for rubber have also grown. With the increasingly fierce competition, technological development and innovation capacity are the keys for maintaining market leadership in the synthetic rubber industry.  Since its establishment, TSRC has adhered to clear R&D strategy by consistently introducing new products in response to the market needs, and has laid solid R&D foundation and enhanced capacity over  years. As a result, TSRC is now positioned at an advantage over its direct competitors in the Asia Pacific region.</td>
          </tr>
          <tr>
            <td class="gray12_3">The following are the  milestones of TSRC’s R&amp;D team:            </td>
          </tr>
          <tr>
            <td align="right" ><table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%" align="left" valign="top"><span class="gray12">1.</span></td>
                <td width="97%" align="left" valign="top"><span class="gray12_3">Cooperated with Goodrich to develop styrene butadiene rubber (SBR), which went on commercial production in 1977.</span></td>
              </tr>
              <tr>
                <td align="left" valign="top"><span class="gray12">2.</span></td>
                <td align="left" valign="top"><span class="gray12_3">Developed butadiene  rubber (BR) jointly with Japanese UBE in 1997.</span></td>
              </tr>
              <tr>
                <td align="left" valign="top"><span class="gray12">3.</span></td>
                <td align="left" valign="top"><span class="gray12_3">Acquired TPE technology from Phillips company in 1984</span></td>
              </tr>
              <tr>
                <td align="left" valign="top"><span class="gray12">4.</span></td>
                <td align="left" valign="top"><span class="gray12_3">Developed Metallocene-based  high value-added hydrogenated styrene  (SEBS) in 2001. So far, TSRC has developed five grades  of SEBS-6150, 6151, 6152, 6154, and  6159.</span></td>
              </tr>
              <tr>
                <td align="left" valign="top"><span class="gray12">5.</span></td>
                <td align="left" valign="top"><span class="gray12_3">Developed SSBR rubber in  2010, which will be used in wear-resistant, energy-saving, environment-friendly and high-performance tires.</span></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="gray12_3">TSRC continues to expand its technological advancement in the global market by invest in professional talents and research facilities. Currently, it has recruited an elite R&D team of about 100 members. TSRC has long been collaborating with academic and research institutes worldwide to develop innovative products, manufacturing processes, and applications to provide total solution for its customers.</td>
          </tr>
          <tr>
            <td class="gray12_3">Upon the successful acquisition of the U.S. based Dexco Polymer in April 2011, TSRC is able to integrate Dexco's over 20 years of experience in the development of advanced manufacturing process, application technology, and European/American markets. The acquisition not only successfully enlarges TSRC's global presence but also transforms TSRC into one of the world's top five SBC suppliers.</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div><div id="footer"><table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></div>
</body>
<!-- InstanceEnd --></html>
