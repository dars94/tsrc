<?php
require_once('../db.php');
$news = $db->boards()->where('lang','en')->order('post_date DESC');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Latest News</title>
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.5.2.min.js"></script>
<script type="text/javascript">
$(function(){
  $('.item_block').hide();
  $('.down_btn').click(function(){
    $(this).hide();
    var id = $(this).attr('id').split('_')[1];
    $('#block_'+id).slideDown();
  });
  $('.up_btn').click(function(){
    var id = $(this).attr('id').split('_')[1];
    $('#down_'+id).show();
    $('#block_'+id).slideUp();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}
function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}
function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}
$(function(){

	$('.item_block_about').css('display','none');

	$('.down_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideDown();

		$(this).css('display','none');

	});

	$('.up_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideUp();

		$('#'+tmp_id.split('_')[0]+'_d').css('display','');

	});

})

</script>
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/news/b01_2.jpg','images/news/b02_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="blue12">　l　</span><span class="light_gray12"> <a href="index.php">English</a><span class="blue12">　l　</span></span><span class="light_gray12"><a href="../CN/index.php">Simplified Chinese</a></span><span class="blue12">　l　</span><span class="light_gray12"><a href="../TW/index.php"> Traditional Chinese</a></span><span class="blue12">　l　</span></td>
        </tr>
      </table>
    </div>
    <div id="logo">
      <div id="logo_img"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
      <div id="logo_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td align="center" class="light_gray12">Key word search</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="../EN/index.php">Home</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/about.php" >About TSRC</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/service.php">Investor Relations</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/product.php">Products</a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/research.php">Technical Development</a></span><span class="blue12">　l　<a href="../EN/news.php">News</a></span><span class="gray12_2"><a href="../EN/news.php"></a></span><span class="blue12">　l　</span><span class="gray12_2"><a href="../EN/member.php">Group Member</a></span><span class="blue12">　l</span></div>
    </div>
    <div id="content">
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><img src="images/news/b01_2.jpg" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/news/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="news02.php"><img src="images/news/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/news/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <div id="content_main_3" style="min-height:300px">
        <img src="images/spacer.gif" width="10" height="20" />
        
                <?php foreach($news as $n):?>
    <div id="item">
          <table width="622" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80" align="left" class="gray12"><?php echo substr($n['post_date'],0,10)?></td>
              <td><span class="blue12_4"><a href="#"><?php echo $n['title']?></a></span><a href="javascript:void(0)"><img src="../images/down.png" alt="" name="btn01" width="17" height="14" border="0" id="down_<?php echo $n['id']?>" class="down_btn" /></a></td>
            </tr>
          </table>
        </div>
        <div class="item_block" id="block_<?php echo $n['id']?>">
      <?php echo $n['content']?>
      <a href="#"><img src="../images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="up_<?php echo $n['id']?>" class="up_btn" /></a>
    </div>
    <?php endforeach;?>

      </div>
    </div>
  </div>
</div>
<div id="footer">
  <table width="998" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="833" align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="../EN/map.php">sitemap</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
</html>
