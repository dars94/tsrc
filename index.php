<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TSRC台橡─亞太地區合成橡膠的領導者</title>
<link href="TW/css/index.css" rel="stylesheet" type="text/css" />
<link href="TW/css/text.css" rel="stylesheet" type="text/css" />
<script src="TW/Scripts/swfobject_modified.js" type="text/javascript"></script>
<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function hideFlash(){
	$('#FlashID').css('display','none');
	$('#main_block').css('display','block');
	$('#content').css('visibility','visible');
}
$(function(){
	$('#content').css('visibility','hidden');
	$('#main_block').css('display','none');
  $('#Image6').click(function(){
    $('#form1').submit();
  });
});
function MM_preloadImages() { //v3.0
	var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
	var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
	var p,i,x;	if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n); return x;
}	

function MM_swapImage() { //v3.0
	var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	 if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>

<body onload="MM_preloadImages('TW/images/go_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="logo"><img src="TW/images/index_top.jpg" width="998" height="260" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│</span><span class="gray12_2"><a href="TW/index.php">　首頁　</a></span><span class="blue12">│　</span><span class="gray12_2"><a href="TW/about.php">關於台橡</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="TW/service.php">投資人服務　</a></span><span class="blue12">│　</span><span class="gray12_2"><a href="TW/product.php">產品　</a></span><span class="blue12">│</span><span class="gray12_2"><a href="TW/research.php">　研究與發展　</a></span><span class="blue12">│</span><span class="gray12_2"><a href="TW/news.php">　新聞</a></span><span class="blue12">　│</span><span class="gray12_2"><a href="TW/member.php">　集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_langue">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td><span class="blue12">│</span><span class="light_gray12"> <a href="EN/index.php">English</a> </span><span class="blue12">│</span><span class="light_gray12"><a href="CN/index.php"> 中文简体 </a></span><span class="blue12">│</span><span class="light_gray12"><a href="TW/index.php"> 中文繁體 </a></span><span class="blue12">│</span></td>
          </tr>
        </table>
      </div>
      <div id="top_btn02">
        <table width="600" border="0" cellpadding="0" cellspacing="0" class="blue15-b">
          <tr>
            <td><a href="TW/product01_1.php">ESBR</a>　<a href="TW/product01_2.php">SSBR</a>　<a href="TW/product01_3.php">BR</a>　<a href="TW/product01_4.php">NBR</a>　<a href="TW/product02_1.php">TPE</a>　( <a href="TW/product02_2.php">SBS</a> <a href="TW/product02_2.php">SIS</a> <a href="TW/product02_2.php">SEBS</a> )　<a href="TW/product02_4.php">Compounds</a></td>
          </tr>
        </table>
      </div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td><form id="form1" name="form1" method="get" action="TW/search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="TW/images/go.jpg" alt="" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="flash">
      <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="866" height="212">
        <param name="movie" value="TW/images/flash.swf" />
        <param name="quality" value="high" />
        <param name="wmode" value="opaque" />
        <param name="swfversion" value="6.0.65.0" />
        <!-- 此 param 標籤會提示使用 Flash Player 6.0 r65 和更新版本的使用者下載最新版本的 Flash Player。如果您不想讓使用者看到這項提示，請將其刪除。 -->
        <param name="expressinstall" value="TW/Scripts/expressInstall.swf" />
        <!-- 下一個物件標籤僅供非 IE 瀏覽器使用。因此，請使用 IECC 將其自 IE 隱藏。 --> 
        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="TW/images/flash.swf" width="866" height="212">
          <!--<![endif]-->
          <param name="quality" value="high" />
          <param name="wmode" value="opaque" />
          <param name="swfversion" value="6.0.65.0" />
          <param name="expressinstall" value="TW/Scripts/expressInstall.swf" />
          <!-- 瀏覽器會為使用 Flash Player 6.0 和更早版本的使用者顯示下列替代內容。 -->
          <div>
            <h4>這個頁面上的內容需要較新版本的 Adobe Flash Player。</h4>
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="取得 Adobe Flash Player" width="112" height="33" /></a></p>
          </div>
          <!--[if !IE]>-->
        </object>
        <!--<![endif]-->
      </object>
      <table id="main_block" width="866" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="212"><img src="TW/images/flash_01.jpg" width="212" height="212" /></td>
          <td width="6">&nbsp;</td>
          <td width="212"><img src="TW/images/flash_02.jpg" alt="" width="212" height="212" /></td>
          <td width="6">&nbsp;</td>
          <td width="212"><img src="TW/images/flash_03.jpg" alt="" width="212" height="212" /></td>
          <td width="6">&nbsp;</td>
          <td width="212"><img src="TW/images/flash_04.jpg" alt="" width="212" height="212" /></td>
        </tr>
      </table>
    </div>
    <div id="content">
      <table width="866" height="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="212" align="center" valign="top"	background="TW/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="TW/images/title01.jpg" width="197" height="32" /></td>
              </tr>
              <tr>
                <td width="202" align="left"><span class="gray12">台橡積極開發環保性橡材，其中可運用於環保型輪胎的 SSBR 已研發成功具有更耐磨、可回收再生重複利用且無毒害的特質為未來橡材樹立新標準<br />
                ......</span><span class="green12"><a href="TW/about05_5.php">&gt;了解更多</a></span></td>
              </tr>
            </table></td>
          <td width="6">&nbsp;</td>
          <td width="212" align="center" valign="top" background="TW/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="TW/images/title02.jpg" alt="" width="197" height="32" /></td>
              </tr>
              <tr>
                <td align="left"><span class="gray12">台橡股份有限公司今日宣佈成功其完成收購Dexco Polymers LP。Dexco為埃克森美孚化工公司和陶氏化學公司的合資公司，雙方各佔一半股權。<br />
                ......</span><span class="green12"><a href="TW/news.php">&gt;了解更多</a></span></td>
              </tr>
            </table></td>
          <td width="6">&nbsp;</td>
          <td width="212" align="center" valign="top" background="TW/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="TW/images/title03.jpg" alt="" width="197" height="32" /></td>
              </tr>
              <tr>
                <td align="left"><span class="gray12">台橡肩負企業發展所產生的社會與環境責任，我們關愛員工及相關合作夥伴、社群，並關注環保，投資建構各項措施，努力為企業永續經營創造良性循環的和諧環境......</span><span class="green12"><a href="TW/about05_3.php">&gt;了解更多</a></span></td>
              </tr>
            </table></td>
          <td width="6">&nbsp;</td>
          <td width="212" align="center" valign="top" background="TW/images/text_bg02.jpg"><table width="197" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="TW/images/title04.jpg" alt="" width="197" height="32" /></td>
              </tr>
              <tr>
                <td align="left"><span class="gray12">台橡研發精英團隊研發綠色製程多年多項成果實踐於生產線上，並斥資全新儀器處理廢水、廢氣，有效進行節能減碳等各項計劃，勵行企業公民的責任......</span><span class="green12"><a href="TW/about05_6.php">&gt;了解更多</a></span></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
    <div id="down">
      <table width="866" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  class="gray12_2" >歡迎聯繫我們 ：</td>
                <td class="blue12_mail"><a href="mailto:tsrcsales@tsrc-global.com"></a><a href="mailto:tsrcsales@tsrc-global.com"> tsrcsales@tsrc-global.com</a></td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
          <td align="right" class="light_gray10">© Copyright 2011 TSRC Corporation. All Rights Reserved. </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
</html>
