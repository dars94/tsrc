<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/product_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>产品─应用材料营运处</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/product/b01_2.jpg','images/product/b01-1_2.jpg','images/product/b01-2_2.jpg','images/product/b01-3_2.jpg','images/product/b01-4_2.jpg','images/product/b02_2.jpg','images/product/b02-1_2.jpg','images/product/b02-2_2.jpg','images/product/b02-3_2.jpg','images/product/b02-4_2.jpg','images/product/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php"><span class="gray12_2">关于台橡</span></a>　│　</span><span class="gray12_2"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="blue12"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product01.php"><img src="images/product/b01.jpg" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/product/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_1.php"><img src="images/product/b01-1.jpg" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/product/b01-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_2.php"><img src="images/product/b01-2.jpg" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/product/b01-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_3.php"><img src="images/product/b01-3.jpg" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/product/b01-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_4.php"><img src="images/product/b01-4.jpg" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/product/b01-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product02.php"><img src="images/product/b02_3.jpg" name="Image7" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/product/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_1.php"><img src="images/product/b02-1.jpg" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/product/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_2.php"><img src="images/product/b02-2_3.jpg" name="Image9" width="171" height="22" border="0" id="Image9" onmouseover="MM_swapImage('Image9','','images/product/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_3.php"><img src="images/product/b02-3.jpg" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/product/b02-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_4.php"><img src="images/product/b02-4.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/product/b02-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product03.php"><img src="images/product/b03.jpg" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/product/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_3">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td align="center" ><table width="632" class="table_6" summary="Submitted table designs">
              <thead>
                <tr  >
                  <td width="388" align="left" scope="col">　　<img src="../TW/images/product/TA.png" alt="" width="49" height="15" /><span class="blue12">TPE SBS　　　　　　　　　　　　　</span><span class="blue12">Capability</span></td>
                  <td width="60" align="center" class="blue12" scope="col">3206</td>
                  <td width="60" align="center" class="blue12" scope="col">4202</td>
                  <td width="60" align="center" class="blue12" scope="col">4230</td>
                  <td width="60" align="center" class="blue12" scope="col">4265</td>
                  </tr>
              </thead>
              <tfoot>
              </tfoot>
              <tbody>
                <tr >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Polymer Structure</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">Linear</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">Linear</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">Linear</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">Linear</td>
                  </tr>
                <tr >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　BD(or Isoprene)/SM</td>
                  <td align="center" valign="middle" class="gray12_4">71/29</td>
                  <td align="center" valign="middle" class="gray12_4">60/40</td>
                  <td align="center" valign="middle" class="gray12_4">62.5/37.5</td>
                  <td align="center" valign="middle" class="gray12_4">62.5/37.5</td>
                  </tr>
                <tr >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Diblock %</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">18</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">65</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　Specific Gravity</td>
                  <td align="center" valign="middle" class="gray12_4">0.95</td>
                  <td align="center" valign="middle" class="gray12_4">0.96</td>
                  <td align="center" valign="middle" class="gray12_4">0.95</td>
                  <td align="center" valign="middle" class="gray12_4">0.95</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Volatile Matters %</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&lt;0.3</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&lt;0.3</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&lt;0.5</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&lt;0.5</td>
                  </tr>
                <tr align="left"  >
                  <td valign="middle" class="gray12_4" scope="row">　　　Ash</td>
                  <td align="center" valign="middle" class="gray12_4">&lt;0.1</td>
                  <td align="center" valign="middle" class="gray12_4">&lt;0.2</td>
                  <td align="center" valign="middle" class="gray12_4">&lt;0.1</td>
                  <td align="center" valign="middle" class="gray12_4">&lt;0.1</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Extended Oil (phr)</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  </tr>
                <tr >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　Oil Type</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　5% WT% Toluene Solution Viscosity ( CP ) </td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　10% WT% Toluene Solution Viscosity ( CP ) </td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　20% WT% Toluene Solution Viscosity ( CP ) </td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">─</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　25% WT% Toluene Solution Viscosity ( CP ) </td>
                  <td align="center" valign="middle" class="gray12_4">850</td>
                  <td align="center" valign="middle" class="gray12_4">620</td>
                  <td align="center" valign="middle" class="gray12_4">510</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Color</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">white</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">white</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">white</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">white</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　Form</td>
                  <td align="center" valign="middle" class="gray12_4">pellets</td>
                  <td align="center" valign="middle" class="gray12_4">pellets</td>
                  <td align="center" valign="middle" class="gray12_4">pellets</td>
                  <td align="center" valign="middle" class="gray12_4">pellets</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Melt Flow ( g/10min,190&deg;/5kg )</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">5.5</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">7.5</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">13</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">19</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　Melt Flow ( g/10min,190&deg;/1.05kg )</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  <td align="center" valign="middle" class="gray12_4">─</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Tensile Strength (kg/cm2)</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&gt;90</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&gt;200</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&gt;90</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">&gt;35</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　Elongation (%)</td>
                  <td align="center" valign="middle" class="gray12_4">&gt;750</td>
                  <td align="center" valign="middle" class="gray12_4">&gt;650</td>
                  <td align="center" valign="middle" class="gray12_4">&gt;800</td>
                  <td align="center" valign="middle" class="gray12_4">&gt;800</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" bgcolor="#FFFFFF" class="gray12_4" scope="row">　　　Hardness (JISA)</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">72</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">92</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">87</td>
                  <td align="center" valign="middle" bgcolor="#FFFFFF" class="gray12_4">90</td>
                  </tr>
                <tr  >
                  <td align="left" valign="middle" class="gray12_4" scope="row">　　　Applications</td>
                  <td align="center" valign="middle" class="gray12_5">Adhesives,<br />
                    Plastic,<br />
                    modification</td>
                  <td colspan="3" align="center" valign="middle" class="gray12_5">Transparent shoe soles,<br />
                    Asphalt modification,<br />
                    Adhesives, Mechanical goods,<br />
                    Plastic modification</td>
                  </tr>
              </tbody>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right"><a href="product02_2.php"><img src="images/up_page.png" width="73" height="15" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
