<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/service_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>投资人服务─股东专柜</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/service/b01_2.jpg','images/service/b02_2.jpg','images/service/b02-1_2.jpg','images/service/b04_2.jpg','images/service/b05-1_2.jpg','images/service/b05-2_2.jpg','images/service/b05-4_2.jpg','images/service/b05-5_2.jpg','images/service/b03_2.jpg','images/service/b05_2.jpg','images/service/b05-3_2.jpg','images/service/b02-2_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php"><span class="gray12_2">关于台橡</span></a>　│　</span><span class="blue12"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="service.php"><img src="images/service/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/service/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service02.php"><img src="images/service/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/service/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service02_1.php"><img src="images/service/b02-1.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/service/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service02_1.php"><img src="images/service/b02-2.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/service/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service03.php"><img src="images/service/b03.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/service/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service04.php"><img src="images/service/b04.jpg" name="Image7" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/service/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05_2.jpg" name="Image8" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/service/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-1_2.jpg" name="Image9" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/service/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-2_2.jpg" name="Image10" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/service/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-3_2.jpg" name="Image11" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/service/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-4_2.jpg" name="Image12" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/service/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-5_2.jpg" name="Image13" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/service/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_3">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td><table width="632" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="160" align="center" valign="top"><table width="160" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="../TW/images/service/stock.jpg" width="160" height="160" /></td>
                      </tr>
                      <tr>
                        <td align="center" ><table border="0" cellspacing="0" cellpadding="5">
                            <tr>
                              <td align="center" class="gray12">股票代号2103</td>
                            </tr>
                            <tr>
                              <td align="center"><a href="http://www.twse.com.tw/ch/stock_search/stock_search.php" target="_blank"><img src="images/service/stock.png" width="79" height="19" /></a></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                  <td width="25" align="center" valign="top"><img src="images/line_straight.png" width="25" height="350" /></td>
                  <td width="446" align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="26%"><table border="0" cellpadding="2" cellspacing="0">
                            <tr>
                              <td align="center" valign="middle"><img src="../TW/images/service/icon_g2.png" alt="" width="17" height="15" /></td>
                              <td align="center" valign="middle" class="gray12">股东会</td>
                            </tr>
                          </table></td>
                        <td width="25%" class="gray12"><a href="../TW/pdf/05-100-01.pdf" target="_blank">股东会公告</a></td>
                        <td width="6%" class="gray12"><a href="../TW/pdf/05-100-01.pdf" target="_blank"><img src="../TW/images/pdf.png" alt="" width="20" height="20" /></a></td>
                        <td width="43%" class="gray12">( 約96KB )</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td class="gray12"><a href="../TW/pdf/05-02.pdf" target="_blank">开会通知书</a></td>
                        <td class="gray12"><a href="../TW/pdf/05-100-02.pdf" target="_blank"><img src="../TW/images/pdf.png" alt="" width="20" height="20" /></a></td>
                        <td class="gray12">( 約566KB )</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td class="gray12"><a href="../TW/pdf/05-03.pdf" target="_blank">议事手册</a></td>
                        <td class="gray12"><a href="../TW/pdf/05-100-03.pdf" target="_blank"><img src="../TW/images/pdf.png" alt="" width="20" height="20" /></a></td>
                        <td class="gray12">( 約817KB )</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td class="gray12"><a href="../TW/pdf/05-04.pdf" target="_blank">议事录</a></td>
                        <td class="gray12"><a href="../TW/pdf/05-100-04.pdf" target="_blank"><img src="../TW/images/pdf.png" alt="" width="20" height="20" /></a></td>
                        <td class="gray12">( 約1MB )</td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="0">
                            <tr>
                              <td align="center" valign="middle"><img src="../TW/images/service/icon_p.png" alt="" width="17" height="15" /></td>
                              <td align="center" valign="middle" class="gray12">历年股利分派</td>
                            </tr>
                          </table></td>
                        <td class="gray12"><a href="../TW/pdf/05-05.pdf" target="_blank">下载文件</a></td>
                        <td><span class="gray12"><a href="../TW/pdf/05-05.pdf" target="_blank"><img src="../TW/images/pdf.png" alt="" width="20" height="20" /></a></span></td>
                        <td><span class="gray12">( 約48KB )</span></td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="0">
                            <tr>
                              <td align="center" valign="middle"><img src="../TW/images/service/icon_b2.png" alt="" width="17" height="14" /></td>
                              <td align="center" valign="middle" class="gray12">重大讯息公告</td>
                            </tr>
                          </table></td>
                        <td class="gray12"><a href="http://mops.twse.com.tw/mops/web/t05st01" target="_blank">股市观测站<img src="../TW/images/service/finger.png" alt="" width="16" height="20" /></a></td>
                        <td align="center"></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="0">
                            <tr>
                              <td align="center" valign="middle"><img src="../TW/images/service/icon_y2.png" alt="" width="17" height="15" /></td>
                              <td align="center" valign="middle" class="gray12">联络人</td>
                            </tr>
                          </table></td>
                        <td colspan="3" class="gray12">办理股票过户机构</td>
                      </tr>
                      <tr>
                        <td rowspan="3">&nbsp;</td>
                        <td colspan="3" class="blue12">永丰金证券 股务代理部 <img src="../TW/images/service/t.png" width="11" height="11" />02-23816288 
                          <a href="http://www.sinotrade.com.tw" target="_blank"></a></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="blue12">台北市博爱路17号3楼</td>
                      </tr>
                      <tr>
                        <td colspan="3" class="blue12"><a href="http://www.sinotrade.com.tw" target="_blank">http://www.sinotrade.com.tw</a></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td colspan="3" class="gray12">台橡股务单位</td>
                      </tr>
                      <tr>
                        <td rowspan="3">&nbsp;</td>
                        <td colspan="3" class="blue12"><img src="../TW/images/service/t.png" alt="" width="11" height="11" />02-37016000
                          <a href="mailto:spokesman@tsrc-global.com"></a></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="blue12">台北市敦化南路二段95号18楼</td>
                      </tr>
                      <tr>
                        <td colspan="3" class="blue12"><a href="mailto:spokesman@tsrc-global.com">e-mail : spokesman@tsrc-global.com</a></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td colspan="3" class="gray12"> 发言人</td>
                      </tr>
                      <tr>
                        <td rowspan="3">&nbsp;</td>
                        <td colspan="3" class="blue12"><img src="../TW/images/service/t.png" alt="" width="11" height="11" />02-37016000
                          <a href="mailto:spokesman@tsrc-global.com"></a></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="blue12">台北市敦化南路二段95号18楼</td>
                      </tr>
                      <tr>
                        <td colspan="3" class="blue12"><a href="mailto:spokesman@tsrc-global.com">e-mail : spokesman@tsrc-global.com</a></td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
