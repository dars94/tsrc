<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
<link href="css/text.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="385" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ball">
      <tr>
        <td align="center"><table width="95%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><a href="#"><img class='close_btn' src="../TW/images/about/close.jpg" width="25" height="25" /></a></td>
            </tr>
          <tr>
            <td align="left"><span class="green12_3">相关作业场所均依法定期实施作业环境噪音测定，并对厂房周界实施低频噪音测定。近年来各项噪音测定结果都符合法规管制标准，但仍积极实施</span><span class="orange12">噪音防制</span><span class="green12_3">设备之装置、低噪音设备之採购及噪音控制技术之研究。</span></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
