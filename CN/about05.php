<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>关于台橡─总经理兼执行长的话</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php">关于台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><img src="images/about/b05-1_2.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
    
      <div id="content_main_3">
        <table width="632" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">持续创新及全球化佈局，是台橡前进的动力</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡公司成立三十八年，历经国家政策支持、台湾产业结构转型、企业外移大陆浪潮与中国崛起的历史时刻。每一次转折，都能保持稳健的成长步伐不断超越。其中关键因素就是台橡公司坚持勇于创新、灵活思考的态度，面对不同市场需求，抱持多元化的服务策略，满足高端客户需求，品质更是从不妥协，因而奠定市场上无可替代的品牌价值与良好商誉。</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡未来的竞争优势有三大面向：其一是不断的在技术上创新，取得生产制程方面的优势，透过技术上的改良，符合高端客户需求。其二在于导入专案管理的模式，执行『专业分工、团队作战』的经营方式。其三在于往海外扩展企业版图，佈局全球化并与国际市场接轨。</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡公司深刻了解创新的重要性，不断延揽技术人员组成专业研发团队。更透过併购美国Dexco Polymers公司，成为SBC的全球领先厂商。获得高阶制造技术，致力产品差异化、高质化的目标。产品应用方面，已涉足到民生商品、医疗用品等诸多领域，不断创新既有销售模式及通路。</td>
          </tr>
          <tr>
            <td class="gray12_3">在管理效能上，则摆脱集体生产思维，以专案管理模式，为公司注入一股活力。召集不同专业领域的人才，划分不同战斗单位，让生产、研发、业务等各部门，可以灵活发挥专长，并且做横向连结相互支援。</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡公司最早的海外版图从中国与泰国出发，近年开始往俄罗斯、印度、美国市场发展，在迈向国际化的同时，也体认到置身瞬息万变的市场，唯有不断创新，才能取得优势竞争力。</td>
          </tr>
          <tr>
            <td class="gray12_3">随着全球化时代巨轮的转动，台橡继续大步向前迈进，在世界市场上寻求ㄧ个新定位。从上游原料开发、到中下游的产品制造与销售，台橡公司不断与国际接轨，随时掌握全球脉动，在追求成长的同时，台橡也积极追求环保方案之落实，注重废气排放与废弃物处理，透过管理方式，及制程减碳节能，做好工安及环保工作，除了每年回馈地方与社区，也正在取得CSR证，希望能善尽企业社会责任，达到社会大众的期待。
</td>
          </tr>
          <tr>
            <td class="gray12_3">从台湾出发，现今的台橡公司更将带着丰富的世界经验走回台湾，在高分子材料的领域，不断写下亮丽的里程碑。</td>
          </tr>
          <tr>
            <td class="gray12_3">&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td align="right" ><img src="images/about/sign.jpg" width="180" height="80" /></td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      </div>
      
    <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
