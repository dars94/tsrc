     <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" id='btn_1' coords="3,2,168,24" />

                  <area shape="rect" id='btn_2' coords="174,2,342,25" />

                  <area shape="rect" id='btn_3' coords="348,1,513,24" />

                  <area shape="rect" id='btn_4' coords="519,3,683,25" />

                  </map></td>

              </tr>

            <tr>

              <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="50" align="left" valign="top" class="green12">1999.11</td>
                  <td width="542" align="left" class="gray12">将原台湾合成橡胶股份有限公司的名字更改为台橡股份有限公司，并重新设计企业识别标志，以利公司未来更弹性化的发展。 </td>
                </tr>
                <tr>
                  <td align="left" class="green12">1999.04</td>
                  <td align="left" class="gray12">参与投资台湾高速铁路计划。 </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">1998.08</td>
                  <td align="left" class="gray12">BR工厂再进行去瓶颈工程，产能扩展到55,000公吨。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1998.02 </td>
                  <td align="left" class="gray12">雄厂通过ISO-14001有关品质与环境的认证。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1997.01</td>
                  <td align="left" class="gray12">高雄厂通过ISO-9001有关品质的认证。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1996.04</td>
                  <td align="left" class="gray12">主导投资9千2百万美元在大陆兴建年产10万公吨的SBR工厂计划。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1996.02</td>
                  <td align="left" class="gray12">荣获经济部颁发第四届&rdquo; 产业科技发展奖&rdquo;。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1995.10</td>
                  <td align="left" class="gray12">参与投资240万美元在泰国兴建年产5万公吨的BR工厂计划。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1995.05</td>
                  <td align="left" class="gray12">TPE氢化产品开发成功，并积极准备商业化。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1995.04 </td>
                  <td align="left" class="gray12">投资电力事业。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1994.09 </td>
                  <td align="left" class="gray12">BR工厂完成去瓶颈，年产能提升为51,000公吨。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1994.04</td>
                  <td align="left" class="gray12">开发TPE新产品，并扩增TPE第二生产线，使TPE产能提升到54,000公吨/年。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1994.01</td>
                  <td align="left" class="gray12">高雄厂通过ISO-9002有关品质的认证。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1991.09 </td>
                  <td align="left" class="gray12">兴建实验工厂及汽电共生装置。</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td align="center">&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>