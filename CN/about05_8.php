<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>关于台橡─企业社会责任</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php">关于台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05_3.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
         
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05-2.jpg" alt="" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/about/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_3.php"><img src="images/about/b05-3.jpg" alt="" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/about/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_4.php"><img src="images/about/b05-4.jpg" alt="" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/about/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_5.php"><img src="images/about/b05-5.jpg" alt="" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/about/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_6.php"><img src="images/about/b05-6.jpg" alt="" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/about/b05-6_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_7.php"><img src="images/about/b05-7.jpg" alt="" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/about/b05-7_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b05-8_2.jpg" alt="" name="Image14" width="171" height="22" id="Image14" onmouseover="MM_swapImage('Image14','','images/about/b05-8_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top"><img src="../TW/images/about/pic01_5_8.jpg" width="688" height="197" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">落实制度，环安卫绩效丰硕</td>
          </tr>
          <tr>
            <td align="left" valign="top" class="gray12"><table width="632" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="25" align="center" valign="top"><img src="../TW/images/about/mark.png" width="21" height="21" /></td>
                  <td width="581" align="left" valign="top">荣获台湾省政府环境保护处85年度毒性化学物质灾害无预警测试绩优单位。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">配合经济部举办「87年度工业安全卫生技术辅导」协助推广工业安全技术，将改善成果与经验提供同业观摩，协助推广工业安全技术。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">荣获98年度大社工业区安全与环境监测业务推动绩优奖。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">执行99年度防火管理工作荣获评定为绩优场所。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">荣获高雄市「99年度绩优防火管理场所暨防火管理人」。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">热心参与地方公益活动，荣获大社乡保社村村长及大社乡民意代表致赠奖座。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">协助义守大学化学工程学系办理「企业参访」之教学课程活动。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">积极推动大社工业区区域联防，协助区域联防组织内工厂落实安全卫生工作，并强化紧急应变通报及相互支援机制，提升区域安全。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="../TW/images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">协助行政院劳委会南区劳动检查所办理「98年度製造业屋顶坠落预防观摩会」。</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
