<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>关于台橡─主要业务</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){

	$('.item_block_about').css('display','none');

	$('.down_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideDown();

		$(this).css('display','none');

	});

	$('.up_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideUp();

		$('#'+tmp_id.split('_')[0]+'_d').css('display','');

	});

})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php">关于台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b04_2.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><div id="content_top_5"><img src="../TW/images/about/pic01_4_1.jpg" width="688" height="188" /></div><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" --><div id="content_main">
        <div id="main_about">
          <table width="632" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td width="130" align="left" valign="top"><table width="100" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left" valign="top"><span class="gray12">橡胶营运处</span></td>
                  </tr>
                </table></td>
              <td width="500" align="left" valign="top"><div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="../TW/images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">ESBR</span><span class="gray12"> － 比天然胶更耐磨且易于加工　<a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn1_d" /></a></span></div>
                  <div class="item_block_about" id="btn1_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" ><span class="gray12">乳聚苯乙烯—丁二烯橡胶(<span class="blue12">ESBR</span>)性质上比天然胶更耐磨且易于加工，其品质及产量不像天然胶会受到气候及地理因素影响。因此在所有合成橡胶中，<span class="blue12">ESBR</span>不仅是用途最广泛的合成橡胶，更是汽车轮胎、鞋底、输送带、橡胶带、战车及拖引车履带、运动器材、玩具等的主要原料。台橡于台湾高雄厂的</span><span class="blue12">ESBR</span><span class="gray12">年产能10万公吨。在中国，台橡与日本丸红(Marubeni Corporation)及南通石化总公司于江苏省南通经济技术开发区成立「申华化学工业有限公司」生产<span class="blue12">ESBR</span>，年产能原为10万公吨；配合大陆市场成长，已扩产为18万公吨。另外，台橡与印度国有石油(Indian Oil Corporation Ltd.)、日本丸红(Marubeni Corporation)于印度合资成立 Indian Synthetic Rubber Ltd.生产 <span class="blue12">ESBR</span>，规划年产能12万公吨，预计2012年第四季投产，初期以满足印度快速成长的市场需求为主。　</span><a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn1_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /><br />
                  </div>
                </div>
                <div id="about_block">
                  <div id="item_about">
                    <table width="495" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left"><span class="gray12"><a href="#"><img src="../TW/images/product/TA.png" width="49" height="15" /></a></span><span class="blue12">SSBR</span><span class="gray12"> － 具低滚动阻力、抗湿滑性、耐磨　<a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn2_d" /></a></span></td>
                      </tr>
                    </table>
                  </div>
                  <div class="item_block_about" id="btn2_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" ><span class="gray12">溶聚苯乙烯—丁二烯橡胶(</span><span class="blue12">SSBR</span><span class="gray12">)可根据加工及应用需求调整产品特性，突破传统橡胶的侷限，生产客制化的产品。</span><span class="blue12">SSBR</span><span class="gray12">主要应用于节能(低滚动阻力)轮胎、高性能轮胎、及全天候轮胎，不仅可降低油耗并减少汽车废气排放量，符合环保节能减碳趋势， 降低对环境之冲击，并提高产品的附加价值。此外，</span><span class="blue12">SSBR</span><span class="gray12">也可应用于鞋底、输送带、橡胶带、运动器材、玩具等。目前台橡规划台湾高雄<span class="blue12">SSBR</span>厂年产能为3万公吨，预计2011年第四季投产。　</span><a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn2_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="../TW/images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">BR</span><span class="gray12"> － 具优异弹性、耐磨耗性及耐低温特性 <a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn3_d" /></a></span></div>
                  <div class="item_block_about" id="btn3_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498"><span class="gray12">聚丁二烯橡胶(<span class="blue12">BR</span>)具有优异的弹性、耐磨耗性及耐低温特性。主要用于高速辐射轮胎制造及高耐冲击聚苯乙稀HIPS的改质。可增加轮胎使用时安全性及舒适性。用于HIPS改质时，具有良好的加工性及耐冲击的特性。随着HIPS在个人电脑、家庭用品及电子产业上的普遍应用，市场对<span class="blue12">BR</span>需求不断增加，也带动了台橡的成长。目前台橡台湾高雄<span class="blue12">BR</span>厂年产能为6万公吨，台橡与日本宇部兴产株式会社(Ube Industries, Ltd.)、日本丸红(Marubeni Corporation)于泰国合资成立的<span class="blue12">BR</span>工厂(Thai Synthetic Rubbers Ltd.)年产能为7万2千公吨。 </span></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">为了因应客户的需求、提升在亚太市场的竞争力，台橡更积极佈局大中华地区，与日本宇部兴产株式会社(Ube Industries, Ltd.)、日本丸红(Marubeni Corporation)于江苏省南通经济技术开发区设立「台橡宇部(南通)化学工业有限公司」生产<span class="blue12">BR</span>，现有年产能为5万公吨，并计划于2012扩产至7万2千公吨。</span><a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn3_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="../TW/images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">NBR</span><span class="gray12"> －具优异耐油性、耐溶剂性与耐热老化性　<a href="#"> <img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn4_d" /></a></span></div>
                  <div class="item_block_about" id="btn4_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" ><span class="gray12">聚丙烯腈-丁二烯橡胶(<span class="blue12">NBR</span>具有优异的耐油性、耐溶剂性与耐热老化等特性。主要应用于油管、油封、滚轮、工业垫片、传动皮带、鞋材、发泡材、输送带…等民生与工业制品。近年来中国地区汽车产量大幅成长，因此带动<span class="blue12">NBR</span>等合成橡胶强烈的需求。台橡位于江苏省南通经济技术开发区，与德国朗盛集团Lanxess合资成立「朗盛台橡(南通)化学工业有限公司」兴建年产能3万公吨的<span class="blue12">NBR</span>工厂，预计于2012年第二季投产。 </span><a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn4_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td width="130" align="left" valign="top"><table width="100" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left" valign="top"><span class="gray12">应用材料营运处</span></td>
                  </tr>
                </table></td>
              <td><div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="../TW/images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="gray12"><span class="blue12">TPE　热可塑性橡胶SBS, SIS, SEBS</span> －高价值应用橡胶，挑战新质地　<a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn5_d" /></a></span></div>
                  <div class="item_block_about" id="btn5_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12">TAIPOL®<span class="blue12">TPE </span>热可塑性橡胶是由苯乙烯与其他配合单体形成的嵌段式共聚合体(Styrenic Block Copolymer)简称<span class="blue12">SBC</span>。台橡本系列产品包括以丁二烯为配合单体的<span class="blue12">SBS</span>和以异戊二烯为配合的<span class="blue12">SIS</span>系列，以及<span class="blue12">SBS</span>经过氢化后，生成具有更优异耐候性及功能之第二代产品<span class="blue12">SEBS</span>。 </td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">由于热可塑性橡胶<span class="blue12">TPE</span>兼具备塑胶与橡胶特性，被大量应用于高级鞋底、柏油改质、塑胶改质、黏着剂等。它可以直接射出加工，并可回收废料降低成本，制成之鞋底不易打滑，是代替PVC及传统硫化橡胶的重要材料。<span class="blue12">TPE</span>更具有热可塑性能、黏度低、容易互掺混等优点，作为建筑和道路柏油、防水材料的改质剂，可改善柏油冬脆夏软的缺点，增进负载性能。将少量<span class="blue12">TPE</span>与聚丙烯、聚乙烯、聚苯乙烯等聚合物共混，可明显提高塑料製品的抗冲击强度，改善耐低温性能，可应用于医疗器材、汽车零配件、运动器材、玩具及工业用产品。<span class="blue12">TPE</span>与增黏树脂相容性优异，经熔融共混后可製成具备高黏着强度、符合环保需求之黏着剂，广泛应用于自黏感压胶带、标籤与纸尿片等卫生用品。</span></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">台橡因同时具备<span class="blue12">SBS</span>、<span class="blue12">SIS</span>、<span class="blue12">SEBS</span>三大系列产品，可同时满足不同应用范围的客户需求，在以客制化配方的应用产业上，具有独特的竞争优势。</span><a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn5_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"><img src="../TW/images/about/blend.png" alt="" width="88" height="17" /><span class="blue12">TPE　应用材料</span><span class="gray12"> －舒适生活的一部份 <a href="#"><img src="../TW/images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn6_d" /></a></span></div>
                  <div class="item_block_about" id="btn6_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12">热可塑性複合材料<span class="blue12">TPR</span>因符合环保要求得以快速成长，本公司运用相关技术推出<span class="blue12">活粒T-BLEND&reg;</span>产品，本产品是以<span class="blue12">SBS</span>或<span class="blue12">SEBS</span>为主的掺配应用材料，具备良好的触感，可调整的物性和硬度，优良的耐候性、耐低温性及热可塑性。<span class="blue12">活粒T-BLEND®</span>产品可依客户需求设计配方产制供应，产品边料可回收再制，降低生产成本；能直接注塑或挤压成型，节省工时。这种高功能的环保产品，提供塑模产业更大量宽广的应用，开发更多领域产品，如生活用品、超软质凝胶，无卤耐燃低电压电线产品，<span class="blue12">SEBS</span>发泡材料、门窗及冰箱密封条、弹性薄膜等。</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="blue12">活粒T-BLEND®</span><span class="gray12">，</span><span class="blue12">TPE</span><span class="gray12">应用材料在台湾高雄、上海松江、山东济南设有生产基地，年产量达27,000公吨，并在台湾、华东、华北、华南四处设有行销据点，并将不断拓展其他亚太地区市场，便于推广新产品及进行各种新材料的研发。 </span></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">另外为建立与客户之间互相沟通的桥樑，台橡特别斥资在高雄冈山建造「</span><span class="blue12">TPE</span><span class="gray12">应用研究中心并在上海成立「客户服务中心」。除了引进精密先进的实验设备外，菁英研发团队并密切与国外学术界及专业研究机构技术合作，主要为扩大</span><span class="blue12">SEBS</span><span class="gray12">及其掺配产品之应用领域，积极发展高附加价值的新材料和新技术，进而提供应用材料之谘询建议、设计研发等全系列的服务。</span><a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn6_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="../TW/images/down03.png" width="510" height="20" /> </div>
                </div></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table>
        </div>
      </div><!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
