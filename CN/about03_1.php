     <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" coords="3,2,168,24" id='btn_1' />

                  <area shape="rect" id='btn_2' coords="174,2,342,25" />

                  <area shape="rect" id='btn_3' coords="348,1,513,24" />

                  <area shape="rect" id='btn_4' coords="519,-1,683,21" />

              </map></td>

              </tr>

            <tr>

              <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="50" align="left" class="green12">2011.11</td>
                  <td width="542" align="left" class="gray12">与台湾中油公司及富邦金控创投完成合资谈判，成立台耀石化材料科技(股)公司。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2011.04</td>
                  <td width="542" align="left" class="gray12">併购美国Dexco  Polymers 100% 股权 。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2010.05</td>
                  <td align="left" class="gray12">在中国与德国朗盛合资成立朗盛台橡(南通)化学工业有限公司，筹建年产能3万公吨之NBR厂。</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">2010.04</td>
                  <td align="left" class="gray12">在印度与印度国有石油有限公司(Indian Oil Corporation Ltd.)、日本丸红株式会社(Marubeni Corporation)，合资筹建年产能12万公吨之ESBR厂。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2009.12</td>
                  <td align="left" class="gray12">获颁OHSAS 18001 & TOSHMS职业安全卫生管理系统认证。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2009.05 </td>
                  <td align="left" class="gray12">与俄罗斯RUSTEP LLC/ OJSC SIBUR HOLDING签订技术授权合同。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2009.05</td>
                  <td align="left" class="gray12">台橡宇部(南通)化学工业有限公司正式量产。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2008.07</td>
                  <td align="left" class="gray12">转投资新加坡Polybus Corporation Pte. Ltd.正式营运。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2008.07</td>
                  <td align="left" class="gray12">台橡(南通)实业有限公司正式量产。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2007.11</td>
                  <td align="left" class="gray12">结束德国Atlantic Polymers GmbH转投资公司。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2007.09 </td>
                  <td align="left" class="gray12">台橡(济南)实业有限公司正式量产。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2006.12</td>
                  <td align="left" class="gray12">在中国南通成立台橡宇部(南通)化学工业有限公司，筹建年产能5万公吨之BR厂。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2006.09</td>
                  <td align="left" class="gray12">在中国济南成立台橡(济南)实业有限公司，筹建年产能5仟公吨之掺配料厂。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2006.09</td>
                  <td align="left" class="gray12">在中国南通成立台橡(南通)实业有限公司，筹建年产能2万公吨之SEBS厂。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2005.12</td>
                  <td align="left" class="gray12">冈山厂TPE应用研究中心筹建完成，正式营运。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2005.06 </td>
                  <td align="left" class="gray12">转投资中国申华化学工业有限公司，完成扩建工程，年产能提升至18万公吨。/td>
                </tr>
                <tr>
                  <td align="left" class="green12">2005.04</td>
                  <td align="left" class="gray12">应用材料事业处冈山厂正式投产。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.10</td>
                  <td align="left" class="gray12">台北新办公室落成开始正式营运。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.08 </td>
                  <td align="left" class="gray12">应用材料事业处冈山厂开始筹建。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.07 </td>
                  <td align="left" class="gray12">应用材料事业处成立TPE应用研究中心。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.04 </td>
                  <td align="left" class="gray12">转投资之中国南通千象仓储有限公司正式营运。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2003.10</td>
                  <td align="left" class="gray12">转投资中国申华化学工业有限公司，完成去瓶颈工程，年产能提升至12万公吨。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2002.02 </td>
                  <td align="left" class="gray12">TPE工厂新产品SEBS试车成功，开启橡胶产品多元化应用之新契机。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2001.08 </td>
                  <td align="left" class="gray12">转投资之中国台橡(上海)公司正式开业，强化大中华市场佈局。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2001.03</td>
                  <td align="left" class="gray12">成立橡胶事业、应用材料事业和转投资事业责任中心制度。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2000.08 </td>
                  <td align="left" class="gray12">与德国当地专业人士合资设立Atlantic Polymers GmbH为本公司产品拓销至欧洲市场。</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td align="center">&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>