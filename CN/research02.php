<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/research_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>新制程开发</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','../TW/images/research/b01_2.jpg','../TW/images/research/b02_2.jpg','../TW/images/research/b03_2.jpg','images/research/b01_2.jpg','images/research/b02_2.jpg','images/research/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php"><span class="gray12_2">关于台橡</span></a>　│　</span><span class="gray12_2"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="blue12"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="research01.php"><img src="images/research/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/research/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/research/b02_2.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/research/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="research03.php"><img src="images/research/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/research/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="top" -->
      <div id="content_top">
        <table width="688" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="184"><img src="../TW/images/research/pic01_2.jpg" alt="" width="184" height="198" /></td>
            <td width="5">&nbsp;</td>
            <td width="310"><img src="../TW/images/research/pic02_2.jpg" width="310" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="184"><img src="../TW/images/research/pic03-2.jpg" width="184" height="198" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable -->
      <div id="content_main"><!-- InstanceBeginEditable name="main" -->
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="7">&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">节能减废，绿色制程</td>
          </tr>
          <tr>
            <td class="gray12_3">随着台橡收购美国Dexco Polymers公司后，台橡整合了Dexco发展20馀年的先进制程技术：例如具有优良物理特性的高纯度SBC产品；以及可生产四嵌段以上的不对称嵌段聚合物，提供客户更多先进的高性能新产品。</td>
          </tr>
          <tr>
            <td class="gray12_3">开发节能减废一直是台橡研发团队的工作重点。台橡积极投入绿色制程技术开发，致力于废弃物回收使用与节省能源，开发对环境影响最小的制程， BR工场废水回收的热水循环制程，将SBR、BR、TPE等成品乾燥废气採焚化处理、各厂区之暴雨截流及雨、污水分流分治以保护河川于制程中不受污染，并设置地下水监测井及土壤採样检测防治污染，回收溶剂、废气等多项节能减废制程，善尽地球公民环保责任。</td>
          </tr>
          <tr>
            <td align="center"><table width="93%" border="0" cellpadding="0" cellspacing="0" class="gray12">
                <tr>
                  <td width="12%" align="left"><span class="blue12">01. </span>SBR冷凝水回收再利用于制程</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">02. </span>BR冷凝水回收再利用于制程</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">03. </span>TPE冷凝水回收再利用于制程</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">04. </span>BR废热回收及热水循环工程</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">05. </span>BR废水回收使用于冷却水塔</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">06. </span>SBR成品乾燥废气採吸附处理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">07. </span>BR成品乾燥废气採吸附处理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">08. </span>TPE成品乾燥及粉尘採焚化处理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">09. </span>溶剂桶槽外排气冷凝回收处理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">10. </span>暴雨截流缓冲槽</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">11. </span>暴雨截流雨污水分流分治</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">12. </span> 地下水採样监测井</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div><div id="footer"><table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></div>
</body>
<!-- InstanceEnd --></html>
