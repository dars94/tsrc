<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>关于台橡─企业社会责任</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){

	tmp_img1 = new Image();

	tmp_img1_2 = new Image();

	tmp_img1.src = 'images/about/b05-4-1.png';

	tmp_img1_2.src = 'images/about/b05-4-1_2.png';

	tmp_img2 = new Image();

	tmp_img2_2 = new Image();

	tmp_img2.src = 'images/about/b05-4-2.png';

	tmp_img2_2.src = 'images/about/b05-4-2_2.png';

	tmp_img3 = new Image();

	tmp_img3_2 = new Image();

	tmp_img3.src = 'images/about/b05-4-3.png';

	tmp_img3_2.src = 'images/about/b05-4-3_2.png';

	tmp_img4 = new Image();

	tmp_img4_2 = new Image();

	tmp_img4.src = 'images/about/b05-4-4.png';

	tmp_img4_2.src = 'images/about/b05-4-4_2.png';

	$('#Image15')

		.click(function(){

			resetImgBtn()

			$(this).addClass('current');

			$(this).attr('src',tmp_img1_2.src);

			$.ajax({

				url:'about05_4_1.php',

				success:function(data){$('#info_block').html(data)}

			});

		})

		.hover(function(){

			$(this).attr('src',tmp_img1_2.src);

		},function(){

			if($(this).attr('class') != 'current'){

				$(this).attr('src',tmp_img1.src);

			}

		});

	$('#Image16')

		.click(function(){

			resetImgBtn()

			$(this).addClass('current');

			$(this).attr('src',tmp_img2_2.src);

			$.ajax({

				url:'about05_4_2.php',

				success:function(data){$('#info_block').html(data)}

			});

		})

		.hover(function(){

			$(this).attr('src',tmp_img2_2.src);

		},function(){

			if($(this).attr('class') != 'current'){

				$(this).attr('src',tmp_img2.src);

			}

		});

	$('#Image17')

		.click(function(){

			resetImgBtn()

			$(this).addClass('current');

			$(this).attr('src',tmp_img3_2.src);

			$.ajax({

				url:'about05_4_3.php',

				success:function(data){$('#info_block').html(data)}

			});

		})

		.hover(function(){

			$(this).attr('src',tmp_img3_2.src);

		},function(){

			if($(this).attr('class') != 'current'){

				$(this).attr('src',tmp_img3.src);

			}

		});

	$('#Image18')

		.click(function(){

			resetImgBtn()

			$(this).addClass('current');

			$(this).attr('src',tmp_img4_2.src);

			$.ajax({

				url:'about05_4_4.php',

				success:function(data){$('#info_block').html(data)}

			});

		})

		.hover(function(){

			$(this).attr('src',tmp_img4_2.src);

		},function(){

			if($(this).attr('class') != 'current'){

				$(this).attr('src',tmp_img4.src);

			}

		});

	function resetImgBtn(){

		$('#img_btns img').removeClass('current');

		$('#Image15').attr('src',tmp_img1.src);

		$('#Image16').attr('src',tmp_img2.src);

		$('#Image17').attr('src',tmp_img3.src);

		$('#Image18').attr('src',tmp_img4.src);

	}

});
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php">关于台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
           <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05_3.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
         
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05-2.jpg" alt="" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/about/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_3.php"><img src="images/about/b05-3.jpg" alt="" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/about/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b05-4_2.jpg" alt="" name="Image10" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/about/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="about05_5.php"><img src="images/about/b05-5.jpg" alt="" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/about/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_6.php"><img src="images/about/b05-6.jpg" alt="" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/about/b05-6_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_7.php"><img src="images/about/b05-7.jpg" alt="" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/about/b05-7_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_8.php"><img src="images/about/b05-8.jpg" alt="" width="171" height="22" id="Image14" onmouseover="MM_swapImage('Image14','','images/about/b05-8_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top_6"><img src="../TW/images/about/pic01_5_4.jpg" width="688" height="120" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" --><div id="content_main">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">优厚福利，关爱照顾员工</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡公司秉持核心价值「以人为本」，透过技术、安全卫生文化、责任、沟通等原则，追求零灾害、零伤害目标，并依据劳动基准法、安全卫生法等相关法规为基础，制定并依法执行相关劳动条件业务事项外，且优于法令每年定期实施员工健康检查，以维护员工身心健康：</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td class="gray12"><table width="632" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="140" align="left" valign="top"><table width="130" border="0" cellspacing="0" cellpadding="5" id="img_btns">
                      <tr>
                        <td width="130"><a href="javascript:void(0)"><img src="images/about/b05-4-1_2.png" class="current" alt="" width="66" height="12" id="Image15" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-4-2.png" alt="" width="126" height="13" id="Image16" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-4-3.png" alt="" width="66" height="13" id="Image17" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-4-4.png" alt="" width="66" height="12" id="Image18" /></a></td>
                      </tr>
                    </table></td>
                  <td width="25" align="center" valign="top"><img src="images/line_straight02.png" alt="" width="25" height="165" /></td>
                  <td width="477" align="left" valign="top"><table width="477" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="26" align="left" valign="top"><img src="../TW/images/about/arrow.png" alt="" width="26" height="21" /></td>
                        <td class="gray12_3" id="info_block">本公司透过职工福利委员会之运作，提供员工三节、生日、劳动节等礼金；实施员工「自选式福利措施」，员工每年以『福利点券』组合自身需求之福利项目，包含旅游休閒活动 、子女教育补助、团购用品..等，真正落实员工福利之实质内涵。而员工依法享有劳、健保外，公司并免费提供员工团体保险，且涵盖员工眷属；在劳工退休制度方面则依循劳基法及劳工退休制度之规定办理，且透过「劳工退休准备金监督委员会」，定期开会审阅监督退休金使用状况，及每年精算退休金提拨状况，以保障员工退休金权益。</td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      </div><!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
