<?php
require_once('../db.php');
$report = $db->reports()->where('lang','cn')->order('year desc,weight desc');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/service_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>投资人服务─每月营业额、财务报告</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','images/service/b01_2.jpg','images/service/b02_2.jpg','images/service/b02-1_2.jpg','images/service/b04_2.jpg','images/service/b05-1_2.jpg','images/service/b05-2_2.jpg','images/service/b05-4_2.jpg','images/service/b05-5_2.jpg','images/service/b03_2.jpg','images/service/b05_2.jpg','images/service/b05-3_2.jpg','images/service/b02-2_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php"><span class="gray12_2">关于台橡</span></a>　│　</span><span class="blue12"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="service.php"><img src="images/service/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/service/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service02.php"><img src="images/service/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/service/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/service/b02-1_2.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/service/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><img src="images/service/b02-2_2.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/service/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="service03.php"><img src="images/service/b03.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/service/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service04.php"><img src="images/service/b04.jpg" name="Image7" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/service/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05.jpg" name="Image8" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/service/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-1.jpg" name="Image9" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/service/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-2.jpg" name="Image10" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/service/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-3.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/service/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-4.jpg" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/service/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="service05.php"><img src="images/service/b05-5.jpg" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/service/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" --><div id="content_main_6">

        <table width="688" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td>        <div id="content_main_7">

          <table width="415" border="0" align="center" cellpadding="0" cellspacing="0">

            <tr>

              <td height="15" colspan="2">&nbsp;</td>

            </tr>

            <tr>

              <td colspan="2" class="green14_b">每月营业额</td>

            </tr>

            <tr>

             <td width="83"><span class="gray12">2012/02/12</span></td>

              <td width="332"><span class="gray12">2012年01月份营业收入</span><span class="green12">11.30</span><span class="gray12">亿元，较去年同期减少 14%</span></td>

            </tr>
            <tr>
              <td><span class="gray12">2012/01/12</span></td>
              <td width="332"><span class="gray12">2011年12月份营业收入</span><span class="green12">23.40</span><span class="gray12">亿元，较去年同期增加 42%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/12/12</span></td>
              <td width="332"><span class="gray12">2011年11月份营业收入</span><span class="green12">15.30</span><span class="gray12">亿元，较去年同期增加 13%</span></td>
            </tr>
            <tr>
              <td width="83"><span class="gray12">2011/11/12</span></td>
              <td width="332"><span class="gray12">2011年10月份营业收入</span><span class="green12">12.63</span><span class="gray12">亿元，较去年同期减少 1%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/10/12</span></td>
              <td width="332"><span class="gray12">2011年09月份营业收入</span><span class="green12">18.75</span><span class="gray12">亿元，较去年同期增加 41%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/09/12</span></td>
              <td width="332"><span class="gray12">2011年08月份营业收入</span><span class="green12">18.06</span><span class="gray12">亿元，较去年同期增加 52%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/08/12</span></td>
              <td width="332"><span class="gray12">2011年07月份营业收入</span><span class="green12">16.02</span><span class="gray12">亿元，较去年同期增加 41%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/07/12</span></td>
              <td width="332"><span class="gray12">2011年06月份营业收入</span><span class="green12">19.66</span><span class="gray12">亿元，较去年同期增加 30%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/06/12</span></td>
              <td width="332"><span class="gray12">2011年05月份营业收入</span><span class="green12">19.04</span><span class="gray12">亿元，较去年同期增加 41%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/05/12</span></td>
              <td width="332"><span class="gray12">2011年04月份营业收入</span><span class="green12">14.89</span><span class="gray12">亿元，较去年同期增加 40%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/04/12</span></td>
              <td width="332"><span class="gray12">2011年03月份营业收入</span><span class="green12">19.59</span><span class="gray12">亿元，较去年同期增加 38%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/03/12</span></td>
              <td width="332"><span class="gray12">2011年02月份营业收入</span><span class="green12">15.60</span><span class="gray12">亿元，较去年同期增加 79%</span></td>
            </tr>
            <tr>
              <td><span class="gray12">2011/02/12</span></td>
              <td width="332"><span class="gray12">2011年01月份营业收入</span><span class="green12">13.20</span><span class="gray12">亿元，较去年同期增加 19%</span></td>
            </tr>

            <tr>

              <td><span class="gray12">2011/01/12</span></td>

              <td width="332"><span class="gray12">2010年12月份营业收入</span><span class="green12">16.50</span><span class="gray12">亿元，较去年同期增加 41%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/12/12</span></td>

              <td><span class="gray12">2010年11月份营业收入</span><span class="green12">13.50</span><span class="gray12">亿元，较去年同期增加 52%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/11/12</span></td>

              <td><span class="gray12">2010年10月份营业收入</span><span class="green12">12.80</span><span class="gray12">亿元，较去年同期增加 61%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/10/12</span></td>

              <td><span class="gray12">2010年09月份营业收入</span><span class="green12">13.31</span><span class="gray12">亿元，较去年同期增加 47%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/09/12</span></td>

              <td><span class="gray12">2010年08月份营业收入</span><span class="green12">11.86</span><span class="gray12">亿元，较去年同期增加 37%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/08/12</span></td>

              <td><span class="gray12">2010年07月份营业收入</span><span class="green12">11.36</span><span class="gray12">亿元，较去年同期增加 25%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/07/12</span></td>

              <td><span class="gray12">2010年06月份营业收入</span><span class="green12">15.13</span><span class="gray12">亿元，较去年同期增加 47%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/06/12</span></td>

              <td><span class="gray12">2010年05月份营业收入</span><span class="green12">13.55</span><span class="gray12">亿元，较去年同期增加 88%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/05/12</span></td>

              <td><span class="gray12">2010年04月份营业收入</span><span class="green12">10.6</span><span class="gray12">亿元，较去年同期增加 23%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/04/12</span></td>

              <td><span class="gray12">2010年03月份营业收入</span><span class="green12">14.2</span><span class="gray12">亿元，较去年同期增加 39%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/03/12</span></td>

              <td><span class="gray12">2010年02月份营业收入</span><span class="green12">8.70</span><span class="gray12">亿元，较去年同期增加 18%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/02/12</span></td>

              <td><span class="gray12">2010年01月份营业收入</span><span class="green12">11.10</span><span class="gray12">亿元，较去年同期增加 166%</span></td>

            </tr>

            <tr>

              <td><span class="gray12">2010/01/12</span></td>

              <td><span class="gray12">2009年12月份营业收入</span><span class="green12">11.70</span><span class="gray12">亿元，较去年同期增加 32.5%</span></td>

            </tr>

          </table>

        </div>

        <div id="content_main_8">

          <table width="250" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td align="center"><table width="238" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr>

                  <td height="15" colspan="3" >&nbsp;</td>

                </tr>

                <tr>

                  <td colspan="3" align="left" class="green14_b">财务报告</td>

                </tr>
                <?php
                $year = '';
                foreach($report as $t){
                  if($year == ''){
                    $year = $t['year'];
                  }
                  if($year != '' && $year != $t['year']){
                    echo '<tr>';
                    echo '<td colspan="3" align="left" class="green14_b"><img src="images/line.png" alt="" width="238" height="25" /></td>';
                    echo '</tr>';
                    $year = $t['year'];
                  }
                  echo '<tr>';
                  echo '<td width="130"><span class="gray12"><a href="../admin/public/upload/'.$t['uuid'].'" target="_blank">'.$t['name'].'</a></span></td>';
                  echo '<td width="24"><a href="../admin/public/upload/'.$t['uuid'].'" target="_blank"><img src="../images/pdf.png" alt="" width="20" height="20" /></a></td>';
                  echo '<td width="84"><span class="gray12">( 約'.round($t['size']/1024).'KB)</span></td>';
                  echo '</tr>';
                }
                ?>

                <tr>

                  <td colspan="3">&nbsp;</td>

                </tr>

                <tr>

                  <td colspan="3" class="gray12">最近更新时间：2011/11/29</td>

                </tr>

              </table></td>

            </tr>

            <tr>

              <td>&nbsp;</td>

            </tr>

            <tr>

              <td><img src="images/service/contact.jpg" width="250" height="115" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" coords="53,60,235,79" href="http://newmops.tse.com.tw" target="_blank" />

                </map></td>

            </tr>

          </table>

        </div></td>

          </tr>

          <tr>

            <td>&nbsp;</td>

          </tr>

        </table>



      </div><!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
