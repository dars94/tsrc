<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
<link href="css/text.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="385" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ball">
        <tr>
          <td align="center"><table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right"><a href="#"><img class='close_btn' src="../TW/images/about/close.jpg" width="25" height="25" /></a></td>
              </tr>
              <tr>
                <td align="left"><span class="orange12">温室气体</span><span class="green12_3">排放管理是全球性的议题，也是石化产业所需面临的挑战。台橡公司本着「珍惜地球资源，善尽环保责任」的信念，自1973年成立以来，即持续不断朝向节约能源、能资源与原物料回收及製程效率提昇方面做努力。 对于主管机关的温室气体减量推动，台橡公司配合措施如下：</span></td>
              </tr>
              <tr>
                <td align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="6%" align="left" valign="top"><span class="green12_3">(1)</span></td>
                    <td width="94%" align="left" valign="top"><span class="green12_3">经济部委託之台湾绿色生产力基金会，查证台橡公司自2004~2008年『自愿性节约能源与温室气体减量』，共减少37,520公吨
CO2当量之排放。</span></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><span class="green12_3">(2)</span></td>
                    <td align="left" valign="top"><span class="green12_3">为达成温室气体自愿减量的目标，已于2010年与经济部签署减量自愿协议书，预定使2011~2015年温室气体之排放再减少4,022公吨CO2当量。</span></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><span class="green12_3">(3)</span></td>
                    <td align="left" valign="top"><span class="green12_3">计画于2011年完成高雄厂组织型温室气体盘查(ISO 14064-1)认证，并做为后续温室气体减量之参考。</span></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
