<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/research_c.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>新产品开发</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){
	$('#item_block_1').css('display','none');
	$('#btn01_d').click(function(){
		$('#item_block_1').slideDown();
		$(this).css('display','none');
	});
	$('#btn01_u').click(function(){
		$('#item_block_1').slideUp();
		$('#btn01_d').css('display','');
	});
})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('../TW/images/go_2.jpg','../TW/images/research/b01_2.jpg','../TW/images/research/b02_2.jpg','../TW/images/research/b03_2.jpg','images/research/b01_2.jpg','images/research/b02_2.jpg','images/research/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="../TW/images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首页</a></span><span class="blue12">　│　<a href="about.php"><span class="gray12_2">关于台橡</span></a>　│　</span><span class="gray12_2"><a href="service.php">投资人服务</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">产品</a></span><span class="blue12">　│　</span><span class="blue12"><a href="research.php">研究与发展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新闻</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集团成员</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜索</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="../TW/images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','../TW/images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><img src="images/research/b01_2.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/research/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
          <tr>
            <td><a href="research02.php"><img src="images/research/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/research/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="research03.php"><img src="images/research/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/research/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="top" -->
      <div id="content_top">
        <table width="688" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="184"><img src="../TW/images/research/pic01_1.jpg" alt="" width="184" height="198" /></td>
            <td width="5">&nbsp;</td>
            <td width="310"><img src="../TW/images/research/pic02_1.jpg" width="310" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="184"><img src="../TW/images/research/pic03_1.jpg" width="184" height="198" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable -->
      <div id="content_main"><!-- InstanceBeginEditable name="main" -->
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td class="gray12_3">新产品的开发牵涉新产品、新制程、新应用的各类层面，必须整合各类资源，包括行销、生产、研发等各部门的协同合作才能成功。台橡的研发团队以客为尊，藉着长期与客户密切的接触，深入了解客户的需求，以便开发出符合客户所需的产品，台橡因应时势的变动而调整改善开发制度，不断导入崭新观念与做法，多年来成果卓着：</td>
          </tr>
          <tr>
            <td align="center" class="gray12"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="50%" align="left">• 开发环境友善的加油型SBR、BR</td>
                </tr>
                <tr>
                  <td align="left">• 符合国际重要客户要求的高功能SIS、SBS、SEBS</td>
                </tr>
                <tr>
                  <td align="left">• 具创新技术的SEBS生产制程</td>
                </tr>
                <tr>
                  <td align="left">• 符合FDA要求的SIS、SEBS</td>
                </tr>
                <tr>
                  <td align="left">• 具高耐油性的NBR橡胶生产制程</td>
                </tr>
                <tr>
                  <td align="left">• 高性能星状SIS产品</td>
                </tr>
                <tr>
                  <td align="left">• 具高弹性膜特性的四嵌段SIBS产品</td>
                </tr>
                <tr>
                  <td align="left">• 可供膜切黏胶应用的SBC产品</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="gray12">目前，台橡研发中的项目还有数十项，主要是<span class="gray12-b">特殊性能、高附加价值及环保的新型产品</span>。</td>
          </tr>
          <tr>
            <td class="green12"><div id="item_2">2010年环保节能高性能SSBR橡胶研发进展<a href="#"><img src="../TW/images/down.png" name="btn01" width="17" height="14" id="btn01_d" /></a></div></td>
          </tr>
          <tr>
            <td ><div id="item_block_1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="gray12_3">因应下游产业发展之变化及客户对环境保护意识的需求提高，环保型新产品&quot;溶聚苯乙烯—丁二烯橡胶SSBR&quot;之开发即为本公司近年发展的重要项目之一：</td>
                  </tr>
                  <tr>
                    <td class="gray12_3">由于环保、节能轮胎(Green Tire)渐成发展趋势（欧盟已要求于2012年输入之轮胎须标示其燃油效率Fuel Efficiency、湿抓地力Wet Grip Performance、滚动阻力Rolling Resistance和噪音水准Noise Level等），而要符合Green Tire特性需求使用SSBR/Silica配方技术才能成功，因此，轮胎业必须发展新技术与材料，否则未来在市场上将被淘汰或排挤。而目前国内对于使用于轮胎等级之SSBR并无实际生产厂商，对于环保轮胎使用之改质SSBR均须仰赖进口，于是台橡积极发起与下游厂商联盟，结合上游橡胶业发展新一代的SSBR，并配合下游轮胎厂发展适合Green Tire特性需求的SSBR/Silica配方技术，共同开发环保节能轮胎产品。</td>
                  </tr>
                  <tr>
                    <td class="gray12_3">台橡公司主要关键技术为阴离子聚合微观结构控制及制程开发设计技术，以及相关的末端改质技术和已量产的TPE(热可塑橡胶)生产经验。下游轮胎厂主要关键技术包括：SSBR与Filler(如碳黑或Silica)的混练分散技术开发适当Silica与偶合剂的配方开发；轮胎各部材之能量损失比例预估技术开发，及非线性有限元素分析(FEM)技术及自主的轮胎设计技术。 即以上下游研发联盟形式进行研发合作，由台橡公司开发及提供改质之SSBR产品，下游轮胎厂则以此材料开发一系列之节能环保轮胎设计与制造。</td>
                  </tr>
                  <tr>
                    <td class="gray12_3">以上下游产业合组研发联盟方式 ，台橡建立与下游客户共开发之模式，将可帮助国内产业摆脱国外技术限制，大幅提升竞争力。<a href="#"><img src="../TW/images/up.png" alt="" name="btn02" width="17" height="14" id="btn01_u" /></a></td>
                  </tr>
                </table></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">网站导览</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
