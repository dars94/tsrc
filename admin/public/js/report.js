var report_ds = Ext.create('Ext.data.Store',{
	storeId:'report_ds',
	fields:['id','name','uuid','created_at','updated_at','lang','weight',{name:'year',type:'int'}],
	groupField: 'year',
	proxy:{
		type:'ajax',
		url:base_url+'reports/lists',
		reader:{
			type:'json',
			root:'root',
			totalProperty:'total'
		}
	},
	pageSize:25,
	autoLoad:true
});
report_ds.sort([
	{property:'year',direction:'DESC'},
	{property:'lang',direction:'DESC'},
	{property:'weight',direction:'DESC'},
]);
Ext.define('Report', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'year', type: 'string'},
		{name: 'name', type: 'string'},
		{name: 'uuid'},
		{name: 'lang'}
	]
});

var report_filter_lang_cb = Ext.create('Ext.form.ComboBox', {
    editable:false,
    store: filter_lang_store,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'val',
    emptyText: '全部',
    id:'report_lang_filter',
    name:'lang_filter',
});

var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
	groupHeaderTpl: '民國 {name} 年度 ({rows.length} 筆資料)'
});
var reports = Ext.create('Ext.grid.Panel',{
	id:'tab_reports',
	layout:'fit',
	store:report_ds,
	title:'財務報告',
	features: [groupingFeature],
	viewConfig: {
        plugins: {
            ptype: 'gridviewdragdrop',
            dragText: '調整資料排序'
        },
        listeners:{
			drop:function(node,data){
				sortReports();
			}
		}
    },
	columns:[
		Ext.create('Ext.grid.RowNumberer'),{
			dataIndex:'id',
			hidden:true,
			id:'report_id',
		},{
			header:'年度',
			dataIndex:'year',
			width:80,
			menuDisabled:true,
			sortable:false,
		},{
			header:'標題',
			dataIndex:'name',
			flex:1,
			menuDisabled:true,
			sortable:false,
		},{
			header:'文件檔案',
			dataIndex:'uuid',
			width:200,
			menuDisabled:true,
			renderer:trans_link,
			sortable:false,
		},
		{header:'建立時間',dataIndex:'created_at',sortable:false,width:160,menuDisabled:true},
		{header:'更新時間',dataIndex:'updated_at',sortable:false,width:160,menuDisabled:true},
		{
			header:'語系',
			dataIndex:'lang',
			width:80,
			menuDisabled:true,
			renderer:trans_lang,
			sortable:false,
		}
	],
	tbar:[report_filter_lang_cb,
	{xtype:'tbfill'},
	{
		xtype:'button',
		text:'新增',
		id:'add_report',
		iconCls:'ss_sprite ss_add',
		handler:function(){open_report_form()}
	},{
		type:'button',
		text:'編輯',
		id:'edit_report',
		iconCls:'ss_sprite ss_pencil',
		handler:function(){edit_report()}
	},{
		type:'button',
		text:'刪除',
		id:'dele_report',
		iconCls:'ss_sprite ss_delete',
		handler:function(){dele_report()}
	}],
});

var report_lang_cb = Ext.create('Ext.form.ComboBox', {
    fieldLabel: '語系',
    labelAlign: 'right',
    store: lang_store,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'val',
    allowBlank:false,
    editable:false,
    name:'lang'
});

var report_form = Ext.create('Ext.form.Panel',{
	url:base_url+'reports/save',
	bodyPadding:15,
	frame:true,
	defaults:{
		anchor:'95%',
		labelAlign:'right'
	},
	defaultType:'textfield',
	items:[
		{xtype:'hidden',name:'id'},
		{fieldLabel:'年度',name:'year',allowBlank:false},
		{fieldLabel:'標題',name:'name',allowBlank:false},
		{fieldLabel:'文件檔案',name:'uuid',xtype:'filefield'},
		report_lang_cb
	],
	buttons:[
		{text:'取消',handler:function(){
			this.up('form').getForm().reset();
			report_form_win.close();
		}},
		{text:'送出',id:'report_sub_btn',handler:function(){
			var form = this.up('form').getForm();
			if(form.isValid()){
				form.submit({
					success:function(form,action){
						report_form.getForm().reset();
						report_form_win.close();
						report_ds.load({params:{lang: report_filter_lang_cb.getValue()}});
					},
					failure:function(form,action){
						alert('儲存失敗');
						report_ds.load({params:{lang: report_filter_lang_cb.getValue()}});
					}
				});
			}else{
				alert('請確實填寫所有欄位資訊');
			}
		}}
	]
});
var report_form_win = Ext.create('Ext.window.Window',{
	title:'財務報告表單',
	width:800,
	height:270,
	layout:'fit',
	plain:true,
	closeAction:'hide',
	modal:true,
	items:report_form
});
report_form_win.on('hide',function(){
	report_form.getForm().reset();
});