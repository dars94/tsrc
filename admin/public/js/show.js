var show_ds = Ext.create('Ext.data.Store',{
	storeId:'show_ds',
	fields:['id','datetime','title','content','hot','link','created_at','updated_at','lang'],
	proxy:{
		type:'ajax',
		url:base_url+'shows/lists',
		reader:{
			type:'json',
			root:'root',
			totalProperty:'total'
		}
	},
	pageSize:25,
	autoLoad:true
});

Ext.define('Plant', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'datetime', type: 'string'},
		{name: 'title', type: 'string'},
		{name: 'lang'}
	]
});

var show_filter_lang_cb = Ext.create('Ext.form.ComboBox', {
    editable:false,
    store: filter_lang_store,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'val',
    emptyText: '全部',
    id:'show_lang_filter',
    name:'lang_filter',
});

var showRowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
	clicksToMoveEditor: 2,
	errorSummary:false
});
var show_lang_cb = Ext.create('Ext.form.ComboBox', {
    fieldLabel: '語系',
    store: lang_store,
    labelAlign: 'right',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'val',
    allowBlank:false,
    editable:false,
    name:'lang'
});
var hot_store = Ext.create('Ext.data.Store',{
	fields:['val','name'],
	data:[
		{"val":"0","name":"否"},
		{"val":"1","name":"是"}
	]
});
var hot_cb = Ext.create('Ext.form.ComboBox', {
    fieldLabel: 'HOT',
    store: hot_store,
    labelAlign: 'right',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'val',
    allowBlank:true,
    editable:false,
    name:'hot'
});
var shows = Ext.create('Ext.grid.Panel',{
	id:'tab_shows',
	layout:'fit',
	store:show_ds,
	title:'展覽與研討會',
	viewConfig: {
        plugins: {
            ptype: 'gridviewdragdrop',
            dragText: '調整資料排序'
        },
        listeners:{
			drop:function(node,data){
				sortShows();
			}
		}
    },
	columns:[
		Ext.create('Ext.grid.RowNumberer'),{
			dataIndex:'id',
			hidden:true,
			id:'show_id',
			editor:{
				xtype:'hidden'
			}
		},{
			header:'日期',
			dataIndex:'datetime',
			width:160,
			menuDisabled:true,
			sortable:false,
			editor:{
				allowBlank:false,
				maxLength:14
			}
		},{
			header:'標題',
			dataIndex:'title',
			flex:1,
			menuDisabled:true,
			sortable:false,
			editor:{
				allowBlank:false
			}
		},
		{header:'建立時間',dataIndex:'created_at',sortable:false,width:160,menuDisabled:true},
		{header:'更新時間',dataIndex:'updated_at',sortable:false,width:160,menuDisabled:true},
		{
			header:'語系',
			dataIndex:'lang',
			width:80,
			menuDisabled:true,
			renderer:trans_lang,
			sortable:false,
			editor:{
				xtype:'combobox',
				store: lang_store,
    			queryMode: 'local',
    			displayField: 'name',
    			valueField: 'val',
    			allowBlank:false,
    			editable:false,
    			name:'lang'
			}
		}
	],
	tbar:[show_filter_lang_cb,
	{xtype:'tbfill'},
	{
		xtype:'button',
		text:'新增',
		iconCls:'ss_sprite ss_add',
		handler:function(){
			var r = Ext.create('Plant');
            show_ds.insert(0, r);
            showRowEditing.startEdit(0,2);
		}
	},{
		type:'button',
		text:'編輯',
		id:'edit_show',
		iconCls:'ss_sprite ss_pencil',
		handler:function(){edit_show()}
	},{
		type:'button',
		text:'刪除',
		id:'dele_show',
		iconCls:'ss_sprite ss_delete',
		handler:function(){dele_show()}
	}],
	plugins: [showRowEditing]
});
shows.on('edit',function(editor){
	Ext.Ajax.request({
		url:base_url+'shows/save',
		params:editor.record.data,
		success:function(res){
			show_ds.load({params:{lang: show_filter_lang_cb.getValue()}});
		}
	});
});

var show_form = Ext.create('Ext.form.Panel',{
	url:base_url+'shows/update_show',
	bodyPadding:15,
	frame:true,
	defaults:{
		anchor:'95%',
		labelAlign:'right'
	},
	defaultType:'textfield',
	items:[
		{xtype:'hidden',name:'id'},
		{fieldLabel:'標題',name:'title',allowBlank:false},
		{
			xtype:'CKEditor',
			inputId:'show_content',
			config:{CKConfig:'public/ckeditor/config.js'},
			fieldLabel:'內文',
			name:'content',
			allowBlank:true
		},
		{fieldLabel:'連結',name:'link'},
		hot_cb,
		{fieldLabel:'日期',name:'datetime',allowBlank:false},
		show_lang_cb
	],
	buttons:[
		{text:'取消',handler:function(){
			this.up('form').getForm().reset();
			show_form_win.close();
		}},
		{text:'送出',id:'show_sub_btn',handler:function(){
			var form = this.up('form').getForm();
			if(form.isValid()){
				form.submit({
					success:function(form,action){
						show_form.getForm().reset();
						show_form_win.close();
						show_ds.load({params:{lang: show_filter_lang_cb.getValue()}});
					},
					failure:function(form,action){
						alert('儲存失敗');
						show_ds.load({params:{lang: show_filter_lang_cb.getValue()}});
					}
				});
			}else{
				alert('請確實填寫所有欄位資訊');
			}
		}}
	]
});
var show_form_win = Ext.create('Ext.window.Window',{
	title:'展覽與研討會表單',
	width:800,
	height:500,
	layout:'fit',
	plain:true,
	closeAction:'hide',
	modal:true,
	items:show_form
});
show_form_win.on('hide',function(){
	show_form.getForm().reset();
});
