Ext.define('CKEditor',{
	extend:'Ext.form.TextArea',
	alias:'widget.CKEditor',
	onRender:function(ct,position){
		// console.log('onRender');
		if(!this.el){
			this.defaultAutoCreate = {
				tag:'textarea',
				autocomplete:'off'
			}
		}
		Ext.form.TextArea.superclass.onRender.call(this,ct,position);
		CKEDITOR.replace(this.inputId,this.config.CKConfig);
	},
	setValue:function(value){
		// console.log('setValue');
		if(typeof value != 'undefined'){
			Ext.form.TextArea.superclass.setValue.apply(this,[value]);
			CKEDITOR.instances[this.inputId].setData(value);
		}else{
			if(typeof CKEDITOR.instances[this.inputId] != 'undefined'){
				Ext.form.TextArea.superclass.setValue('');
				CKEDITOR.instances[this.inputId].setData('');
			}
		}
	},
	getValue:function(){
		// console.log('getValue');
		// CKEDITOR.instances[this.inputId].updateElement();
		return Ext.form.TextArea.superclass.getValue(this);
	},
	getRawValue:function(){
		// console.log('getRawValue');
		return CKEDITOR.instances[this.inputId].getData();
	},
});