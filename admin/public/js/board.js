var board_ds = Ext.create('Ext.data.Store',{
	storeId:'board_ds',
	fields:['id','title','content','created_at','post_date','updated_at','lang'],
	proxy:{
		type:'ajax',
		url:base_url+'boards/admin_index',
		reader:{
			type:'json',
			root:'root',
			totalProperty:'total'
		}
	},
	pageSize:25,
	autoLoad:true
});
var board_filter_lang_cb = Ext.create('Ext.form.ComboBox', {
    editable:false,
    store: filter_lang_store,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'val',
    emptyText: '全部',
    id:'board_lang_filter',
    name:'lang_filter',
});

var board = Ext.create('Ext.grid.Panel',{
	id:'tab_board',
	layout:'fit',
	store:board_ds,
	title:'新聞列表',
	columns:[
		Ext.create('Ext.grid.RowNumberer'),
		{dataIndex:'id',hidden:true,sortable:false,id:'id'},
		{header:'標題',dataIndex:'title',flex:1,sortable:false,menuDisabled:true},
		{header:'發佈時間',dataIndex:'post_date',sortable:false,menuDisabled:true,maxLength:10},
		{header:'建立時間',dataIndex:'created_at',width:160,sortable:false,menuDisabled:true},
		{header:'更新時間',dataIndex:'updated_at',width:160,sortable:false,menuDisabled:true},
		{header:'語系',dataIndex:'lang',width:80,menuDisabled:true,sortable:false,renderer:trans_lang}
	],
	dockedItems:[{
		xtype:'pagingtoolbar',
		store:board_ds,
		dock:'bottom',
		beforePageText:'第',
		afterPageText:'頁共 {0} 頁',
		displayMsg:'目前顯示第 {0} - {1} 筆,共 {2} 筆資料',
		displayInfo:true
	}],
	tbar:[board_filter_lang_cb,
	{xtype:'tbfill'},
	{
		xtype:'button',
		text:'新增',
		id:'add_board',
		iconCls:'ss_sprite ss_add',
		handler:function(){open_form()}
	},{
		type:'button',
		text:'編輯',
		id:'edit_board',
		iconCls:'ss_sprite ss_pencil',
		handler:function(){edit_board()}
	},{
		type:'button',
		text:'刪除',
		id:'dele_board',
		iconCls:'ss_sprite ss_delete',
		handler:function(){dele_board()}
	}]
});
var board_lang_cb = Ext.create('Ext.form.ComboBox', {
    fieldLabel: '語系',
    store: lang_store,
    labelAlign: 'right',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'val',
    allowBlank:false,
    editable:false,
    name:'lang'
});
var board_form = Ext.create('Ext.form.Panel',{
	url:base_url+'boards/update_board',
	bodyPadding:15,
	frame:true,
	defaults:{
		anchor:'95%',
		labelAlign:'right'
	},
	defaultType:'textfield',
	items:[
		{xtype:'hidden',name:'id'},
		{fieldLabel:'標題',name:'title',allowBlank:false},
		{
			xtype:'CKEditor',
			inputId:'board_content',
			config:{CKConfig:'public/ckeditor/config.js'},
			fieldLabel:'內文',
			name:'content',
			allowBlank:false
		},
		{fieldLabel:'發佈時間',name:'post_date',allowBlank:false},
		board_lang_cb
	],
	buttons:[
		{text:'取消',handler:function(){
			this.up('form').getForm().reset();
			board_form_win.close();
		}},
		{text:'送出',id:'board_sub_btn',handler:function(){
			var form = this.up('form').getForm();
			if(form.isValid()){
				form.submit({
					success:function(form,action){
						board_form.getForm().reset();
						board_form_win.close();
						board_ds.load({params:{lang: board_filter_lang_cb.getValue()}});
					},
					failure:function(form,action){
						alert('儲存失敗');
						board_ds.load({params:{lang: board_filter_lang_cb.getValue()}});
					}
				});
			}else{
				alert('請確實填寫所有欄位資訊');
			}
		}}
	]
});
var board_form_win = Ext.create('Ext.window.Window',{
	title:'新聞表單',
	width:800,
	height:470,
	layout:'fit',
	plain:true,
	closeAction:'hide',
	modal:true,
	items:board_form
});
board_form_win.on('hide',function(){
	board_form.getForm().reset();
});