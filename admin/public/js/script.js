/* 語系 ds */
var filter_lang_store = Ext.create('Ext.data.Store',{
	fields:['val','name'],
	data:[
		{"val":0,"name":"全部"},
		{"val":"tw","name":"繁體中文"},
		{"val":"cn","name":"簡體中文"},
		{"val":"en","name":"English"}
	]
});
var lang_store = Ext.create('Ext.data.Store',{
	fields:['val','name'],
	data:[
		{"val":"tw","name":"繁體中文"},
		{"val":"cn","name":"簡體中文"},
		{"val":"en","name":"English"}
	]
});

/* render */
function trans_lang(v){
	switch(v){
		case 'tw':
			return '<span class="lang_tw">繁體中文</span>';
			break;
		case 'cn':
			return '<span class="lang_cn">簡體中文</span>';
			break;
		case 'en':
			return '<span class="lang_en">English</span>';
			break;
	}
}
function trans_link(v){
	if(typeof v != 'object'){
		return '<a href="'+base_url+'public/upload/'+v+'" target="_blank">'+v+'</a>';
	}else{
		return '';
	}
}

function open_form(){
	board_form_win.show();
}
function edit_board(){
	var record = board.getSelectionModel().getSelection();
	var record = board_ds.getById(record[0].get('id'));
	board_form_win.show();
	setTimeout(function(){
		board_form.getForm().findField('id').setValue(record.data.id);
		board_form.getForm().findField('title').setValue(record.data.title);
		board_form.getForm().findField('content').setValue(record.data.content);
		board_form.getForm().findField('lang').setValue(record.data.lang);
	},500);
}
function dele_board(){
	var record = board.getSelectionModel().getSelection();
	if(record.length<1){
		alert('請選擇要刪除的資料');
		return false;
	}else{
		if(confirm('確定要刪除此筆資料？ #'+record[0].get('id'))){
			Ext.Ajax.request({
				url:base_url+'boards/dele_board',
				success:function(){
					board_ds.load();
				},
				params:{'id':record[0].get('id')}
			});
		}
	}
}

function edit_show(){
	var record = shows.getSelectionModel().getSelection();
	var record = show_ds.getById(record[0].get('id'));
	show_form_win.show();
	setTimeout(function(){
		show_form.getForm().findField('id').setValue(record.data.id);
		show_form.getForm().findField('title').setValue(record.data.title);
		show_form.getForm().findField('content').setValue(record.data.content);
		show_form.getForm().findField('lang').setValue(record.data.lang);
		show_form.getForm().findField('link').setValue(record.data.link);
		show_form.getForm().findField('hot').setValue(record.data.hot);
		show_form.getForm().findField('datetime').setValue(record.data.datetime);
	},500);
}
function dele_show(){
	var record = shows.getSelectionModel().getSelection();
	if(record.length<1){
		alert('請選擇要刪除的資料');
		return false;
	}else{
		if(confirm('確定要刪除此筆資料？ #'+record[0].get('id'))){
			Ext.Ajax.request({
				url:base_url+'shows/dele_show',
				success:function(){
					show_ds.load();
				},
				params:{'id':record[0].get('id')}
			});
		}
	}
}

function open_report_form(){
	report_form_win.show();
}
function edit_report(){
	var record = reports.getSelectionModel().getSelection();
	console.log(record[0].get('id'));
	var record = report_ds.getById(record[0].get('id'));
	report_form_win.show();
	setTimeout(function(){
		report_form.getForm().findField('id').setValue(record.data.id);
		report_form.getForm().findField('name').setValue(record.data.name);
		report_form.getForm().findField('year').setValue(record.data.year);
		report_form.getForm().findField('lang').setValue(record.data.lang);
	},500);
}
function dele_report(){
	var record = reports.getSelectionModel().getSelection();
	if(record.length<1){
		alert('請選擇要刪除的資料');
		return false;
	}else{
		if(confirm('確定要刪除此筆資料？ #'+record[0].get('id'))){
			Ext.Ajax.request({
				url:base_url+'reports/dele_report',
				success:function(){
					report_ds.load();
				},
				params:{'id':record[0].get('id')}
			});
		}
	}
}
function sortReports(){
	var tmp = reports.getView().getRecords(reports.getView().getNodes());
	var length = tmp.length;
	var i = 0;
	var ar = new Array();
	while(i < length){
		ar.push(tmp[i].data.id);
		i++;
	}
	Ext.Ajax.request({
		url:base_url+'reports/save_sort',
		params:{'length':length,'sort':ar.join(',')},
		success:function(){
			report_ds.load();
		}
	});
}
function sortShows(){
	var tmp = shows.getView().getRecords(shows.getView().getNodes());
	var length = tmp.length;
	var i = 0;
	var ar = new Array();
	while(i < length){
		ar.push(tmp[i].data.id);
		i++;
	}
	Ext.Ajax.request({
		url:base_url+'shows/save_sort',
		params:{'length':length,'sort':ar.join(',')},
		success:function(){
			show_ds.load();
		}
	});
}