Ext.Loader.setConfig({
	enabled:true
});
Ext.Loader.setPath('Ext.ux','public/ux');
Ext.require([
	'Ext.selection.CellModel',
	'Ext.container.Viewport',
	'Ext.tree.*',
	'Ext.form.*',
	'Ext.layout.container.Absolute',
	'Ext.window.Window'
]);

var mainTab = Ext.create('Ext.tab.Panel',{
	region:'center',
	activeTab:0,
	items:[board,shows,reports]
});

Ext.application({
	name: 'TSRC',
	launch: function() {
		/* create main viewport */
		Ext.create('Ext.container.Viewport', {
			id:'mainviewport',
			layout: 'border',
			items: [{
				region:'north',
				height:50,
				html : '<img src="public/images/admin_logo.png">'
			},mainTab]
		});
	}
});
board_filter_lang_cb.on('change',function(obj,val){
	board_ds.load({params:{lang: val}});
});
show_filter_lang_cb.on('change',function(obj,val){
	show_ds.load({params:{lang: val}});
});
report_filter_lang_cb.on('change',function(obj,val){
	report_ds.load({params:{lang: val}});
});