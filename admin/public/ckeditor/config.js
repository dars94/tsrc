﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.toolbarCanCollapse = false;
	config.resize_enabled = false;
	config.language = 'zh';
	config.uiColor = '#CFDAE7';
	config.colorButton_backStyle = 'transparent';
	config.toolbar = [
		['Maximize','Source'],
		['Bold','Italic','Underline'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight'],
		['Link','Unlink','Anchor'],
		['Image','Table','HorizontalRule'],
		'/',
		['Styles','Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Cut','Copy','Paste','PasteText','PasteFromWord']
	];
	//config.filebrowserUploadUrl = base_url+'admin/ckupload/';
};
