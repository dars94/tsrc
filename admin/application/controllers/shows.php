<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shows extends CI_Controller {
	public function lists()
	{
		if(!empty($_GET['lang'])){
			$this->db->where('lang',$_GET['lang']);
		}
		$this->db->order_by('a.weight DESC,a.id DESC');
		$query = $this->db->get('shows as a');
		$res = new stdClass();
		$res->root = $query->result();
		$res->total = $this->db->count_all('shows');
		echo json_encode($res);
	}
	public function save()
	{
		$time = date('Y-m-d H:i:s');
		$tmp = array(
			'datetime' => $this->input->post('datetime'),
			'title' => $this->input->post('title'),
			'lang' => $this->input->post('lang'),
			'updated_at' => $time
		);
		if($this->input->post('id')){
			$this->db->where('id',$this->input->post('id'));
			$this->db->update('shows',$tmp);
		}else{
			$tmp['created_at'] = $time;
			$this->db->insert('shows',$tmp);
		}
		echo '{"success":true}';
	}
	public function dele_show(){
		$this->db->where('id',$this->input->post('id'));
		$this->db->delete('shows');
		echo '{"success":true}';
	}
	public function save_sort(){
		$tmp = explode(',',$_POST['sort']);
		$i = 0;
		while($i<$_POST['length']){
			$this->db->where('id',$tmp[$i]);
			$this->db->update('shows',array('weight'=>($_POST['length']-$i)));
			$i++;
		}
		echo '{"success":true}';
	}
	public function update_show()
	{
		$time = date('Y-m-d H:i:s');
		$tmp = array();
		$tmp['title'] = $this->input->post('title');
		$tmp['content'] = $this->input->post('content');
		$tmp['updated_at'] = $time;
		$tmp['datetime'] = $this->input->post('datetime');
		$tmp['lang'] = $this->input->post('lang');
		$tmp['link'] = $this->input->post('link');
		$tmp['hot'] = $this->input->post('hot');
		if(strlen($this->input->post('id'))>0)
		{
			$this->db->where('id',$this->input->post('id'));
			$this->db->update('shows',$tmp);
		}
		else
		{
			$tmp['created_at'] = $time;
			$this->db->insert('shows',$tmp);
		}
		echo '{"success":true}';
	}
}