<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Boards extends CI_Controller {
	public function admin_index()
	{
		if(!empty($_GET['lang'])){
			$this->db->where('lang',$_GET['lang']);
		}
		$this->db->limit($_GET['limit'],$_GET['start']);
		$this->db->order_by('a.post_date','DESC');
		$query = $this->db->get('boards as a');
		$res = new stdClass();
		$res->root = $query->result();
		$res->total = $this->db->count_all('boards');
		echo json_encode($res);
	}
	public function update_board()
	{
		$time = date('Y-m-d H:i:s');
		$tmp = array();
		$tmp['title'] = $this->input->post('title');
		$tmp['content'] = $this->input->post('content');
		$tmp['updated_at'] = $time;
		$tmp['post_date'] = $this->input->post('post_date');
		$tmp['lang'] = $this->input->post('lang');
		if(strlen($this->input->post('id'))>0)
		{
			$this->db->where('id',$this->input->post('id'));
			$this->db->update('boards',$tmp);
		}
		else
		{
			$tmp['created_at'] = $time;
			$this->db->insert('boards',$tmp);
		}
		echo '{"success":true}';
	}
	public function dele_board(){
		$this->db->where('id',$this->input->post('id'));
		$this->db->delete('boards');
		echo '{"success":true}';
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */