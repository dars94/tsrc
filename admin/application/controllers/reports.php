<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {
	public function lists()
	{
		if(!empty($_GET['lang'])){
			$this->db->where('lang',$_GET['lang']);
		}
		$this->db->order_by('a.year DESC,a.weight DESC,a.id DESC');
		$query = $this->db->get('reports as a');
		$res = new stdClass();
		$res->root = $query->result();
		$res->total = $this->db->count_all('reports');
		echo json_encode($res);
	}
	public function save()
	{
		$time = date('Y-m-d H:i:s');
		$tmp = array(
			'year' => $this->input->post('year'),
			'name' => $this->input->post('name'),
			'lang' => $this->input->post('lang'),
			'updated_at' => $time
		);
		/* file upload */
		$config['upload_path']='public/upload/';
		$config['allowed_types']='rtf|zip|rar|ppt|docx|doc|xls|pdf|gif|jpg|jpeg|png|bmp';
		$this->load->library('upload',$config);

		if ($this->upload->do_upload('uuid')){
			$data = $this->upload->data();
			$tmp['uuid'] = $data['file_name'];
			$tmp['size'] = $data['file_size'];	
		}

		if($this->input->post('id')){
			$this->db->where('id',$this->input->post('id'));
			$this->db->update('reports',$tmp);
		}else{
			$tmp['created_at'] = $time;
			$this->db->insert('reports',$tmp);
		}
		echo '{"success":true}';
	}
	public function dele_report(){
		$this->db->where('id',$this->input->post('id'));
		$this->db->delete('reports');
		echo '{"success":true}';
	}
	public function save_sort(){
		$tmp = explode(',',$_POST['sort']);
		$i = 0;
		while($i<$_POST['length']){
			$this->db->where('id',$tmp[$i]);
			$this->db->update('reports',array('weight'=>($_POST['length']-$i)));
			$i++;
		}
		echo '{"success":true}';
	}
}