<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fileworm extends CI_Controller{
	/* 爬出所有檔案 */
	public function index(){
		$lang = 'cn';
		if($this->input->post('files')){
			$this->load->library('simple_html_dom');
			foreach($this->input->post('files') as $f){
				echo "http://www.song-yee.com.tw/TSRC/".strtoupper($lang)."/".$f.'<br>';
				$html = file_get_html("http://www.song-yee.com.tw/TSRC/".strtoupper($lang)."/".$f);
				
				if($html){
					if(@$html->find('#content')){
						$content = $html->find('#content',0)->plaintext;
					}else if(@$html->plaintext){
						$content = $html->plaintext;
					}
					$content = trim(str_replace(' ','',preg_replace('/&nbsp;/',"",$content)));
					if(strlen($content)>0){
						$tmp = array(
							'lang'=>$lang,
							'content'=>$content,
							'page_name'=>$f
						);
						$this->db->insert('pages',$tmp);
					}
				}
			}
			echo 'Done.<br>';
		}
		$path = getcwd()."/../".strtoupper($lang)."/";
		$dir = opendir($path);
		$files = array();
		while(($file = readdir($dir)) !== false){
			if(filetype($path.$file) != 'dir'){
				array_push($files,$file);
			}
		}
		closedir($dir);
		$this->load->view('worm/index',array('files'=>$files));
	}
}