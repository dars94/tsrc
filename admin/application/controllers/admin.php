<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function index()
	{
		$this->load->view('admin/index');
	}
	public function ckupload(){
		$config['upload_path']='public/upload/';
		$config['allowed_types']='rtf|zip|rar|ppt|docx|doc|xls|pdf|gif|jpg|jpeg|png|bmp';
		$config['max_size']='8000';
		$config['max_width']='0';
		$config['max_height']='0';
		$config['encrypt_name']=true;
		$config['remove_spaces']=true;
		$this->load->library('upload',$config);
		$upload = $this->upload->do_upload('upload');
		
		$data=$this->upload->data();
		header('Content-type:text/html;charset=utf8');
		echo '<script type="text/javascript">';
		$num = $this->input->get('CKEditorFuncNum');
		$url = site_url('public/upload/'.$data['file_name']);
		$msg = "File Upload Success";
		echo "window.parent.CKEDITOR.tools.callFunction($num,'$url','$msg');";
		echo '</script>';
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */