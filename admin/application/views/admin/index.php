<!DOCTYPE html>
<html>
	<head>
		<title>TSRC台灣橡膠後台管理系統</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('public/extjs/resources/css/ext-all.css')?>">
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('public/icons/sprite.css')?>">
		<link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/admin.css')?>">
		<style type="text/css">
		.cke_dialog{
			z-index:99999;
		}
		</style>
		<script>var base_url = "<?php echo site_url()?>";</script>
		<script src="<?php echo site_url('public/extjs/bootstrap.js')?>"></script>
		<script src="<?php echo site_url('public/ckeditor/ckeditor.js')?>"></script>
		<script src="<?php echo site_url('public/js/ckeditor.js')?>"></script>
		<script src="<?php echo site_url('public/js/script.js')?>"></script>
		<script src="<?php echo site_url('public/js/menu.js')?>"></script>
		<script src="<?php echo site_url('public/js/board.js')?>"></script>
		<script src="<?php echo site_url('public/js/show.js')?>"></script>
		<script src="<?php echo site_url('public/js/report.js')?>"></script>
		<script src="<?php echo site_url('public/js/app.js')?>"></script>
	</head>
	<body></body>
</html>