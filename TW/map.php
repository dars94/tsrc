<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/map.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>網站導覽</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<link href="css/map.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg')">
<div id="wrap_map">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_map">
        <div id="sitemap_bg"></div>
        <div id="sitemap-content">
          <div id="alpha" class="gray12">
            <p class="toppage"><a href="index.php">首頁</a></p>
            <ul class="sitemap">
              <li><a href="about.php">關於台橡</a>
                <ul>
                  <li><a href="about01.php">企業願景與使命</a></li>
                  <li><a href="about02.php">董事長的話</a></li>
                  <li><a href="about05.php">總經理兼執行長的話</a></li>
                  <li><a href="about03.php">公司沿革</a></li>
                  <li><a href="about04.php">主要業務</a></li>
                  <li class="end"><a href="about05_2.php">企業社會責任</a>
                    <ul>
                      <li><a href="about05_2.php">公司願景</a></li>
                      <li><a href="about05_3.php">社會參與</a></li>
                      <li><a href="about05_4.php">員工照顧</a></li>
                      <li><a href="about05_5.php">綠色產品發展</a></li>
                      <li><a href="about05_6.php">環境保護政策</a></li>
                      <li><a href="about05_7.php">安全衛生管理</a></li>
                      <li class="end"><a href="about05_8.php">環安衛績效</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="service.php">投資人服務</a>
                <ul>
                  <li><a href="service.php">致股東報告書</a></li>
                  <li><a href="service02.php">財務資訊</a>
                    <ul>
                      <li><a href="service02_1.php">每月營業額</a></li>
                      <li class="end"><a href="service02_1.php">財務報告</a></li>
                    </ul>
                  </li>
                  <li><a href="service03.php">公司年報</a></li>
                  <li><a href="service04.php">公司治理</a></li>
                  <li class="end"><a href="service05.php">股東專櫃</a>
                    <ul>
                      <li><a href="service05.php">股價查詢</a></li>
                      <li><a href="service05.php">股東會</a></li>
                      <li><a href="service05.php">歷年股利分派</a></li>
                      <li><a href="service05.php">重大訊息公告</a></li>
                      <li class="end"><a href="service05.php">聯絡人</a></li>
                    </ul>
                </ul>
              </li>
              <li><a href="product.php">產品</a>
                <ul>
                  <li><a href="product01.php">橡膠營運處</a>
                    <ul>
                      <li><a href="product01_1.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">ESBR</span></a></li>
                      <li><a href="product01_2.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">SSBR</span></a></li>
                      <li><a href="product01_3.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">BR</span></a></li>
                      <li class="end"><a href="product01_4.php"><img src="images/product/TA.png" alt="" width="45" height="14" /><span class="blue12">NBR</span></a></li>
                    </ul>
                  </li>
                  <li><a href="product02.php">應用材料營運處</a>
                    <ul>
                    <li><a href="product02_5.php">客服中心</a></li>
                      <li><a href="product02_1.php"><img src="images/product/TA.png" alt="" width="45" height="14" border="0" /></a><a href="product02_1.php"><span class="blue12">TPE</span></a></li>
                      <li><a href="product02_2.php">SBS SIS SEBS</a></li>
                      <li><a href="product02_3.php">關於TPE</a></li>
                      <li class="end"><img src="images/blend02.png" width="80" height="15" /></li>
                    </ul>
                  </li>
                  <li class="end"><a href="product03.php">共同開發橡材服務</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div id="beta" class="gray12">
            <ul class="sitemap">
              <li><a href="research.php">研究與發展</a>
                <ul>
                  <li><a href="research01.php">新產品開發</a></li>
                  <li><a href="research02.php">新製程開發</a></li>
                  <li class="end"><a href="research03.php">新應用技術與服務</a></li>
                </ul>
              </li>
              <li><a href="news.php">新聞</a>
                <ul>
                  <li><a href="news.php">最新新聞</a></li>
                  <li class="end"><a href="news02.php">展覽會與研討會</a></li>
                </ul>
              </li>
              <li><a href="member.php">集團成員</a>
                <ul>
                  <li><a href="member.php">總公司 台橡股份有限公司</a></li>
                  <li><a href="member02.php">申華化學工業有限公司</a></li>
                  <li><a href="member03.php">台橡(南通)實業有限公司</a></li>
                  <li><a href="member04.php">台橡宇部(南通)化學工業有限公司</a></li>
                  <li><a href="member05.php">朗盛台橡(南通)化學工業有限公司</a></li>
                  <li><a href="member06.php">DEXCO POLYMERS LP</a></li>
                  <li><a href="member07.php">INDIAN SYNTHETIC RUBBER LTD.</a></li>
                  <li><a href="member08.php">THAI SYNTHETIC RUBBER COMPANY LTD.</a></li>
                  <li><a href="member09.php">台橡(上海)實業有限公司</a></li>
                  <li><a href="member10.php">台橡(濟南)實業有限公司</a></li>
                  <li class="end"><a href="member11.php">南通千象倉儲有限公司</a></li>
                </ul>
              </li>
              <li class="end"><a href="#">其它</a>
                <ul>
                  <li><a href="../EN/index.php">English</a></li>
                  <li><a href="../CN/index.php">中文简体</a></li>
                  <li><a href="index.php">中文繁體</a></li>
                  <li><a href="search.php">搜尋</a></li>
                  <li><a href="map.php">網站地圖</a></li>
                  <li class="end"><a href="mailto:tsrcsales@tsrc-global.com">聯繫我們</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer_map">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
