     <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" id='btn_1' coords="3,2,168,24" />

                  <area shape="rect" id='btn_2' coords="174,2,342,25" />

                  <area shape="rect" id='btn_3' coords="348,1,513,24" />

                  <area shape="rect" id='btn_4' coords="519,3,683,25" />

                  </map></td>

              </tr>

            <tr>

              <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="50" align="left" valign="top" class="green12">1999.11</td>
                  <td width="542" align="left" class="gray12">將原台灣合成橡膠股份有限公司的名字更改為台橡股份有限公司，並重新設計企業識別標誌，以利公司未來更彈性化的發展。 </td>
                </tr>
                <tr>
                  <td align="left" class="green12">1999.04</td>
                  <td align="left" class="gray12">參與投資台灣高速鐵路計劃。 </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">1998.08</td>
                  <td align="left" class="gray12">BR工廠再進行去瓶頸工程，產能擴展到55,000公噸。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1998.02 </td>
                  <td align="left" class="gray12">高雄廠通過ISO-14001有關品質與環境的認證。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1997.01</td>
                  <td align="left" class="gray12">高雄廠通過ISO-9001有關品質的認證。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1996.04</td>
                  <td align="left" class="gray12">主導投資9千2百萬美元在大陸興建年產10萬公噸的SBR工廠計劃。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1996.02</td>
                  <td align="left" class="gray12">榮獲經濟部頒發第四屆&rdquo; 產業科技發展獎&rdquo;。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1995.10</td>
                  <td align="left" class="gray12">參與投資240萬美元在泰國興建年產5萬公噸的BR工廠計劃。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1995.05</td>
                  <td align="left" class="gray12">TPE氫化產品開發成功，並積極準備商業化。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1995.04 </td>
                  <td align="left" class="gray12">投資電力事業。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1994.09 </td>
                  <td align="left" class="gray12">BR工廠完成去瓶頸，年產能提升為51,000公噸。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1994.04</td>
                  <td align="left" class="gray12">開發TPE新產品，並擴增TPE第二生產線，使TPE產能提升到54,000公噸/年。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1994.01</td>
                  <td align="left" class="gray12">高雄廠通過ISO-9002有關品質的認證。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">1991.09 </td>
                  <td align="left" class="gray12">興建實驗工廠及汽電共生裝置。</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td align="center">&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>