<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/product.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->

<title>產品</title>

<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->

<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/product/b01_2.jpg','images/product/b01-1_2.jpg','images/product/b01-2_2.jpg','images/product/b01-3_2.jpg','images/product/b01-4_2.jpg','images/product/b02_2.jpg','images/product/b02-1_2.jpg','images/product/b02-2_2.jpg','images/product/b02-3_2.jpg','images/product/b02-4_2.jpg','images/product/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│</span><span class="blue12">　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　<span class="gray12_2"><a href="product.php">產品</a></span>　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->

      <div id="content_left">

        <table width="171" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td height="25">&nbsp;</td>

          </tr>

          <tr>

            <td><a href="product01.php"><img src="images/product/b01.jpg" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/product/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product01_1.php"><img src="images/product/b01-1.jpg" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/product/b01-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product01_2.php"><img src="images/product/b01-2.jpg" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/product/b01-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product01_3.php"><img src="images/product/b01-3.jpg" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/product/b01-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product01_4.php"><img src="images/product/b01-4.jpg" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/product/b01-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td>&nbsp;</td>

          </tr>

          <tr>

            <td><a href="product02.php"><img src="images/product/b02.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/product/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product02_1.php"><img src="images/product/b02-1.jpg" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/product/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product02_2.php"><img src="images/product/b02-2.jpg" width="171" height="22" border="0" id="Image9" onmouseover="MM_swapImage('Image9','','images/product/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product02_3.php"><img src="images/product/b02-3.jpg" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/product/b02-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="product02_4.php"><img src="images/product/b02-4.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/product/b02-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td>&nbsp;</td>

          </tr>

          <tr>

            <td><a href="product03.php"><img src="images/product/b03.jpg" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/product/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

        </table>

      </div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->

    <div id="content_top">

      <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="184"><img src="images/product/pic01.jpg" alt="" width="184" height="198" /></td>

          <td width="5">&nbsp;</td>

          <td width="310"><img src="images/product/pic02.jpg" width="310" height="197" /></td>

          <td width="5">&nbsp;</td>

          <td width="184"><img src="images/product/pic03.jpg" width="184" height="198" /></td>

        </tr>

      </table>

    </div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->

      <div id="content_main">

        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">

          <tr>

            <td height="15">&nbsp;</td>

          </tr>

          <tr>

            <td><span class="blue14-b">一部汽車運用了X種合成橡膠</span><span class="gray12">

            </span></td>

          </tr>

          <tr>

            <td class="gray12_3">全球65%的橡膠用於汽車工業，隨著汽車科技發展，對於橡膠性能的要求隨之提高，給予了合成橡膠快速發展的利基。經大量實驗證明以溶聚丁苯橡膠<span class="blue12">SSBR</span>所生產的輪胎，可以兼顧滾動阻力的降低和提高抗濕滑性，是生產高性能輪胎重要的成份；丁腈橡膠<span class="blue12">NBR</span>優異的耐油性、耐溶劑性與耐熱老化特性則是汽車引擎油管最合適的材料；彈性好、耐衝擊的<span class="blue12">SEBS</span>使用於安全氣囊蓋；熱可塑性的<span class="blue12">TPE</span>摻配料使用於車窗邊條，符合耐候性、耐低溫性要求。除了運用於汽車產業，以合成橡膠為原料生產出的鞋底、玩具、建築材料、日用品、工業用品分布在各角落，使我們生活更加豐富多彩。</td>

          </tr>
          <tr>
            <td class="gray12_3">合成橡膠屬於技術密集的產業， 台橡從成立開始就掌握創新開發的重要性，不斷研發新技術、新品種、新牌號以符合新市場需求，目前台橡產品種類多，牌號齊全，而且從上游原料到下游摻配料一應俱全，可滿足客戶生產線的各種需求。此外，台橡不斷投入與客戶共同開發專屬新橡材的技術服務，並依據客戶加工及應用端需求調整產品特性，向高附加價值產品推進。</td>
          </tr>

          <tr>

            <td class="gray12_3">全球環保意識抬頭也帶動合成橡膠的綠色發展趨勢，台橡已研發成功環保型橡材<span class="blue12">SSBR</span>，運用於輪胎不僅可降低油耗並可減少汽車廢氣排放量，預計2011年底開始量產。 台橡將繼續研發有益環保的橡材，並推動產業樹立橡膠環保新標準。</td>

          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>

          <tr>

            <td >&nbsp;</td>

          </tr>

        </table>

      </div>

      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
   <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
