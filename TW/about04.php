<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>關於台橡─主要業務</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){

	$('.item_block_about').css('display','none');

	$('.down_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideDown();

		$(this).css('display','none');

	});

	$('.up_btn').click(function(){

		var tmp_id = $(this).attr('id');

		$('#'+tmp_id.split('_')[0]+'_c').slideUp();

		$('#'+tmp_id.split('_')[0]+'_d').css('display','');

	});

})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　<a href="about.php">關於台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b04_2.jpg" name="Image4" width="171" height="22" id="Image4" /></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top_5"><img src="images/about/pic01_4_1.jpg" width="688" height="188" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main">
        <div id="main_about">
          <table width="632" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td width="130" align="left" valign="top"><table width="100" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left" valign="top"><span class="gray12">橡膠營運處</span></td>
                  </tr>
                </table></td>
              <td width="500" align="left" valign="top"><div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">ESBR</span><span class="gray12"> － 比天然膠更耐磨且易於加工 <a href="#"><img src="images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn1_d" /></a></span></div>
                  <div class="item_block_about" id="btn1_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" ><span class="gray12">乳聚苯乙烯—丁二烯橡膠(<span class="blue12">ESBR</span>)性質上比天然膠更耐磨且易於加工，其品質及產量不像天然膠會受到氣候及地理因素影響。因此在所有合成橡膠中，<span class="blue12">ESBR</span>不僅是用途最廣泛的合成橡膠，更是汽車輪胎、鞋底、輸送帶、橡膠帶、戰車及拖引車履帶、運動器材、玩具等的主要原料。台橡於台灣
                          高雄廠的</span><span class="blue12">ESBR</span><span class="gray12">年產能10萬公噸。在中國，台橡與日本丸紅(Marubeni Corporation)及南通石化總公司於江蘇省南通經濟技術開發區成立「申華化學工業有限公司」生產<span class="blue12">ESBR</span>，年產能原為10萬公噸；配合大陸市場成長，已擴產為18萬公噸。另外，台橡與印度國有石油(Indian Oil Corporation Ltd.)、日本丸紅(Marubeni Corporation)於印度合資成立 Indian Synthetic Rubber Ltd.生產 <span class="blue12">ESBR</span>，規劃年產能12萬公噸，預計2012年第四季投產，初期以滿足印度快速成長的市場需求為主。 </span><a href="#"><img src="images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn1_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="images/down03.png" width="510" height="20" /><br />
                  </div>
                </div>
                <div id="about_block">
                  <div id="item_about">
                    <table width="495" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left"><span class="gray12"><a href="#"><img src="images/product/TA.png" width="49" height="15" /></a></span><span class="blue12">SSBR</span><span class="gray12"> － 具低滾動阻力、抗濕滑性、耐磨 <a href="#"><img src="images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn2_d" /></a></span></td>
                      </tr>
                    </table>
                  </div>
                  <div class="item_block_about" id="btn2_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" ><span class="gray12">溶聚苯乙烯—丁二烯橡膠(</span><span class="blue12">SSBR</span><span class="gray12">)可根據加工及應用需求調整產品特性，突破傳統橡膠的侷限，生產客製化的產品。</span><span class="blue12">SSBR</span><span class="gray12">主要應用於節能(低滾動阻力)輪胎、高性能輪胎、及全天候輪胎，不僅可降低油耗並減少汽車廢氣排放量，符合環保節能減碳趨勢， 降低對環境之衝擊，並提高產品的附加價值。此外，</span><span class="blue12">SSBR</span><span class="gray12">也可應用於鞋底、輸送帶、橡膠帶、運動器材、玩具等。目前台橡規劃台灣高雄<span class="blue12">SSBR</span>廠年產能為3萬公噸，預計2011年第四季投產。 </span><a href="#"><img src="images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn2_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">BR</span><span class="gray12"> － 具優異彈性、耐磨耗性及耐低溫特性 <a href="#"><img src="images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn3_d" /></a></span></div>
                  <div class="item_block_about" id="btn3_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498"><span class="gray12">聚丁二烯橡膠(<span class="blue12">BR</span>)具有優異的彈性、耐磨耗性及耐低溫特性。主要用於高速輻射輪胎製造及高耐衝擊聚苯乙稀HIPS的改質。可增加輪胎使用時安全性及舒適性。用於HIPS改質時，具有良好的加工性及耐衝擊的特性。隨著HIPS在個人電腦、家庭用品及電子產業上的普遍應用，市場對<span class="blue12">BR</span>需求不斷增加，也帶動了台橡的成長。目前台橡台灣高雄<span class="blue12">BR</span>廠年產能為6萬公噸，台橡與日本宇部興產株式会社(Ube Industries, Ltd.)、日本丸紅(Marubeni Corporation)於泰國合資成立的<span class="blue12">BR</span>工廠(Thai Synthetic Rubbers Ltd.)年產能為7萬2千公噸。 </span></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">為了因應客戶的需求、提升在亞太市場的競爭力，台橡更積極佈局大中華地區，與日本宇部興產株式会社(Ube Industries, Ltd.)、日本丸紅(Marubeni Corporation)於江蘇省南通經濟技術開發區設立「台橡宇部(南通)化學工業有限公司」生產<span class="blue12">BR</span>，現有年產能為5萬公噸，並計劃於2012擴產至7萬2千公噸。</span><a href="#"><img src="images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn3_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="blue12">NBR</span><span class="gray12"> －具優異耐油性、耐溶劑性與耐熱老化性<a href="#"> <img src="images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn4_d" /></a></span></div>
                  <div class="item_block_about" id="btn4_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" ><span class="gray12">聚丙烯腈-丁二烯橡膠(<span class="blue12">NBR</span>)具有優異的耐油性、耐溶劑性與耐熱老化等特性。主要應用於油管、油封、滾輪、工業墊片、傳動皮帶、鞋材、發泡材、輸送帶…等民生與工業製品。近年來中國地區汽車產量大幅成長，因此帶動<span class="blue12">NBR</span>等合成橡膠強烈的需求。台橡位於江蘇省南通經濟技術開發區，與德國朗盛集團Lanxess合資成立「朗盛台橡(南通)化學工業有限公司」興建年產能3萬公噸的<span class="blue12">NBR</span>工廠，預計於2012年第二季投產。 </span><a href="#"><img src="images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn4_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="images/down03.png" width="510" height="20" /> </div>
                </div></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td width="130" align="left" valign="top"><table width="100" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left" valign="top"><span class="gray12">應用材料營運處</span></td>
                  </tr>
                </table></td>
              <td><div id="about_block">
                  <div id="item_about"><span class="gray12"><a href="#"><img src="images/product/TA.png" alt="" width="49" height="15" /></a></span><span class="gray12"><span class="blue12">TPE　熱可塑性橡膠SBS, SIS, SEBS</span> －高價值應用橡膠，挑戰新質地 <a href="#"><img src="images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn5_d" /></a></span></div>
                  <div class="item_block_about" id="btn5_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12">TAIPOL®<span class="blue12">TPE </span>熱可塑性橡膠是由苯乙烯與其他配合單體形成的嵌段式共聚合體(Styrenic Block Copolymer)簡稱<span class="blue12">SBC</span>。台橡本系列產品包括以丁二烯為配合單體的<span class="blue12">SBS</span>和以異戊二烯為配合的<span class="blue12">SIS</span>系列，以及<span class="blue12">SBS</span>經過氫化後，生成具有更優異耐候性及功能之第二代產品<span class="blue12">SEBS</span>。 </td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">由於熱可塑性橡膠<span class="blue12">TPE</span>兼具備塑膠與橡膠特性，被大量應用於高級鞋底、柏油改質、塑膠改質、黏著劑等。它可以直接射出加工，並可回收廢料降低成本，製成之鞋底不易打滑，是代替PVC及傳統硫化橡膠的重要材料。<span class="blue12">TPE</span>更具有熱可塑性能、黏度低、容易互摻混等優點，作為建築和道路柏油、防水材料的改質劑，可改善柏油冬脆夏軟的缺點，增進負載性能。將少量<span class="blue12">TPE</span>與聚丙烯、聚乙烯、聚苯乙烯等聚合物共混，可明顯提高塑料製品的抗衝擊強度，改善耐低溫性能，可應用於醫療器材、汽車零配件、運動器材、玩具及工業用產品。<span class="blue12">TPE</span>與增黏樹脂相容性優異，經熔融共混後可製成具備高黏著強度、符合環保需求之黏著劑，廣泛應用於自黏感壓膠帶、標籤與紙尿片等衛生用品。</span></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">台橡因同時具備<span class="blue12">SBS</span>、<span class="blue12">SIS</span>、<span class="blue12">SEBS</span>三大系列產品，可同時滿足不同應用範圍的客戶需求，在以客製化配方的應用產業上，具有獨特的競爭優勢。</span><a href="#"><img src="images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn5_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="images/down03.png" width="510" height="20" /> </div>
                </div>
                <div id="about_block">
                  <div id="item_about"><img src="images/about/blend.png" alt="" width="88" height="17" /><span class="blue12">TPE　應用材料</span><span class="gray12"> －舒適生活的一部份 <a href="#"><img src="images/down.png" alt="" width="17" height="14" border="0" class="down_btn" id="btn6_d" /></a></span></div>
                  <div class="item_block_about" id="btn6_c">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td width="498" class="gray12">熱可塑性複合材料<span class="blue12">TPR</span>因符合環保要求得以快速成長，本公司運用相關技術推出<span class="blue12">活粒T-BLEND&reg;</span>產品，本產品是以<span class="blue12">SBS</span>或<span class="blue12">SEBS</span>為主的摻配應用材料，具備良好的觸感，可調整的物性和硬度，優良的耐候性、耐低溫性及熱可塑性。<span class="blue12">活粒T-BLEND®</span>產品可依客戶需求設計配方產製供應，產品邊料可回收再製，降低生產成本；能直接注塑或擠壓成型，節省工時。這種高功能的環保產品，提供塑模產業更大量寬廣的應用，開發更多領域產品，如生活用品、超軟質凝膠，無鹵耐燃低電壓電線產品，<span class="blue12">SEBS</span>發泡材料、門窗及冰箱密封條、彈性薄膜等。 </td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="blue12">活粒T-BLEND®</span><span class="gray12">，</span><span class="blue12">TPE</span><span class="gray12">應用材料在台灣高雄、上海松江、山東濟南設有生產基地，年產量達27,000公噸，並在台灣、華東、華北、華南四處設有行銷據點，並將不斷拓展其他亞太地區市場，便於推廣新產品及進行各種新材料的研發。 </span></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><span class="blue12">●</span></td>
                        <td><span class="gray12">另外為建立與客戶之間互相溝通的橋樑，台橡特別斥資在高雄岡山建造「</span><span class="blue12">TPE</span><span class="gray12">應用研究中心」並在上海成立「客戶服務中心」。除了引進精密先進的實驗設備外，菁英研發團隊並密切與國外學術界及專業研究機構技術合作，主要為擴大</span><span class="blue12">SEBS</span><span class="gray12">及其摻配產品之應用領域，積極發展高附加價值的新材料和新技術，進而提供應用材料之諮詢建議、設計研發等全系列的服務。</span><a href="#"><img src="images/up.png" alt="" name="btn02" width="17" height="14" border="0" id="btn6_u" class="up_btn" /></a></td>
                      </tr>
                    </table>
                    <img src="images/down03.png" width="510" height="20" /> </div>
                </div></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
