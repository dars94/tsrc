<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->

<title>關於台橡─企業社會責任</title>

<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.tooltip.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script src="Scripts/jquery.tooltip.js"></script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
map_img = new Image();
map_img.src = "images/about/ball.png";
map1_img = new Image();
map1_img.src = "images/about/ball_1.png"
map2_img = new Image();
map2_img.src = "images/about/ball_2.png"
map3_img = new Image();
map3_img.src = "images/about/ball_3.png"
map4_img = new Image();
map4_img.src = "images/about/ball_4.png"
map5_img = new Image();
map5_img.src = "images/about/ball_5.png"
map6_img = new Image();
map6_img.src = "images/about/ball_6.png"
map7_img = new Image();
map7_img.src = "images/about/ball_7.png"
$(function(){
	$('#map1_block').css('display','none');
	$('#map2_block').css('display','none');
	$('#map3_block').css('display','none');
	$('#map4_block').css('display','none');
	$('#map5_block').css('display','none');
	$('#map6_block').css('display','none');
	$('#map7_block').css('display','none');
	$('.close_btn').live('click',function(){$('#tooltip').css('display','none')});
	$('map *').tooltip({
		delay:0,
		bodyHandler:function(){
			return $('#'+$(this).attr('id')+'_block').html();
		},
		showURL:false
	});
	$('#map1').hover(function(){
		$('#ball_img').attr('src',map1_img.src);
	},function(){
		$('#ball_img').attr('src',map_img.src);
	});
	$('#map2').hover(function(){
		$('#ball_img').attr('src',map2_img.src);
	},function(){
		$('#ball_img').attr('src',map_img.src);
	});
	$('#map3').hover(function(){
		$('#ball_img').attr('src',map3_img.src);
	},function(){
		$('#ball_img').attr('src',map_img.src);
	});
	$('#map4').hover(function(){
		$('#ball_img').attr('src',map4_img.src);
	},function(){
		$('#ball_img').attr('src',map_img.src);
	});
	$('#map5').hover(function(){
		$('#ball_img').attr('src',map5_img.src);
	},function(){
		$('#ball_img').attr('src',map_img.src);
	});
	$('#map6').hover(function(){
		$('#ball_img').attr('src',map6_img.src);
	},function(){
		$('#ball_img').attr('src',map_img.src);
	});
	$('#map7').hover(function(){
		$('#ball_img').attr('src',map7_img.src);
	},function(){
		$('#ball_img').attr('src',map_img.src);
	});
})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　<a href="about.php">關於台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->

      <div id="content_left">

        <table width="171" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td height="25">&nbsp;</td>

          </tr>

          <tr>

            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>
 <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>

            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about05_2.php"><img src="images/about/b05_3.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

         

          <tr>

            <td><a href="about05_2.php"><img src="images/about/b05-2.jpg" alt="" name="Image8" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/about/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about05_3.php"><img src="images/about/b05-3.jpg" alt="" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/about/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about05_4.php"><img src="images/about/b05-4.jpg" alt="" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/about/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about05_5.php"><img src="images/about/b05-5.jpg" alt="" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/about/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><img src="images/about/b05-6_2.jpg" alt="" width="171" height="22" id="Image12" /></td>

          </tr>

          <tr>

            <td><a href="about05_7.php"><img src="images/about/b05-7.jpg" alt="" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/about/b05-7_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about05_8.php"><img src="images/about/b05-8.jpg" alt="" width="171" height="22" id="Image14" onmouseover="MM_swapImage('Image14','','images/about/b05-8_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

        </table>

      </div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->

    <div id="content_main_3">

      <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">

        <tr>

          <td>&nbsp;</td>

        </tr>

        <tr>

          <td class="blue14-b">務實力行，善盡環保責任</td>

        </tr>

        <tr>

          <td class="gray12_3">為善盡環保護責任及維護利害相關者之權益，台橡公司導入ISO-14001環境管理系統，並於1998年通過驗證。全員依
循環境政策：「珍惜地球資源，善盡環保責任；持續不斷改善，企業永續經營。」積極配合系統之運作，展現兌現承諾與持續改善之決心。</td>

        </tr>

        <tr>

          <td align="center" >
			<img src="images/about/ball.png" name="ball_img" width="456" height="376" border="0" usemap="#Map" id="ball_img" />
            <map name="Map" id="Map">
              <area shape="rect" coords="286,82,383,102" href="javascript:void(0)" id='map2' />
              <area shape="rect" coords="346,184,468,207" href="javascript:void(0)" id='map3' />
              <area shape="rect" coords="238,335,327,353" href="javascript:void(0)" id='map4' />
              <area shape="rect" coords="60,232,185,254" href="javascript:void(0)" id='map5' />
              <area shape="rect" coords="95,179,166,201" href="javascript:void(0)" id='map6' />
              <area shape="rect" coords="3,77,150,100" href="javascript:void(0)" id='map7' />
              <area shape="rect" coords="284,40,351,62" href="javascript:void(0)" id='map1' />
            </map>
		  </td>
        </tr>

        <tr>

          <td >&nbsp;</td>

        </tr>

      </table>

    </div>

    <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
<div id='map1_block'><?php include "about05_6_1.php"?></div>
<div id='map2_block'><?php include "about05_6_2.php"?></div>
<div id='map3_block'><?php include "about05_6_3.php"?></div>
<div id='map4_block'><?php include "about05_6_4.php"?></div>
<div id='map5_block'><?php include "about05_6_5.php"?></div>
<div id='map6_block'><?php include "about05_6_6.php"?></div>
<div id='map7_block'><?php include "about05_6_7.php"?></div>
</body>
<!-- InstanceEnd --></html>
