<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>關於台橡─企業社會責任</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　<a href="about.php">關於台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05_3.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05-2.jpg" alt="" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/about/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_3.php"><img src="images/about/b05-3.jpg" alt="" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/about/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_4.php"><img src="images/about/b05-4.jpg" alt="" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/about/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_5.php"><img src="images/about/b05-5.jpg" alt="" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/about/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_6.php"><img src="images/about/b05-6.jpg" alt="" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/about/b05-6_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_7.php"><img src="images/about/b05-7.jpg" alt="" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/about/b05-7_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b05-8_2.jpg" alt="" width="171" height="22" id="Image14" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top"><img src="images/about/pic01_5_8.jpg" width="688" height="197" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">落實制度，環安衛績效豐碩</td>
          </tr>
          <tr>
            <td align="left" valign="top" class="gray12"><table width="632" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="25" align="center" valign="top"><img src="images/about/mark.png" width="21" height="21" /></td>
                  <td width="581" align="left" valign="top">榮獲台灣省政府環境保護處85年度毒性化學物質災害無預警測試績優單位。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">配合經濟部舉辦「87年度工業安全衛生技術輔導」協助推廣工業安全技術，將改善成果與經驗提供同業觀摩，協助推廣工業安全技術。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">榮獲98年度大社工業區安全與環境監測業務推動績優獎。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">執行99年度防火管理工作榮獲評定為績優場所。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">榮獲高雄市「99年度績優防火管理場所暨防火管理人」。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">熱心參與地方公益活動，榮獲大社鄉保社村村長及大社鄉民意代表致贈獎座。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">協助義守大學化學工程學系辦理「企業參訪」之教學課程活動。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">積極推動大社工業區區域聯防，協助區域聯防組織內工廠落實安全衛生工作，並強化緊急應變通報及相互支援機制，提升區域安全。</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><img src="images/about/mark.png" alt="" width="21" height="21" /></td>
                  <td align="left" valign="top">協助行政院勞委會南區勞動檢查所辦理「98年度製造業屋頂墜落預防觀摩會」。</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
