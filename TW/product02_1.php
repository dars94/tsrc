<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/product.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>產品─應用材料營運處</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/product/b01_2.jpg','images/product/b01-1_2.jpg','images/product/b01-2_2.jpg','images/product/b01-3_2.jpg','images/product/b01-4_2.jpg','images/product/b02_2.jpg','images/product/b02-1_2.jpg','images/product/b02-2_2.jpg','images/product/b02-3_2.jpg','images/product/b02-4_2.jpg','images/product/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│</span><span class="blue12">　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　<span class="gray12_2"><a href="product.php">產品</a></span>　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product01.php"><img src="images/product/b01.jpg" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/product/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_1.php"><img src="images/product/b01-1.jpg" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/product/b01-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_2.php"><img src="images/product/b01-2.jpg" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/product/b01-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_3.php"><img src="images/product/b01-3.jpg" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/product/b01-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product01_4.php"><img src="images/product/b01-4.jpg" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/product/b01-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product02.php"><img src="images/product/b02_3.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/product/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_1.php"><img src="images/product/b02-1_2.jpg" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/product/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_2.php"><img src="images/product/b02-2.jpg" width="171" height="22" border="0" id="Image9" onmouseover="MM_swapImage('Image9','','images/product/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_3.php"><img src="images/product/b02-3.jpg" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/product/b02-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="product02_4.php"><img src="images/product/b02-4.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/product/b02-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="product03.php"><img src="images/product/b03.jpg" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/product/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top_5"><img src="images/product/pic02_2_1.jpg" width="688" height="188" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_3">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="67%"><span class="blue14-b"><img src="images/product/TA.png" alt="" width="49" height="15" />TPE</span><span class="green14_3_b">　高價值應用橡膠，挑戰新質地</span></td>
            <td width="33%" align="right"><a href="pdf/tpe.pdf" target="_blank"><img src="images/down.png" width="17" height="14" /></a><span class="green12_3"><a href="pdf/tpe.pdf" target="_blank"> 下載產品說明書(<img src="images/pdf.png" width="20" height="20" />，1.8MB)</a></span></td>
          </tr>
          <tr>
            <td colspan="2">　　<img src="images/product/TA.png" width="49" height="15" /><span class="blue12">TPE</span><span class="gray12">熱可塑性橡膠是由苯乙烯與其他配合單體形成的嵌段式共聚合體(Styrenic Block Copolymer)簡稱</span><span class="blue12">SBC</span><span class="gray12">。台橡本系列產品包括以丁二烯為配合單體的</span><span class="blue12">SBS</span><span class="gray12">和以異戊二烯為配合的</span><span class="blue12">SIS</span><span class="gray12">系列，以及</span><span class="blue12">SBS</span><span class="gray12">經過氫化後，生成具有更優異耐候性及功能之第二代產品</span><span class="blue12">SEBS</span><span class="gray12">。 </span><span class="gray12"> </span></td>
          </tr>
          <tr>
            <td colspan="2" class="gray12_3">由於熱可塑性橡膠<span class="blue12">TPE</span>兼具備塑膠與橡膠特性，被大量應用於高級鞋底、柏油改質、塑膠改質、黏著劑等。它可以直接射出加工，並可回收廢料降低成本，製成之鞋底不易打滑，是代替PVC及傳統硫化<span class="blue12">TPE</span>更具有熱可塑性能、黏度低、容易互摻混等優點，作為建築和道路柏油、防水材料的改質劑，可改善柏油冬脆夏軟的缺點，增進負載性能。將少量<span class="blue12">TPE</span>與聚丙烯、聚乙烯、聚苯乙烯等聚合物共混，可明顯提高塑料製品的抗衝擊強度，改善耐低溫性能，可應用於醫療器材、汽車零配件、運動器材、玩具及工業用產品。<span class="blue12">TPE</span>與增黏樹脂相容性優異，經熔融共混後可製成具備高黏著強度、符合環保需求之黏著劑，廣泛應用於自黏感壓膠帶、標籤與紙尿片等衛生用品。 </td>
          </tr>
          <tr>
            <td colspan="2" class="gray12_3">台橡因同時具備<span class="blue12">SBS</span>、<span class="blue12">SIS</span>、<span class="blue12">SEBS</span>三大系列產品，可同時滿足不同應用範圍的客戶需求，在以客製化配方的應用產業上，具有獨特的競爭優勢。</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
