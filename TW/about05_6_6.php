<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
<link href="css/text.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="385" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ball">
      <tr>
        <td align="center"><table width="95%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><a href="#"><img class='close_btn' src="images/about/close.jpg" width="25" height="25" /></a></td>
            </tr>
          <tr>
            <td align="left"><span class="green12_3">相關作業場所均依法定期實施作業環境噪音測定，並對廠房周界實施低頻噪音測定。近年來各項噪音測定結果都符合法規管制標準，但仍積極實施</span><span class="orange12">噪音防制</span><span class="green12_3">設備之裝置、低噪音設備之採購及噪音控制技術之研究。</span></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
