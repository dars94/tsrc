<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/research.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>新製程開發</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/research/b01_2.jpg','images/research/b02_2.jpg','images/research/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　<a href="research.php">研究與發展</a>　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="research01.php"><img src="images/research/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/research/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/research/b02_2.jpg" name="Image2" width="171" height="22" id="Image2" /></td>
          </tr>
          <tr>
            <td><a href="research03.php"><img src="images/research/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/research/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="top" -->
      <div id="content_top">
        <table width="688" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="184"><img src="images/research/pic01_2.jpg" alt="" width="184" height="198" /></td>
            <td width="5">&nbsp;</td>
            <td width="310"><img src="images/research/pic02_2.jpg" width="310" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="184"><img src="images/research/pic03-2.jpg" width="184" height="198" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable -->
      <div id="content_main"><!-- InstanceBeginEditable name="main" -->
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="7">&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">節能減廢，綠色製程</td>
          </tr>
          <tr>
            <td class="gray12_3">隨著台橡收購美國Dexco Polymers公司後，台橡整合了Dexco發展20餘年的先進製程技術：例如具有優良物理特性的高純度SBC產品；以及可生產四嵌段以上的不對稱嵌段聚合物，提供客戶更多先進的高性能新產品。</td>
          </tr>
          <tr>
            <td class="gray12_3">開發節能減廢一直是台橡研發團隊的工作重點。台橡積極投入綠色製程技術開發，致力於廢棄物回收使用與節省能源，開發對環境影響最小的製程， BR工場廢水回收的熱水循環製程，將SBR、BR、TPE等成品乾燥廢氣採焚化處理、各廠區之暴雨截流及雨、污水分流分治以保護河川於製程中不受污染，並設置地下水監測井及土壤採樣檢測防治污染，回收溶劑、廢氣等多項節能減廢製程，善盡地球公民環保責任。</td>
          </tr>
          <tr>
            <td align="center"><table width="93%" border="0" cellpadding="0" cellspacing="0" class="gray12">
                <tr>
                  <td width="12%" align="left"><span class="blue12">01.</span> SBR冷凝水回收再利用於製程 </td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">02.</span> BR冷凝水回收再利用於製程</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">03.</span> TPE冷凝水回收再利用於製程</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">04.</span> BR廢熱回收及熱水循環工程</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">05.</span> BR廢水回收使用於冷卻水塔</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">06.</span> SBR成品乾燥廢氣採吸附處理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">07.</span> BR成品乾燥廢氣採吸附處理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">08. </span>TPE成品乾燥及粉塵採焚化處理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">09. </span>溶劑桶槽外排氣冷凝回收處理</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">10.</span> 暴雨截流緩衝槽</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">11.</span> 暴雨截流雨污水分流分治</td>
                </tr>
                <tr>
                  <td align="left"><span class="blue12">12. </span>地下水採樣監測井</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
