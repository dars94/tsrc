     <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0" usemap="#Map" />

                <map name="Map" id="Map">

                  <area shape="rect" coords="3,2,168,24" id='btn_1' />

                  <area shape="rect" id='btn_2' coords="174,2,342,25" />

                  <area shape="rect" id='btn_3' coords="348,1,513,24" />

                  <area shape="rect" id='btn_4' coords="519,3,683,25" />

                  </map></td>

              </tr>

            <tr>

              <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td height="15" colspan="2" class="green12">&nbsp;</td>
                </tr>
                <tr>
                  <td width="50" align="left" class="green12">2011.11</td>
                  <td width="542" align="left" class="gray12">與台灣中油公司及富邦金控創投完成合資談判，成立台耀石化材料科技(股)公司。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2011.04</td>
                  <td width="542" align="left" class="gray12">併購美國Dexco Polymers 100% 股權 。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2010.05</td>
                  <td align="left" class="gray12">在中國與德國朗盛合資成立朗盛台橡(南通)化學工業有限公司，籌建年產能3萬公噸之NBR廠。</td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="green12">2010.04</td>
                  <td align="left" class="gray12">在印度與印度國有石油有限公司(Indian Oil Corporation Ltd.)、日本丸紅株式會社(Marubeni Corporation)，合資籌建年產能12萬公噸之ESBR廠。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2009.12</td>
                  <td align="left" class="gray12">獲頒OHSAS 18001 &amp; TOSHMS職業安全衛生管理系統認證。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2009.05 </td>
                  <td align="left" class="gray12">與俄羅斯RUSTEP LLC/ OJSC SIBUR HOLDING簽訂技術授權合同。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2009.05</td>
                  <td align="left" class="gray12">台橡宇部(南通)化學工業有限公司正式量產。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2008.07</td>
                  <td align="left" class="gray12">轉投資新加坡Polybus Corporation Pte. Ltd.正式營運。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2008.07</td>
                  <td align="left" class="gray12">台橡(南通)實業有限公司正式量產。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2007.11</td>
                  <td align="left" class="gray12">結束德國Atlantic Polymers GmbH轉投資公司。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2007.09 </td>
                  <td align="left" class="gray12">台橡(濟南)實業有限公司正式量產。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2006.12</td>
                  <td align="left" class="gray12">在中國南通成立台橡宇部(南通)化學工業有限公司，籌建年產能5萬公噸之BR廠。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2006.09</td>
                  <td align="left" class="gray12">在中國濟南成立台橡(濟南)實業有限公司，籌建年產能5仟公噸之摻配料廠。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2006.09</td>
                  <td align="left" class="gray12">在中國南通成立台橡(南通)實業有限公司，籌建年產能2萬公噸之SEBS廠。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2005.12</td>
                  <td align="left" class="gray12">岡山廠TPE應用研究中心籌建完成，正式營運。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2005.06 </td>
                  <td align="left" class="gray12">轉投資中國申華化學工業有限公司，完成擴建工程，年產能提升至18萬公噸。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2005.04</td>
                  <td align="left" class="gray12">應用材料事業處岡山廠正式投產。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.10</td>
                  <td align="left" class="gray12">台北新辦公室落成開始正式營運。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.08 </td>
                  <td align="left" class="gray12">應用材料事業處岡山廠開始籌建。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.07 </td>
                  <td align="left" class="gray12">應用材料事業處成立TPE應用研究中心。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2004.04 </td>
                  <td align="left" class="gray12">轉投資之中國南通千象倉儲有限公司正式營運。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2003.10</td>
                  <td align="left" class="gray12">轉投資中國申華化學工業有限公司，完成去瓶頸工程，年產能提升至12萬公噸。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2002.02 </td>
                  <td align="left" class="gray12">TPE工廠新產品SEBS試車成功，開啟橡膠產品多元化應用之新契機。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2001.08 </td>
                  <td align="left" class="gray12">轉投資之中國台橡(上海)公司正式開業，強化大中華市場佈局。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2001.03</td>
                  <td align="left" class="gray12">成立橡膠事業、應用材料事業和轉投資事業責任中心制度。</td>
                </tr>
                <tr>
                  <td align="left" class="green12">2000.08 </td>
                  <td align="left" class="gray12">與德國當地專業人士合資設立Atlantic Polymers GmbH為本公司產品拓銷至歐洲市場。</td>
                </tr>
              </table></td>

              </tr>

            <tr>

              <td align="center">&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>