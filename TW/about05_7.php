<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>關於台橡─企業社會責任</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script><script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
tmp = new Array();

tmp2 = new Array();

i=1;

while(i<=9){

	tmp[i] = new Image;

	tmp[i].src = 'images/about/b05-7-'+i+'.png';

	tmp2[i] = new Image;

	tmp2[i].src = 'images/about/b05-7-'+i+'_2.png';

	//console.log(tmp[i].src);

	i++;

}

$(function(){

	$('#img_btns img')

		.hover(function(){

			$(this).attr('src',tmp2[$(this).attr('id').split('-')[2]].src);

		},function(){

			if($(this).attr('class') != 'current'){

				$(this).attr('src',tmp[$(this).attr('id').split('-')[2]].src);

			}

		})

		.click(function(){

			$('#img_btns img').removeClass('current');

			i=1;

			while(i<=9){

				$('#b05-7-'+i).attr('src',tmp[i].src);

				i++;

			}

			$(this).addClass('current');

			$(this).attr('src',tmp2[$(this).attr('id').split('-')[2]].src);

			$.ajax({

				url:'about05_7_'+$(this).attr('id').split('-')[2]+'.php',

				success:function(data){$('#data_info').html(data);}

			});

		});

})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　<a href="about.php">關於台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05_3.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05-2.jpg" alt="" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/about/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_3.php"><img src="images/about/b05-3.jpg" alt="" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/about/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_4.php"><img src="images/about/b05-4.jpg" alt="" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/about/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_5.php"><img src="images/about/b05-5.jpg" alt="" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/about/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_6.php"><img src="images/about/b05-6.jpg" alt="" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/about/b05-6_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/about/b05-7_2.jpg" alt="" width="171" height="22" id="Image13" /></td>
          </tr>
          <tr>
            <td><a href="about05_8.php"><img src="images/about/b05-8.jpg" alt="" width="171" height="22" id="Image14" onmouseover="MM_swapImage('Image14','','images/about/b05-8_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->
      <div id="content_top_6"><img src="images/about/pic01_5_7.jpg" width="688" height="120" /></div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main">
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">安全第一，嚴格執行作業管理制度</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td class="gray12"><table width="632" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="140" align="left" valign="top"><table id='img_btns' width="130" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="130"><a href="javascript:void(0)"><img class="current" src="images/about/b05-7-1_2.png" name="Image15" width="118" height="13" id="b05-7-1" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-2.png" alt="" width="106" height="12" id="b05-7-2" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-3.png" alt="" width="81" height="12" id="b05-7-3" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-4.png" alt="" width="68" height="13" id="b05-7-4" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-5.png" alt="" width="68" height="12" id="b05-7-5" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-6.png" alt="" width="81" height="12" id="b05-7-6" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-7.png" alt="" width="93" height="13" id="b05-7-7" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-8.png" alt="" width="68" height="13" id="b05-7-8" /></a></td>
                      </tr>
                      <tr>
                        <td><a href="javascript:void(0)"><img src="images/about/b05-7-9.png" alt="" width="105" height="13" id="b05-7-9" /></a></td>
                      </tr>
                    </table></td>
                  <td width="25" align="center" valign="top"><img src="images/line_straight02.png" alt="" width="25" height="200" /></td>
                  <td width="477" align="left" valign="top"><table width="477" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="26" align="left" valign="top"><img src="images/about/arrow.png" alt="" width="26" height="21" /></td>
                        <td class="gray12_3" id="data_info">為保障員工及利害相關者之職業安全衛生，於2009年通過OHSAS 18001/TOSHMS管理系統驗證。在「以人為本」的核心價值下，透過所發展的管理策略與技術，全員參與並充分溝通，共同追求零災害及零傷害目標，此外，也透過專業輔導單位，提昇整體安全衛生文化與意識。</td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
