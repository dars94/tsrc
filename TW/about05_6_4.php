<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
<link href="css/text.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="385" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ball">
      <tr>
        <td align="center"><table width="95%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><a href="#"><img class='close_btn' src="images/about/close.jpg" width="25" height="25" /></a></td>
            </tr>
          <tr>
            <td align="left"><span class="green12_3">針對廠內之</span><span class="orange12">空氣污染防制</span><span class="green12_3">設備、鍋爐煙囪排氣自動監測系統..等，皆依法取得設置暨操作許可證，並定期安排煙道排氣檢測作業。針對檢測結果持續監控與分析追蹤，確保廠內排放氣體皆能符合法規規定。</span></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
