<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->

<title>關於台橡─公司沿革</title>

<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){
	$('#btn_1').live('click',function(){
		$.ajax({
			url:'about03_1.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
	$('#btn_2').live('click',function(){
		$.ajax({
			url:'about03_2.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
	$('#btn_3').live('click',function(){
		$.ajax({
			url:'about03_3.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
	$('#btn_4').live('click',function(){
		$.ajax({
			url:'about03_4.php',
			success:function(data){$('#tabel_year').html(data)}
		});
	});
})
</script>
<!-- InstanceBeginEditable name="head" -->

<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　<a href="about.php">關於台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->

      <div id="content_left">

        <table width="171" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td height="25">&nbsp;</td>

          </tr>

          <tr>

            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>
 <tr>
            <td><a href="about05.php"><img src="images/about/b05-1.jpg" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>

            <td><img src="images/about/b03_2.jpg" name="Image3" width="171" height="22" id="Image3" /></td>

          </tr>

          <tr>

            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

        </table>

      </div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->

    <div id="content_top_6"><img src="images/about/pic01_3_1.jpg" width="688" height="120" /></div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->

    <div id="content_main">

	<!-- table year -->

      <div id="tabel_year">

      <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td><table width="688" border="0" cellspacing="0" cellpadding="0">

            <tr>

              <td bgcolor="#FFFFFF"><img src="images/about/btn_year.jpg" alt="" width="688" height="30" border="0"usemap="#Map" /></a>
                <map name="Map" id="Map">
                  <area shape="rect" coords="3,3,168,25" href="javascript:void(0)" id='btn_1' />
                  <area shape="rect" coords="174,2,342,25" href="javascript:void(0)" id='btn_2' />
                  <area shape="rect" coords="348,1,513,24" href="javascript:void(0)" id='btn_3' />
                  <area shape="rect" coords="519,3,683,25" href="javascript:void(0)" id='btn_4' />
                  </map></td>

              </tr>

            <tr>

              <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="2">

                <tr>

                  <td height="15" colspan="2" class="green12">&nbsp;</td>

                  </tr>

                <tr>

                  <td width="50" align="left" class="green12">2011.11</td>

                  <td width="542" align="left" class="gray12">與台灣中油公司及富邦金控創投完成合資談判，成立台耀石化材料科技(股)公司。</td>

                  </tr>
                <tr>
                  <td align="left" class="green12">2011.04</td>
                  <td width="542" align="left" class="gray12">併購美國Dexco Polymers 100% 股權 。</td>
                </tr>

                <tr>

                  <td align="left" class="green12">2010.05</td>

                  <td align="left" class="gray12"> 在中國與德國朗盛合資成立朗盛台橡(南通)化學工業有限公司，籌建年產能3萬公噸之NBR廠。</td>

                  </tr>

                <tr>

                  <td align="left" valign="top" class="green12">2010.04</td>

                  <td align="left" class="gray12">在印度與印度國有石油有限公司(Indian Oil Corporation Ltd.)、日本丸紅株式會社(Marubeni Corporation)，合資籌建年產能12萬公噸之ESBR廠。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2009.12</td>

                  <td align="left" class="gray12">獲頒OHSAS 18001 &amp; TOSHMS職業安全衛生管理系統認證。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2009.05 </td>

                  <td align="left" class="gray12">與俄羅斯RUSTEP LLC/ OJSC SIBUR HOLDING簽訂技術授權合同。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2009.05</td>

                  <td align="left" class="gray12">台橡宇部(南通)化學工業有限公司正式量產。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2008.07</td>

                  <td align="left" class="gray12">轉投資新加坡Polybus Corporation Pte. Ltd.正式營運。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2008.07</td>

                  <td align="left" class="gray12">台橡(南通)實業有限公司正式量產。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2007.11</td>

                  <td align="left" class="gray12">結束德國Atlantic Polymers GmbH轉投資公司。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2007.09 </td>

                  <td align="left" class="gray12">台橡(濟南)實業有限公司正式量產。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2006.12</td>

                  <td align="left" class="gray12">在中國南通成立台橡宇部(南通)化學工業有限公司，籌建年產能5萬公噸之BR廠。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2006.09</td>

                  <td align="left" class="gray12">在中國濟南成立台橡(濟南)實業有限公司，籌建年產能5仟公噸之摻配料廠。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2006.09</td>

                  <td align="left" class="gray12">在中國南通成立台橡(南通)實業有限公司，籌建年產能2萬公噸之SEBS廠。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2005.12</td>

                  <td align="left" class="gray12">岡山廠TPE應用研究中心籌建完成，正式營運。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2005.06 </td>

                  <td align="left" class="gray12">轉投資中國申華化學工業有限公司，完成擴建工程，年產能提升至18萬公噸。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2005.04</td>

                  <td align="left" class="gray12">應用材料事業處岡山廠正式投產。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2004.10</td>

                  <td align="left" class="gray12">台北新辦公室落成開始正式營運。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2004.08 </td>

                  <td align="left" class="gray12">應用材料事業處岡山廠開始籌建。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2004.07 </td>

                  <td align="left" class="gray12">應用材料事業處成立TPE應用研究中心。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2004.04 </td>

                  <td align="left" class="gray12">轉投資之中國南通千象倉儲有限公司正式營運。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2003.10</td>

                  <td align="left" class="gray12">轉投資中國申華化學工業有限公司，完成去瓶頸工程，年產能提升至12萬公噸。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2002.02 </td>

                  <td align="left" class="gray12">TPE工廠新產品SEBS試車成功，開啟橡膠產品多元化應用之新契機。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2001.08 </td>

                  <td align="left" class="gray12">轉投資之中國台橡(上海)公司正式開業，強化大中華市場佈局。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2001.03</td>

                  <td align="left" class="gray12">成立橡膠事業、應用材料事業和轉投資事業責任中心制度。</td>

                  </tr>

                <tr>

                  <td align="left" class="green12">2000.08 </td>

                  <td align="left" class="gray12">與德國當地專業人士合資設立Atlantic Polymers GmbH為本公司產品拓銷至歐洲市場。</td>

                  </tr>

                </table></td>

              </tr>

            <tr>

              <td>&nbsp;</td>

            </tr>

            </table></td>

        </tr>

        </table>

      </div>
    </div>

    <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
