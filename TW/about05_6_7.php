<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
<link href="css/text.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="385" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ball">
        <tr>
          <td align="center"><table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right"><a href="#"><img class='close_btn' src="images/about/close.jpg" width="25" height="25" /></a></td>
              </tr>
              <tr>
                <td align="left"><span class="orange12">溫室氣體</span><span class="green12_3">排放管理是全球性的議題，也是石化產業所需面臨的挑戰。台橡公司本著「珍惜地球資源，善盡環保責任」的信念，自1973年成立以來，即持續不斷朝向節約能源、能資源與原物料回收及製程效率提昇方面做努力。 對於主管機關的溫室氣體減量推動，台橡公司配合措施如下：</span></td>
              </tr>
              <tr>
                <td align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="6%" align="left" valign="top"><span class="green12_3">(1)</span></td>
                    <td width="94%" align="left" valign="top"><span class="green12_3">經濟部委託之台灣綠色生產力基金會，查證台橡公司自2004~2008年『自願性節約能源與溫室氣體減量』，共減少37,520公噸CO2當量之排放。</span></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><span class="green12_3">(2)</span></td>
                    <td align="left" valign="top"><span class="green12_3">為達成溫室氣體自願減量的目標，已於2010年與經濟部簽署減量自願協議書，預定使2011~2015年溫室氣體之排放再減少4,022公噸CO2當量。</span></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><span class="green12_3">(3)</span></td>
                    <td align="left" valign="top"><span class="green12_3">計畫於2011年完成高雄廠組織型溫室氣體盤查(ISO 14064-1)。</span></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
