<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/service.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->

<title>投資人服務─致股東報告書</title>

<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->

<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/service/b01_2.jpg','images/service/b02_2.jpg','images/service/b02-1_2.jpg','images/service/b04_2.jpg','images/service/b05-1_2.jpg','images/service/b05-2_2.jpg','images/service/b05-4_2.jpg','images/service/b05-5_2.jpg','images/service/b03_2.jpg','images/service/b05_2.jpg','images/service/b05-3_2.jpg','images/service/b02-2_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│　<a href="service.php">投資人服務</a>　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->

      <div id="content_left">

        <table width="171" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td height="25">&nbsp;</td>

          </tr>

          <tr>

            <td><img src="images/service/b01_2.jpg" name="Image1" width="171" height="22" id="Image1" /></td>

          </tr>

          <tr>

            <td><a href="service02.php"><img src="images/service/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/service/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service02_1.php"><img src="images/service/b02-1.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/service/b02-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service02_1.php"><img src="images/service/b02-2.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/service/b02-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service03.php"><img src="images/service/b03.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/service/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service04.php"><img src="images/service/b04.jpg" name="Image7" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/service/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service05.php"><img src="images/service/b05.jpg" name="Image8" width="171" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/service/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service05.php"><img src="images/service/b05-1.jpg" name="Image9" width="171" height="22" id="Image9" onmouseover="MM_swapImage('Image9','','images/service/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service05.php"><img src="images/service/b05-2.jpg" name="Image10" width="171" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/service/b05-2_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service05.php"><img src="images/service/b05-3.jpg" width="171" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/service/b05-3_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service05.php"><img src="images/service/b05-4.jpg" width="171" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/service/b05-4_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="service05.php"><img src="images/service/b05-5.jpg" width="171" height="22" id="Image13" onmouseover="MM_swapImage('Image13','','images/service/b05-5_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

        </table>

      </div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" -->

    <div id="content_top">

      <table width="688" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="184"><img src="images/service/pic01.jpg" alt="" width="184" height="197" /></td>

          <td width="5">&nbsp;</td>

          <td width="310"><img src="images/service/pic02.jpg" width="310" height="197" /></td>

          <td width="5">&nbsp;</td>

          <td width="184"><img src="images/service/pic03.jpg" width="184" height="197" /></td>

        </tr>

      </table>

    </div>

    <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->

    <div id="content_main">

      <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">

        <tr>

          <td height="15">&nbsp;</td>

        </tr>

        <tr>

          <td class="gray12">各位股東女士、先生：<br /></td>

        </tr>
        <tr>
          <td class="gray12_3">民國99年本公司全球化布局大幅向前邁進，4月與印度國有石油有限公司及日本丸紅株式會社於印度成立合資公司興建ESBR廠；5月與德國朗盛公司在大陸南通合資興建NBR工廠；12月簽約併購美國生產高性能SBC產品之Dexco公司。此外，99年研究開發取得二項經濟部業界科專計劃之獎勵，工安環保則榮獲高雄市政府評鑑為防火管理工作績優單位，執行成效斐然。</td>
        </tr>

        <tr>

          <td class="green12_2_b">99年度營業結果</td>

        </tr>

        <tr>

          <td class="black12-b">營業計劃實施成果及預算執行情形  及 財務收支及獲利能力分析</td>

        </tr>

        <tr>

          <td class="gray12_3">99年度全球經濟景氣回溫，汽車市場需求強勁，帶動合成橡膠需求量大幅增加。 主要原料丁二烯價格雖持續上揚，因本公司運籌調度得當，故能確保主要原料穩定供應，並藉由銷產購有效整合，增加新產品銷售比重提升獲利，99年度合併營收為新台幣366.86億元，較98年度成長66%，營業毛利新台幣65.86億元，較98年度增加36%，營業淨利新台幣50.31億元，較98年度增加36%。結算稅後淨利為新台幣32.79億元，較98年度增加41%，較預算增加165%；稅後純益率為11.15%，每股稅後盈餘為5.05元。</td>

        </tr>

        <tr>

          <td class="black12-b">研究發展狀況</td>

        </tr>

        <tr>

          <td class="gray12_3">在橡膠材料方面與下游輪胎廠商合作成功開發出環保節能輪胎用SSBR材料，亦同時持續進行高耐油性汽車與鞋材所需NBR材料之應用開發。在應用材料方面，商業化量產多項TPE新產品並通過客戶驗證。在掺配料部分開發出高價值發泡抗震掺配料，應用於鞋材並獲得國際大廠認證。在製程技術開發上延聘國際級專家共同協助開發環保及節能製程，大幅減少能源耗用及廢棄物的產生。</td>

        </tr>

        <tr>

          <td class="green12_2_b">100年度營業計劃概要</td>

        </tr>

        <tr>

          <td class="gray12_3">展望新的一年，全球經濟景氣雖持續回溫，但因原油等原物料價格大幅上升，整體經濟景氣情勢仍存在不確定性。預估合成橡膠需求在亞洲地區將持續成長，本公司今年仍繼續訂定全產全銷之目標，強化管控原物料及能源成本，將產能作充分有效利用，期能順利達成高獲利目標。在新投資項目方面，印度ESBR及南通NBR、SEBS、SIS等新擴建項目將依工程計劃時程及預算有效執行，如期完成商業化運轉。</td>

        </tr>

        <tr>

          <td class="green12_2_b">未來公司發展策略</td>

        </tr>

        <tr>

          <td class="gray12_3">本公司併購美國Dexco公司，在印度合資興建ESBR工廠，並在南通地區與朗盛合資興建NBR工廠及TPE SIS &amp; SEBS等擴建項目，已陸續啟動，全球化布局向前邁進一大步，本公司以擁有之核心技術及研發創新基礎，配合全球供應鏈系統有效運作，將可續創營運佳績，為公司創造更大利潤以回報全體股東之支持與鼓勵。</td>

        </tr>

        <tr>

          <td>&nbsp;</td>

        </tr>

        <tr>

          <td align="right"><img src="images/about/sign02.jpg" width="141" height="74" /></td>

        </tr>

        <tr>

          <td>&nbsp;</td>

        </tr>

      </table>

    </div>

    <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer"><table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></div>
</body>
<!-- InstanceEnd --></html>
