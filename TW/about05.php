<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/about.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>關於台橡─企業社會責任</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/about/b01_2.jpg','images/about/b02_2.jpg','images/about/b03_2.jpg','images/about/b04_2.jpg','images/about/b05_2.jpg','images/about/b05-1_2.jpg','images/about/b05-2_2.jpg','images/about/b05-3_2.jpg','images/about/b05-4_2.jpg','images/about/b05-5_2.jpg','images/about/b05-6_2.jpg','images/about/b05-7_2.jpg','images/about/b05-8_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　<a href="about.php">關於台橡</a>　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="about01.php"><img src="images/about/b01.jpg" name="Image1" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/about/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about02.php"><img src="images/about/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/about/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05.php"><img src="images/about/b05-1_2.jpg" name="Image7" width="171" height="22" id="Image7" onmouseover="MM_swapImage('Image7','','images/about/b05-1_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about03.php"><img src="images/about/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/about/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about04.php"><img src="images/about/b04.jpg" name="Image4" width="171" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/about/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="about05_2.php"><img src="images/about/b05.jpg" name="Image5" width="171" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/about/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
         
        </table>
      </div>
      <!-- InstanceEndEditable --><!-- InstanceBeginEditable name="top" --><!-- InstanceEndEditable --><!-- InstanceBeginEditable name="main" -->
      <div id="content_main_3">
        <table width="632" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="blue14-b">持續創新及全球化佈局，是台橡前進的動力</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡公司成立三十八年，歷經國家政策支持、台灣產業結構轉型、企業外移大陸浪潮與中國崛起的歷史時刻。每一次轉折，都能保持穩健的成長步伐不斷超越。其中關鍵因素就是台橡公司堅持勇於創新、靈活思考的態度，面對不同市場需求，抱持多元化的服務策略，滿足高端客戶需求，品質更是從不妥協，因而奠定市場上無可替代的品牌價值與良好商譽。</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡未來的競爭優勢有三大面向：其一是不斷的在技術上創新，取得生產製程方面的優勢，透過技術上的改良，符合高端客戶需求。其二在於導入專案管理的模式，執行『專業分工、團隊作戰』的經營方式。其三在於往海外擴展企業版圖，佈局全球化並與國際市場接軌。</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡公司深刻瞭解創新的重要性，不斷延攬技術人員組成專業研發團隊。更透過併購美國Dexco Polymers公司，成為SBC的全球領先廠商。獲得高階製造技術，致力產品差異化、高質化的目標。產品應用方面，已涉足到民生商品、醫療用品等諸多領域，不斷創新既有銷售模式及通路。</td>
          </tr>
          <tr>
            <td class="gray12_3">在管理效能上，則擺脫集體生產思維，以專案管理模式，為公司注入一股活力。召集不同專業領域的人才，劃分不同戰鬥單位，讓生產 、研發 、業務等各部門，可以靈活發揮專長，並且做橫向連結相互支援。</td>
          </tr>
          <tr>
            <td class="gray12_3">台橡公司最早的海外版圖從中國與泰國出發，近年開始往俄羅斯、印度、美國市場發展，在邁向國際化的同時，也體認到置身瞬息萬變的市場，唯有不斷創新，才能取得優勢競爭力。</td>
          </tr>
          <tr>
            <td class="gray12_3">隨著全球化時代巨輪的轉動，台橡繼續大步向前邁進，在世界市場上尋求ㄧ個新定位。從上游原料開發、到中下游的產品製造與銷售，台橡公司不斷與國際接軌，隨時掌握全球脈動，在追求成長的同時，台橡也積極追求環保方案之落實，注重廢氣排放與廢棄物處理，透過管理方式，及製程減碳節能，做好工安及環保工作，除了每年回饋地方與社區，也正在取得CSR認證，希望能善盡企業社會責任，達到社會大眾的期待。
</td>
          </tr>
          <tr>
            <td class="gray12_3">從台灣出發，現今的台橡公司更將帶著豐富的世界經驗走回台灣，在高分子材料的領域，不斷寫下亮麗的里程碑。</td>
          </tr>
          <tr>
            <td class="gray12_3">&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td align="right" ><img src="images/about/sign.jpg" width="180" height="80" /></td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --></div>
  </div>
</div><div id="footer">
    <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
</body>
<!-- InstanceEnd --></html>
