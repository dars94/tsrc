<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/member.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>集團成員─DECO POLYMERS LP</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/member/b01_2.jpg','images/member/b02_2.jpg','images/member/b03_2.jpg','images/member/b04_2.jpg','images/member/b05_2.jpg','images/member/b06_2.jpg','images/member/b07_2.jpg','images/member/b08_2.jpg','images/member/b09_2.jpg','images/member/b10_2.jpg','images/member/b11_2.jpg')">
<div id="wrap_2">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品　</a></span><span class="blue12">│<a href="#">　</a></span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="news.php" >新聞</a></span><span class="blue12">　│　<a href="member.php">集團成員</a>　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left_2">
        <table width="224" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><a href="member.php"><img src="images/member/b01.jpg" width="224" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/member/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member02.php"><img src="images/member/b02.jpg" width="224" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/member/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member03.php"><img src="images/member/b03.jpg" width="224" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/member/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member04.php"><img src="images/member/b04.jpg" width="224" height="22" id="Image4" onmouseover="MM_swapImage('Image4','','images/member/b04_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member05.php"><img src="images/member/b05.jpg" width="224" height="22" id="Image5" onmouseover="MM_swapImage('Image5','','images/member/b05_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><img src="images/member/b06_2.jpg" name="Image7" width="224" height="22" id="Image7" /></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member07.php"><img src="images/member/b07.jpg" width="224" height="22" id="Image8" onmouseover="MM_swapImage('Image8','','images/member/b07_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
         </tr><tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member08.php"><img src="images/member/b08.jpg" width="224" height="38" id="Image9" onmouseover="MM_swapImage('Image9','','images/member/b08_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member09.php"><img src="images/member/b09.jpg" width="224" height="22" id="Image10" onmouseover="MM_swapImage('Image10','','images/member/b09_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member10.php"><img src="images/member/b10.jpg" width="224" height="22" id="Image11" onmouseover="MM_swapImage('Image11','','images/member/b10_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="4" /></td>
          </tr>
          <tr>
            <td><a href="member11.php"><img src="images/member/b11.jpg" width="224" height="22" id="Image12" onmouseover="MM_swapImage('Image12','','images/member/b11_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><img src="images/spacer.gif" width="10" height="82" /></td>
          </tr>
          <tr>
            <td class="gray12_2" align="center">歡迎聯繫我們 ：</td>
          </tr>
          <tr>
            <td class="blue12" align="center"><a href="mailto:tsrcsales@tsrc-global.com">tsrcsales@tsrc-global.com</a></td>
          </tr>
        </table>
      </div>
    <!-- InstanceEndEditable -->
      <div id="content_top_3"><img src="images/member/pic01.jpg" width="635" height="110" /></div>
      <div id="content_main_2"><!-- InstanceBeginEditable name="main" -->
        <table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="left"><table width="584" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="205" align="left" valign="top" class="gray12">&nbsp;</td>
                <td width="379" align="left" valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td scope="col"><span class="blue14-b">DEXCO POLYMERS L.P.</span></td>
                  </tr>
                  <tr>
                    <td><span class="gray12">12012 Wickchester Lane, Suite 280, Houston, TX 77079, U.S.A.</span></td>
                  </tr>
                  <tr>
                    <td><span class="gray12">電話：+1 (281) 754 5800</span></td>
                  </tr>
                  <tr>
                    <td><span class="gray12">傳真：+1 (281) 754 5801</span></td>
                  </tr>
                  <tr>
                    <td><span class="gray12">Toll Free：  (877) 251-0580 </span></td>
                  </tr>
                  <tr>
                    <td><span class="gray12">網址：<a href="http://www.dexcopolymers.com" target="_blank">http://www.dexcopolymers.com</a></span></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="blue12_3">Dexco Polymers L.P.成立於1988年，總部位美國德州休士頓，工廠設於路易思安那州Plaquemine，生產SIS和SBS熱塑性彈性體，其應用範圍包括黏著劑、個人保健用品和聚合物改性應用等高端差異性產品，年產能達6.2萬公噸。主要生產／銷售VECTOR<sup>TM</sup>品牌產品。2011年4月台橡併購Dexco，不僅成功使雙方拓展全球版圖，更讓台橡進一步成為SBC全球前五大供應商。</td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td >&nbsp;</td>
          </tr>
        </table>
      <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div><div id="footer02">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="635" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
          <td width="25" align="right">&nbsp;</td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
