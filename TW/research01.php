<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/research.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>新產品開發</title>
<!-- InstanceEndEditable -->
<link href="css/page.css" rel="stylesheet" type="text/css" />
<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
$(function(){
	$('#item_block_1').css('display','none');
	$('#btn01_d').click(function(){
		$('#item_block_1').slideDown();
		$(this).css('display','none');
	});
	$('#btn01_u').click(function(){
		$('#item_block_1').slideUp();
		$('#btn01_d').css('display','');
	});
})
</script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/go_2.jpg','images/research/b01_2.jpg','images/research/b02_2.jpg','images/research/b03_2.jpg')">
<div id="wrap">
  <div id="main">
    <div id="langue">
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>
    </div>
    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>
    <div id="top">
      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品</a></span><span class="blue12">　│　<a href="research.php">研究與發展</a>　│　</span><span class="gray12_2"><a href="news.php">新聞</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>
      <div id="top_search">
        <table border="0" align="right" cellpadding="2" cellspacing="0">
          <tr>
            <td width="30" align="center" class="light_gray12">搜尋</td>
            <td><form id="searchform" name="searchform" method="get" action="search.php">
                <label for="textfield"></label>
                <input type="text" name="keyword" id="textfield" />
              </form></td>
            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
          </tr>
        </table>
      </div>
    </div>
    <div id="content"><!-- InstanceBeginEditable name="left" -->
      <div id="content_left">
        <table width="171" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25">&nbsp;</td>
          </tr>
          <tr>
            <td><img src="images/research/b01_2.jpg" name="Image1" width="171" height="22" id="Image1" /></td>
          </tr>
          <tr>
            <td><a href="research02.php"><img src="images/research/b02.jpg" name="Image2" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/research/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td><a href="research03.php"><img src="images/research/b03.jpg" name="Image3" width="171" height="22" id="Image3" onmouseover="MM_swapImage('Image3','','images/research/b03_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="top" -->
      <div id="content_top">
        <table width="688" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="184"><img src="images/research/pic01_1.jpg" alt="" width="184" height="198" /></td>
            <td width="5">&nbsp;</td>
            <td width="310"><img src="images/research/pic02_1.jpg" width="310" height="197" /></td>
            <td width="5">&nbsp;</td>
            <td width="184"><img src="images/research/pic03_1.jpg" width="184" height="198" /></td>
          </tr>
        </table>
      </div>
      <!-- InstanceEndEditable -->
      <div id="content_main"><!-- InstanceBeginEditable name="main" -->
        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td class="gray12_3">新產品的開發牽涉新產品、新製程、新應用的各類層面，必須整合各類資源，包括行銷、生產、研發等各部門的協同合作才能成功。台橡的研發團隊以客為尊，藉著長期與客戶密切的接觸，深入瞭解客戶的需求，以便開發出符合客戶所需的產品，台橡因應時勢的變動而調整改善開發制度，不斷導入嶄新觀念與做法，多年來成果卓著：</td>
          </tr>
          <tr>
            <td align="center" class="gray12"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="50%" align="left">• 開發環境友善的加油型SBR、BR</td>
                </tr>
                <tr>
                  <td align="left">• 符合國際重要客戶要求的高功能SIS、SBS、SEBS</td>
                </tr>
                <tr>
                  <td align="left">• 具創新技術的SEBS生產製程</td>
                </tr>
                <tr>
                  <td align="left">• 符合FDA要求的SIS、SEBS</td>
                </tr>
                <tr>
                  <td align="left">• 具高耐油性的NBR橡膠生產製程</td>
                </tr>
                <tr>
                  <td align="left">• 高性能星狀SIS產品</td>
                </tr>
                <tr>
                  <td align="left">• 具高彈性膜特性的四嵌段SIBS產品</td>
                </tr>
                <tr>
                  <td align="left">• 可供膜切黏膠應用的SBC產品</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="gray12">目前，台橡研發中的項目還有數十項，主要是<span class="gray12-b">特殊性能、高附加價值及環保的新型產品</span>。</td>
          </tr>
          <tr>
            <td class="green12"><div id="item_2">2010年環保節能高性能SSBR橡膠研發進展<a href="#"><img src="images/down.png" name="btn01" width="17" height="14" id="btn01_d" /></a></div></td>
          </tr>
          <tr>
            <td ><div id="item_block_1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="gray12_3">因應下游產業發展之變化及客戶對環境保護意識的需求提高，環保型新產品&quot;溶聚苯乙烯—丁二烯橡膠SSBR&quot;之開發即為本公司近年發展的重要項目之一：</td>
                  </tr>
                  <tr>
                    <td class="gray12_3">由於環保、節能輪胎(Green Tire)漸成發展趨勢(歐盟已要求於2012年輸入之輪胎須標示其燃油效率Fuel Efficiency、濕抓地力Wet Grip Performance、滾動阻力Rolling Resistance和噪音水準Noise Level等)，而要符合Green Tire特性需求使用SSBR/Silica配方技術才能成功，因此，輪胎業必須發展新技術與材料，否則未來在市場上將被淘汰或排擠。而目前國內對於使用於輪胎等級之SSBR並無實際生產廠商，對於環保輪胎使用之改質SSBR均須仰賴進口，於是台橡積極發起與下游廠商聯盟，結合上游橡膠業發展新一代的SSBR，並配合下游輪胎廠發展適合Green Tire特性需求的SSBR/Silica配方技術，共同開發環保節能輪胎產品。</td>
                  </tr>
                  <tr>
                    <td class="gray12_3">台橡公司主要關鍵技術為陰離子聚合微觀結構控制及製程開發設計技術，以及相關的末端改質技術和已量產的TPE(熱可塑橡膠)生產經驗。下游輪胎廠主要關鍵技術包括：SSBR與Filler(如碳黑或Silica)的混練分散技術開發適當Silica與偶合劑的配方開發；輪胎各部材之能量損失比例預估技術開發，及非線性有限元素分析(FEM)技術及自主的輪胎設計技術。 即以上下游研發聯盟形式進行研發合作，由台橡公司開發及提供改質之SSBR產品，下游輪胎廠則以此材料開發一系列之節能環保輪胎設計與製造。</td>
                  </tr>
                  <tr>
                    <td class="gray12_3">以上下游產業合組研發聯盟方式 ，台橡建立與下游客戶共同開發之模式，將可幫助國內產業擺脫國外技術限制，大幅提升競爭力。<a href="#"><img src="images/up.png" alt="" name="btn02" width="17" height="14" id="btn01_u" /></a></td>
                  </tr>
                </table>
              </div></td>
          </tr>
          <tr>
            <td class="gray12_3">&nbsp;</td>
          </tr>
          <tr>
            <td class="gray12_3">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <!-- InstanceEndEditable --></div>
    </div>
  </div>
</div>
<div id="footer">
  <table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="688" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd -->
</html>
