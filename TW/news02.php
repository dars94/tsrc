<?php
require_once('../db.php');
$shows = $db->shows()->where('lang','tw')->order('weight desc');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>展覽會與研討會</title>

<link href="css/page.css" rel="stylesheet" type="text/css" />

<link href="css/text.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $('#Image6').click(function(){
    $('#searchform').submit();
  });
});
</script>
<script type="text/javascript">

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}



function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

</script>

</head>



<body onload="MM_preloadImages('images/go_2.jpg','images/news/b01_2.jpg','images/news/b02_2.jpg')">

<div id="wrap">

  <div id="main">

    <div id="langue">

      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><span class="light_gray10">│ <a href="../EN/index.php">English</a> │<a href="../CN/index.php"> 中文简体 </a>│<a href="../TW/index.php"> 中文繁體 </a>│</span></td>
        </tr>
      </table>

    </div>

    <div id="logo"><img src="images/logo.jpg" width="186" height="70" /></div>

    <div id="top">

      <div id="top_btn"><span class="blue12">│　</span><span class="gray12_2"><a href="index.php">首頁</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="about.php">關於台橡</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="service.php">投資人服務</a></span><span class="blue12">　│　</span><span class="gray12_2"><a href="product.php">產品　</a></span><span class="blue12">│<a href="#">　</a></span><span class="gray12_2"><a href="research.php">研究與發展</a></span><span class="blue12">　│　</span><span class="gray12_2"></span><span class="blue12">新聞　│　</span><span class="gray12_2"><a href="member.php">集團成員</a></span><span class="blue12">　│</span></div>

      <div id="top_search">

        <table border="0" align="right" cellpadding="2" cellspacing="0">

          <tr>

            <td width="30" align="center" class="light_gray12">搜尋</td>

            <td><form id="searchform" name="searchform" method="get" action="search.php">

                <label for="textfield"></label>

                <input type="text" name="keyword" id="textfield" />

              </form></td>

            <td><img src="images/go.jpg" name="Image6" width="23" height="16" id="Image6" onmouseover="MM_swapImage('Image6','','images/go_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>

          </tr>

        </table>

      </div>

    </div>

    <div id="content" style="min-height:300px">

      <div id="content_left">

        <table width="171" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td height="25">&nbsp;</td>

          </tr>

          <tr>

            <td><a href="news.php"><img src="images/news/b01.jpg" width="171" height="22" id="Image1" onmouseover="MM_swapImage('Image1','','images/news/b01_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td><a href="news02.php"><img src="images/news/b02_2.jpg" width="171" height="22" id="Image2" onmouseover="MM_swapImage('Image2','','images/news/b02_2.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>

          </tr>

          <tr>

            <td>&nbsp;</td>

          </tr>

        </table>

      </div>

      <div id="content_top_2"><img src="images/news/pic01.jpg" width="688" height="145" /></div>

      <div id="content_main">

        <table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">

          <tr>

            <td height="15">&nbsp;</td>

          </tr>

          <tr>

            <td align="center"><table width="632" border="0" cellpadding="0" cellspacing="0" class="gray12">

            <?php
              foreach($shows as $t){
                echo '<tr>';
                echo '<td width="100" align="center" valign="top">';
                echo $t['datetime'];
                echo '</td>';
                echo '<td align="left" valign="top">';
                if(!empty($t['link'])){
                  echo '<a href="'.$t['link'].'" class="show_links" target="_blank">'.$t['title'].'</a>';
                }else{
                  echo $t['title'];
                }
                if($t['hot'] == '1'){
                  echo '<img src="../TW/images/hot.png" alt="" width="36" height="11" />';
                }
                echo '</td>';
                echo '</tr>';
                if(!empty($t['content'])){
                  echo '<tr>';
                  echo '<td>&nbsp;</td>';
                  echo '<td class="show_content">'.$t['content'].'</td>';
                  echo '</tr>';
                }
              }
              ?>

            </table></td>

          </tr>

          <tr>

            <td>&nbsp;</td>

          </tr>

          <tr>

            <td align="right" class="light_gray10">&nbsp;</td>

          </tr>

        </table>

      </div>

    </div>

  </div>

</div><div id="footer">
    <table width="998" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="866" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="833" align="right"><span class="light_gray10">© Copyright 2011 TSRC Corporation.All Rights Reserved. │ <a href="map.php">網站導覽</a></span></td>
            <td width="33" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>

</body>

</html>

